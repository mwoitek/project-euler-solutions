# Path sum: four ways
# https://projecteuler.net/problem=83

# julia main.jl p083_matrix.txt
# 1.24s user 0.34s system 133% cpu 1.185 total

using DataStructures

function fill_matrix!(m::Matrix{Int}, file_name::String)
    for (i, line) in enumerate(eachline(file_name))
        m[i, begin:end] = parse.(Int, split(line, ","))
    end
    return
end

function read_matrix(file_name::String)
    num_lines = countlines(file_name)
    m = Matrix{Int}(undef, num_lines, num_lines)
    fill_matrix!(m, file_name)
    m
end

function empty_adjacency_list(d::Int)
    adjacency_list = Dict{String,Vector{Tuple{String,Int}}}()
    for p in Iterators.product(1:d, 1:d)
        adjacency_list[join(p, ",")] = Tuple{String,Int}[]
    end
    adjacency_list
end

idxs_to_str(i, j) = "$(i),$(j)"

function get_adjacency_list(m::Matrix{Int})
    d = size(m, 1)
    adjacency_list = empty_adjacency_list(d)
    for j in 1:d
        for i in 1:d
            idxs = idxs_to_str(i, j)
            i - 1 ≥ 1 && push!(adjacency_list[idxs], (idxs_to_str(i - 1, j), m[i - 1, j]))
            i + 1 ≤ d && push!(adjacency_list[idxs], (idxs_to_str(i + 1, j), m[i + 1, j]))
            j - 1 ≥ 1 && push!(adjacency_list[idxs], (idxs_to_str(i, j - 1), m[i, j - 1]))
            j + 1 ≤ d && push!(adjacency_list[idxs], (idxs_to_str(i, j + 1), m[i, j + 1]))
        end
    end
    adjacency_list
end

function dijkstra(adjacency_list::Dict{String,Vector{Tuple{String,Int}}}, source::String)
    dists = Dict{String,Int}()
    dists[source] = 0
    pq = PriorityQueue{String,Int}()
    for vertex in keys(adjacency_list)
        if vertex ≠ source
            dists[vertex] = typemax(Int)
        end
        pq[vertex] = dists[vertex]
    end
    while !isempty(pq)
        vertex = dequeue!(pq)
        for (neighbor, dist_neighbor) in adjacency_list[vertex]
            alt_dist = dists[vertex] + dist_neighbor
            if alt_dist < dists[neighbor]
                dists[neighbor] = alt_dist
                pq[neighbor] = alt_dist
            end
        end
    end
    dists
end

function min_sum(m::Matrix{Int})
    adjacency_list = get_adjacency_list(m)
    d = size(m, 1)
    source = idxs_to_str(1, 1)
    destination = idxs_to_str(d, d)
    partial_sum = dijkstra(adjacency_list, source)[destination]
    m[1, 1] + partial_sum
end

function main()
    file_name = ARGS[1]
    m = read_matrix(file_name)
    answer = min_sum(m)
    println("Answer: ", answer)
end

main()
