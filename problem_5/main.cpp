// Smallest multiple
// https://projecteuler.net/problem=5

#include <iostream>

template <typename UInt>
bool isDivisible(const UInt num, const UInt maxDivisor);
template <typename UInt>
UInt findSmallest(const UInt maxDivisor);

int main() {
  const unsigned MAX_DIVISOR = 20;
  static_assert(MAX_DIVISOR > 0);

  const auto smallest = findSmallest(MAX_DIVISOR);
  std::cout << "Smallest number: " << smallest << '\n';

  return 0;
}

template <typename UInt>
bool isDivisible(const UInt num, const UInt maxDivisor) {
  bool divisible = true;

  for (UInt divisor = UInt(2); divisor <= maxDivisor; divisor++) {
    if (num % divisor != 0) {
      divisible = false;
      break;
    }
  }

  return divisible;
}

template <typename UInt>
UInt findSmallest(const UInt maxDivisor) {
  UInt num = maxDivisor;
  bool found = false;

  while (!found) {
    found = isDivisible(num++, maxDivisor);
  }

  return num - 1;
}
