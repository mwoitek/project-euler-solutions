# Names scores
# https://projecteuler.net/problem=22

import csv
from string import ascii_uppercase

LETTER_TO_NUM_DICT = {letter: num for num, letter in enumerate(ascii_uppercase, start=1)}


def name_to_num(name: str) -> int:
    return sum([LETTER_TO_NUM_DICT[letter] for letter in name])


def compute_scores() -> dict[str, int]:
    scores_dict = {}
    with open("names.csv", newline="") as csv_file:
        reader = csv.DictReader(csv_file, fieldnames=["line_num", "name"])
        for row in reader:
            line_num = int(row["line_num"])
            name = row["name"]
            scores_dict[name] = line_num * name_to_num(name)
    return scores_dict


def main():
    scores_dict = compute_scores()
    total_score = sum(scores_dict.values())
    print(f"Total of all the name scores: {total_score}")


if __name__ == "__main__":
    main()
