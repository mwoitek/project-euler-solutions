# Dice Game
# https://projecteuler.net/problem=205

using Printf: @printf

function compute_totals(num_dice, num_faces)
    totals_dict = Dict()
    for roll in Iterators.product([1:num_faces for _ in 1:num_dice]...)
        total = sum(roll)
        if haskey(totals_dict, total)
            totals_dict[total] += 1
        else
            totals_dict[total] = 1
        end
    end
    totals_dict
end

function compute_probabilities(num_dice, num_faces)
    totals_dict = compute_totals(num_dice, num_faces)
    num_possibilities = sum(values(totals_dict))
    Dict(total => cnt // num_possibilities for (total, cnt) in pairs(totals_dict))
end

function compute_probability_pete_wins()
    prob_pete_wins = 0
    probs_colin = compute_probabilities(6, 6)
    probs_pete = compute_probabilities(9, 4)
    for (total_pete, prob_pete) in pairs(probs_pete)
        totals_colin = Iterators.filter(<(total_pete), keys(probs_colin))
        prob_pete_wins +=
            prob_pete * sum(Iterators.map(t -> probs_colin[t], totals_colin); init = 0)
    end
    prob_pete_wins
end

function main()
    prob_pete_wins = compute_probability_pete_wins()
    println("Probability:")
    println("Exact result: ", prob_pete_wins)
    @printf "Approximate result: %.7f\n" Float64(prob_pete_wins)
end

main()
