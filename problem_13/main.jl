# Large sum
# https://projecteuler.net/problem=13

function read_numbers()
    numbers = open("numbers.txt", "r") do f
        parse.(BigInt, readlines(f))
    end
    numbers
end

function get_first_digits(num::BigInt, num_digits::Int)
    string(num)[1:num_digits]
end

numbers = read_numbers()
num_digits = 10
digits = get_first_digits(sum(numbers), num_digits)
println("First $num_digits digits: $digits")
