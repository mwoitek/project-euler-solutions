# Resilience
# https://projecteuler.net/problem=243

# 4828.31s user 2.37s system 99% cpu 1:20:39.03 total
# Answer: 892371480

using Primes: totient

const upper_limit = 15499 // 94744

resilience(d::Integer) = totient(d) // (d - oneunit(d))

function get_answer()
    resiliences = Iterators.map(d -> (d, resilience(d)), Iterators.countfrom(2, 1))
    itr = Iterators.dropwhile(p -> last(p) ≥ upper_limit, resiliences)
    Iterators.take(itr, 1) |> collect |> first |> first
end

function main()
    answer = get_answer()
    println("Answer: ", answer)
end

main()
