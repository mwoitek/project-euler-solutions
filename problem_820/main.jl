# N-th digit of reciprocals
# https://projecteuler.net/problem=820

# This code works, but it is slow. Find a better solution later.

include("common.jl")

function get_nth_digit_terminating(den::Int, n::Int)
    c = 0
    d, r = 0, 1
    while true
        c += 1
        d, r = divrem(10 * r, den)
        (r == 0 || c == n) && break
    end
    c == n ? d : 0
end

sum_digits_terminating(dens::Set{Int}, max_den::Int) =
    sum(Base.Fix2(get_nth_digit_terminating, max_den), dens)

function get_nth_digit_repeating(den::Int, n::Int, len_non_repeat::Int, len_repeat::Int)
    if n ≤ len_non_repeat
        d, r = 0, 1
        for _ in 1:n
            d, r = divrem(10 * r, den)
        end
        return d
    end
    i = mod(n - len_non_repeat, len_repeat)
    new_n = i == 0 ? len_non_repeat + len_repeat : len_non_repeat + i
    d, r = 0, 1
    for _ in 1:new_n
        d, r = divrem(10 * r, den)
    end
    d
end

function lengths_generator(max_den::Int)
    Channel{NTuple{4,Int}}() do ch
        for (i, line) in enumerate(eachline("lengths.csv"))
            den, len_non_repeat, len_repeat = parse.(Int, split(line, ","))
            den > max_den && break
            put!(ch, (i, den, len_non_repeat, len_repeat))
        end
    end
end

function fill_digits_arr!(digits_arr::Vector{Int}, max_den::Int)
    Threads.foreach(lengths_generator(max_den)) do tpl
        i, den, len_non_repeat, len_repeat = tpl
        digits_arr[i] = get_nth_digit_repeating(den, max_den, len_non_repeat, len_repeat)
    end
end

function sum_digits(max_den::Int)
    trivial_dens = get_trivial_denominators(max_den)
    sum_terminating = sum_digits_terminating(trivial_dens, max_den)
    digits_arr = Vector{Int}(undef, max_den - (length(trivial_dens) + 1))
    fill_digits_arr!(digits_arr, max_den)
    sum_repeating = sum(digits_arr)
    sum_terminating + sum_repeating
end

function main()
    max_den = parse(Int, ARGS[1])
    answer = sum_digits(max_den)
    println("Answer: ", answer)
end

main()
