function compute_powers(b::Int, upper_limit::Int)
    exps = Iterators.countfrom(0, 1)
    powers = Iterators.map(e -> b^e, exps)
    Iterators.takewhile(≤(upper_limit), powers)
end

function get_trivial_denominators(max_den::Int)
    powers_2 = compute_powers(2, max_den)
    powers_5 = compute_powers(5, max_den)
    prods = Iterators.map(prod, Iterators.product(powers_2, powers_5))
    Set(collect(Int, Iterators.filter(p -> 1 < p ≤ max_den, prods)))
end

get_non_trivial_denominators(trivial_dens::Set{Int}, max_den::Int) =
    collect(Int, Iterators.filter(∉(trivial_dens), 2:max_den))
