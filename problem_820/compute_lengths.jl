using Combinatorics: combinations
using IterTools: distinct
using Primes: factor, totient

include("common.jl")

function get_exponent(num::Int, b::Int)
    e = 0
    q, r = divrem(num, b)
    while r == 0
        e += 1
        q, r = divrem(q, b)
    end
    e
end

function compute_period(den::Int)
    factors = factor(Vector{Int}, totient(den))
    pushfirst!(factors, 1)
    divisors = Iterators.map(prod, combinations(factors))
    Iterators.dropwhile(d -> powermod(10, d, den) ≠ 1, distinct(divisors)) |> first
end

function lengths(den::Int)
    exponent_2 = get_exponent(den, 2)
    exponent_5 = get_exponent(den, 5)
    len_non_repeat = max(exponent_2, exponent_5)
    coprime_to_10 = den ÷ (2^exponent_2 * 5^exponent_5)
    len_repeat = compute_period(coprime_to_10)
    (len_non_repeat, len_repeat)
end

function compute_all_lengths!(lengths_arr::Vector{NTuple{2,Int}}, dens::Vector{Int})
    for (i, den) in enumerate(dens)
        lengths_arr[i] = lengths(den)
    end
    return
end

function write_lengths(lengths_arr::Vector{NTuple{2,Int}}, dens::Vector{Int})
    open("lengths.csv", "w") do file
        for (den, (len_non_repeat, len_repeat)) in Iterators.zip(dens, lengths_arr)
            println(file, den, ",", len_non_repeat, ",", len_repeat)
        end
    end
    return
end

function main()
    max_den = parse(Int, ARGS[1])
    trivial_dens = get_trivial_denominators(max_den)
    non_trivial_dens = get_non_trivial_denominators(trivial_dens, max_den)
    lengths_arr = Vector{NTuple{2,Int}}(undef, length(non_trivial_dens))
    compute_all_lengths!(lengths_arr, non_trivial_dens)
    write_lengths(lengths_arr, non_trivial_dens)
end

main()
