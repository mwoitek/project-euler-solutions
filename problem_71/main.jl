# Ordered fractions
# https://projecteuler.net/problem=71

# https://en.wikipedia.org/wiki/Farey_sequence

function farey_sequence(n::Integer, descending::Bool = false)
    num_0 = zero(typeof(n))
    num_1 = oneunit(n)
    a, b, c, d = num_0, num_1, num_1, n
    if descending
        a, c = num_1, n - num_1
    end
    Channel() do channel
        put!(channel, a // b)
        while (descending && a > num_0) || (!descending && c ≤ n)
            k = (n + b) ÷ d
            a, b, c, d = c, d, k * c - a, k * d - b
            put!(channel, a // b)
        end
    end
end

function naive_solution(max_d::Integer) # Fast enough for max_d ≤ 10000
    fractions = farey_sequence(max_d, true)
    itr = Iterators.dropwhile(
        f -> f ≥ convert(typeof(max_d), 3) // convert(typeof(max_d), 7),
        fractions,
    )
    fraction = (first ∘ collect)(Iterators.take(itr, 1))
    num = numerator(fraction)
    m = (num - convert(typeof(max_d), 2)) ÷ convert(typeof(max_d), 3)
    println("Naive solution:")
    println("Fraction: $(fraction)")
    println("Numerator: $(num)")
    println("m: $(m)")
end

function fast_solution(max_d::Integer)
    m = convert(typeof(max_d), ceil((max_d - 12) / 7))
    a = convert(typeof(max_d), 2 + 3 * m)
    b = convert(typeof(max_d), 5 + 7 * m)
    println("Fast solution:")
    println("Fraction: $(a // b)")
    println("Numerator: $(a)")
    println("m: $(m)")
end

function main()
    max_denominator = parse(Int, ARGS[1])
    max_denominator ≤ 10000 && naive_solution(max_denominator)
    fast_solution(max_denominator)
end

main()
