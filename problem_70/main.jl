# Totient permutation
# https://projecteuler.net/problem=70

using Primes: factor
using Transducers

function φ(n::T) where {T<:Integer}
    tr = factor(Set, n) |> Map(p -> one(T) - one(T) // p)
    foldl(*, tr; init = n) |> numerator
end

function is_pair_of_permutations((x, y)::Tuple{T,T}) where {T<:Integer}
    sort(digits(x), rev = true) == sort(digits(y), rev = true)
end

function compute_ratios(n_max::T) where {T<:Integer}
    T(2):(n_max - one(T)) |>
    Zip(Map(identity), Map(φ)) |>
    Filter(is_pair_of_permutations) |>
    Zip(Map(first), Map((n, t)::Tuple{T,T} -> n / t))
end

function find_solution(n_max::T) where {T<:Integer}
    foldl((p1, p2) -> last(p1) ≤ last(p2) ? p1 : p2, compute_ratios(n_max))
end

function main()
    n_max = 10_000_000
    n, ratio = find_solution(n_max)
    println("Answer:")
    println("n = ", n)
    println("Ratio = ", ratio)
end

main()
