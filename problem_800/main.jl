# Hybrid Integers
# https://projecteuler.net/problem=800

# julia main.jl 800800 800800
# 285.16s user 0.47s system 99% cpu 4:45.77 total

using Memoize: @memoize

const primes_file_name = "/home/woitek/repos/project_euler/primes/primes.txt"

@memoize mem_log(x::Int) = log(x)

test_pair_of_primes(p::Int, q::Int, a::Int, b::Int) =
    q * mem_log(p) + p * mem_log(q) ≤ b * mem_log(a)

function compute_number_of_primes(a::Int, b::Int)
    num_primes = 0
    primes = Iterators.map(Base.Fix1(parse, Int), eachline(primes_file_name))
    for q in primes
        test_pair_of_primes(2, q, a, b) || break
        num_primes += 1
    end
    num_primes
end

function fill_primes!(primes::Vector{Int}, num_primes::Int)
    primes_itr = Iterators.map(Base.Fix1(parse, Int), eachline(primes_file_name))
    for (i, p) in enumerate(Iterators.take(primes_itr, num_primes))
        primes[i] = p
    end
    return
end

function read_primes(a::Int, b::Int)
    num_primes = compute_number_of_primes(a, b)
    primes = Vector{Int}(undef, num_primes)
    fill_primes!(primes, num_primes)
    primes
end

function c(a::Int, b::Int, primes::Vector{Int}, num_primes::Int)
    cnt = 0
    for i in eachindex(primes)
        p = primes[i]
        j = i + 1
        while j ≤ num_primes && test_pair_of_primes(p, primes[j], a, b)
            cnt += 1
            j += 1
        end
    end
    cnt
end

function main()
    a = parse(Int, ARGS[1])
    b = parse(Int, ARGS[2])
    primes = read_primes(a, b)
    answer = c(a, b, primes, length(primes))
    println("Answer: ", answer)
end

main()
