# Path sum: three ways
# https://projecteuler.net/problem=82

# julia main.jl p082_matrix.txt
# 2.84s user 0.42s system 109% cpu 2.970 total

using DataStructures

function fill_matrix!(m::Matrix{Int}, file_name::String)
    for (i, line) in enumerate(eachline(file_name))
        m[i, begin:end] = parse.(Int, split(line, ","))
    end
    return
end

function read_matrix(file_name::String)
    num_lines = countlines(file_name)
    m = Matrix{Int}(undef, num_lines, num_lines)
    fill_matrix!(m, file_name)
    m
end

function empty_adjacency_list(d::Int)
    adjacency_list = Dict{String,Vector{Tuple{String,Int}}}()
    for p in Iterators.product(1:d, 1:d)
        adjacency_list[join(p, ",")] = Tuple{String,Int}[]
    end
    adjacency_list
end

idxs_to_str(i, j) = "$(i),$(j)"

function get_adjacency_list(m::Matrix{Int})
    d = size(m, 1)
    adjacency_list = empty_adjacency_list(d)
    for j in 1:d
        for i in 1:d
            idxs = idxs_to_str(i, j)
            i - 1 ≥ 1 && push!(adjacency_list[idxs], (idxs_to_str(i - 1, j), m[i - 1, j]))
            i + 1 ≤ d && push!(adjacency_list[idxs], (idxs_to_str(i + 1, j), m[i + 1, j]))
            j + 1 ≤ d && push!(adjacency_list[idxs], (idxs_to_str(i, j + 1), m[i, j + 1]))
        end
    end
    adjacency_list
end

function dijkstra(adjacency_list::Dict{String,Vector{Tuple{String,Int}}}, source::String)
    dists = Dict{String,Int}()
    dists[source] = 0
    pq = PriorityQueue{String,Int}()
    for vertex in keys(adjacency_list)
        if vertex ≠ source
            dists[vertex] = typemax(Int)
        end
        pq[vertex] = dists[vertex]
    end
    while !isempty(pq)
        vertex = dequeue!(pq)
        for (neighbor, dist_neighbor) in adjacency_list[vertex]
            alt_dist = dists[vertex] + dist_neighbor
            if alt_dist < dists[neighbor]
                dists[neighbor] = alt_dist
                pq[neighbor] = alt_dist
            end
        end
    end
    dists
end

function min_sum(
    adjacency_list::Dict{String,Vector{Tuple{String,Int}}},
    source::String,
    source_val::Int,
)
    partial_sums = dijkstra(adjacency_list, source)
    d = (string ∘ isqrt ∘ length)(adjacency_list)
    last_col_nodes = Iterators.filter(n -> endswith(n, "," * d), keys(adjacency_list))
    sums = Iterators.map(n -> source_val + partial_sums[n], last_col_nodes)
    minimum(sums)
end

function min_sum(m::Matrix{Int})
    adjacency_list = get_adjacency_list(m)
    d = size(m, 1)
    sources = Iterators.map(Base.Fix2(idxs_to_str, 1), 1:d)
    source_vals = @view m[begin:end, 1]
    sums = Iterators.map(
        p -> min_sum(adjacency_list, p...),
        Iterators.zip(sources, source_vals),
    )
    minimum(sums)
end

function main()
    file_name = ARGS[1]
    m = read_matrix(file_name)
    answer = min_sum(m)
    println("Answer: ", answer)
end

main()
