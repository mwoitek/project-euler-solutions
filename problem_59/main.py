# XOR decryption
# https://projecteuler.net/problem=59

from operator import itemgetter
from operator import xor


def read_message() -> list[int]:
    with open("p059_cipher.txt", "r") as f:
        return [int(i) for i in f.readline().split(",")]


def split_message(msg: list[int], start_idx: int) -> list[int]:
    return [msg[i] for i in range(start_idx, len(msg), 3)]


def compute_counts(msg_part: list[int]) -> dict[int, int]:
    counts = {}
    for number in msg_part:
        if number in counts:
            counts[number] += 1
        else:
            counts[number] = 1
    return {k: v for k, v in sorted(counts.items(), key=itemgetter(1, 0), reverse=True)}


def find_part_of_key(msg_part: list[int]) -> int:
    counts = compute_counts(msg_part)
    top_3 = list(counts)[:3]
    for number in top_3:
        for k in range(ord("a"), ord("z") + 1):
            x = xor(number, k)
            if chr(x).lower() == "e":
                return k
    return -1


def decrypt_part(msg: list[int], start_idx: int) -> list[int]:
    msg_part = split_message(msg, start_idx)
    part_of_key = find_part_of_key(msg_part)
    return [xor(number, part_of_key) for number in msg_part]


def decrypt_message(msg: list[int]) -> str:
    decrypted = ""
    decrypted_pt1 = decrypt_part(msg, 0)
    decrypted_pt2 = decrypt_part(msg, 1)
    decrypted_pt3 = decrypt_part(msg, 2)
    for i in range(len(msg) // 3):
        decrypted += chr(decrypted_pt1[i])
        decrypted += chr(decrypted_pt2[i])
        decrypted += chr(decrypted_pt3[i])
    return decrypted


if __name__ == "__main__":
    msg = read_message()
    decrypted = decrypt_message(msg)
    print("Decrypted message:")
    print(decrypted)
    print()
    print(f"Sum of the ASCII values: {sum(map(ord, decrypted))}")
