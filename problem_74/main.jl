# Digit factorial chains
# https://projecteuler.net/problem=74

# julia --threads 2 main.jl 60 1000000

using Transducers

create_factorials_dict() = Dict(d => factorial(d) for d in 0:9)

function sum_digit_factorials(number::Int, factorials_dict::Dict{Int,Int})
    foldl(+, number |> digits |> Map(d -> factorials_dict[d]))
end

function compute_chain_length(number::Int, factorials_dict::Dict{Int,Int})
    term = number
    terms = Set{Int}()
    while term ∉ terms
        push!(terms, term)
        term = sum_digit_factorials(term, factorials_dict)
    end
    length(terms)
end

function count_chains(num_terms::Int, upper_limit::Int)
    cnt = Threads.Atomic{Int}(0)
    factorials_dict = create_factorials_dict()
    chain_length = Base.Fix2(compute_chain_length, factorials_dict)
    Threads.@threads for number in 0:upper_limit
        chain_length(number) == num_terms && Threads.atomic_add!(cnt, 1)
    end
    cnt[]
end

function main()
    num_terms = parse(Int, ARGS[1])
    upper_limit = parse(Int, ARGS[2])
    cnt = count_chains(num_terms, upper_limit)
    println("Number of chains: ", cnt)
end

main()
