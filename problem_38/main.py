# Pandigital multiples
# https://projecteuler.net/problem=38

from operator import itemgetter


def read_pandigital_numbers() -> set[str]:
    with open("pandigital_numbers_1_to_9.txt", "r") as file:
        return set(map(lambda l: l.strip(), file))


def is_pandigital(number: str, pandigital_numbers: set[str]) -> bool:
    return number in pandigital_numbers


def get_max_pandigital(
    number: int,
    pandigital_numbers: set[str],
) -> tuple[int, int, int]:
    prods = list(
        map(
            lambda i: i * number,
            range(1, 10),
        )
    )
    concat_prods = map(
        lambda i: "".join(map(str, prods[:i])),
        range(2, 10),
    )
    pandigital_prods = filter(
        lambda p: len(p[1]) == 9 and is_pandigital(p[1], pandigital_numbers),
        enumerate(concat_prods, start=2),
    )
    try:
        max_prod = max(pandigital_prods, key=lambda p: int(p[1]))
        return (number, max_prod[0], int(max_prod[1]))
    except ValueError:
        return (-1, -1, -1)


def get_pandigital_concatenated_products(
    upper_limit: int,
    pandigital_numbers: set[str],
) -> list[tuple[int, int, int]]:
    max_prods = map(
        lambda n: get_max_pandigital(n, pandigital_numbers),
        range(1, upper_limit + 1),
    )
    return list(filter(lambda t: t != (-1, -1, -1), max_prods))


def write_output(pandigital_products: list[tuple[int, int, int]]) -> None:
    with open("solutions.csv", "w") as file:
        for solution in pandigital_products:
            line = ",".join(map(str, solution))
            file.write(f"{line}\n")


if __name__ == "__main__":
    UPPER_LIMIT = 10000
    pandigital_numbers = read_pandigital_numbers()
    pandigital_products = get_pandigital_concatenated_products(
        UPPER_LIMIT,
        pandigital_numbers,
    )
    answer = max(pandigital_products, key=itemgetter(2))
    print("Answer:")
    seq = ",".join(map(str, range(1, answer[1] + 1)))
    print(f"Concatenated product of {answer[0]} and ({seq})")
    print(f"Pandigital number: {answer[2]}")
    write_output(pandigital_products)
