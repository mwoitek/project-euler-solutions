// Largest palindrome product
// https://projecteuler.net/problem=4

#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <vector>

bool isPalindrome(const int& num);
std::vector<int> getPalindromes(const int numDigits);
int getLargestPalindrome(const std::vector<int>& palindromes);

int main() {
  const int NUM_DIGITS = 3;

  const std::vector<int> palindromes = getPalindromes(NUM_DIGITS);
  const int largestPalindrome = getLargestPalindrome(palindromes);
  std::cout << "Largest Palindrome: " << largestPalindrome << '\n';

  return 0;
}

bool isPalindrome(const int& num) {
  const std::string strNum = std::to_string(num);

  std::list<char> digits;
  std::copy(strNum.begin(), strNum.end(), std::back_inserter(digits));

  std::list<char> reverseDigits;
  std::copy(strNum.begin(), strNum.end(), std::front_inserter(reverseDigits));

  return digits == reverseDigits;
}

std::vector<int> getPalindromes(const int numDigits) {
  std::vector<int> palindromes;
  const int LOWER_LIMIT = static_cast<int>(std::pow(10, numDigits - 1));
  const int UPPER_LIMIT = 10 * LOWER_LIMIT;
  int product;

  for (int factor1 = LOWER_LIMIT; factor1 < UPPER_LIMIT; factor1++) {
    for (int factor2 = factor1; factor2 < UPPER_LIMIT; factor2++) {
      product = factor1 * factor2;
      if (isPalindrome(product)) {
        palindromes.push_back(product);
      }
    }
  }

  return palindromes;
}

int getLargestPalindrome(const std::vector<int>& palindromes) {
  return *(std::ranges::max_element(palindromes.begin(), palindromes.end()));
}
