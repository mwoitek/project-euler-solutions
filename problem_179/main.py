# Consecutive positive divisors
# https://projecteuler.net/problem=179

from itertools import pairwise


def create_divisors_dict() -> dict[int, int]:
    divisors_dict = {}
    with open("divisor_function.csv", "r") as file:
        for line in file:
            lst = list(map(int, line.strip().split(",")))
            n, num_divisors = lst[0], lst[1]
            divisors_dict[n] = num_divisors
    return divisors_dict


def count_integers() -> int:
    cnt = 0
    divisors_dict = create_divisors_dict()
    for _ in filter(
        lambda p: divisors_dict[p[0]] == divisors_dict[p[1]],
        pairwise(divisors_dict.keys()),
    ):
        cnt += 1
    return cnt


if __name__ == "__main__":
    cnt = count_integers()
    print(f"Number of integers: {cnt}")
