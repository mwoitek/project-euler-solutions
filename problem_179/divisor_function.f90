program main
  implicit none

  integer, parameter :: max_i = 10000000
  integer, parameter :: out_unit = 33
  integer :: i

  open (unit=out_unit, file="divisor_function.txt", action="write", status="replace")
  do i = 2, max_i
    write (out_unit, *) i, divisor_function(i)
  end do
  close (out_unit)

contains

  function divisor_function(n) result(num_divisors)
    integer, intent(in) :: n
    integer :: num_divisors
    integer :: d
    integer :: step

    if (n == 1 .or. n == 2) then
      num_divisors = n
      return
    end if

    if (mod(n, 2) == 0) then
      if (n / 2 /= 2) then
        num_divisors = 4
      else
        num_divisors = 3
      end if
      step = 1
    else
      num_divisors = 2
      step = 2
    end if

    d = 3
    do while (d * d <= n)
      if (mod(n, d) == 0) then
        num_divisors = num_divisors + 1
        if (n / d /= d) num_divisors = num_divisors + 1
      end if
      d = d + step
    end do
  end function divisor_function
end program main
