#!/bin/bash
base_name="divisor_function"
gfortran -o "$base_name" "${base_name}.f90"
./"$base_name"
awk '{ printf("%s,%s\n",$1,$2) }' "${base_name}.txt" >"${base_name}.csv"
trash-put "$base_name" "${base_name}.txt"
python3 main.py
trash-put "${base_name}.csv"
