#!/bin/sh
script_dir="$(realpath "$0" | xargs -I{} -n1 dirname {})"
"$script_dir"/process_words.sh
upper_limit="$(python3 "$script_dir"/word_to_number.py | cut -d' ' -f3)"
python3 "$script_dir"/main.py "$upper_limit"
join -t ',' "$script_dir"/words_nums.csv "$script_dir"/triangle_words.csv >"$script_dir"/tmp.csv
mv "$script_dir"/tmp.csv "$script_dir"/triangle_words.csv
rm "$script_dir"/words.txt "$script_dir"/words_nums.csv
