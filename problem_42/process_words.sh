#!/bin/sh
script_dir="$(realpath "$0" | xargs -I{} -n1 dirname {})"

in_file="${script_dir}/p042_words.txt"
[ -f "$in_file" ] || exit 1

# shellcheck disable=SC1003
tr -d '"' <"$in_file" |
  sed 's/,/\n/g; $a\' >"${script_dir}/words.txt"
