import sys
from operator import itemgetter
from pathlib import Path
from string import ascii_uppercase

LETTER_TO_NUM_DICT = {letter: num for num, letter in enumerate(ascii_uppercase, start=1)}
SCRIPT_DIR = Path(sys.argv[0]).resolve().parent


def word_to_num(word: str) -> int:
    return sum([LETTER_TO_NUM_DICT[letter] for letter in word])


def convert_words() -> dict[str, int]:
    word_num_dict = {}
    with open(SCRIPT_DIR / "words.txt", mode="r") as f:
        for line in f:
            word = line.strip()
            word_num_dict[word] = word_to_num(word)
    return word_num_dict


def write_output(word_num_dict: dict[str, int]) -> None:
    with open(SCRIPT_DIR / "words_nums.csv", mode="w") as f:
        for word, num in word_num_dict.items():
            f.write(f"{word},{num}\n")


def main():
    word_num_dict = convert_words()
    max_num = max(word_num_dict.items(), key=itemgetter(1))[1]
    print(f"Largest number: {max_num}")
    write_output(word_num_dict)


if __name__ == "__main__":
    main()
