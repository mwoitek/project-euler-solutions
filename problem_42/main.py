# Coded triangle numbers
# https://projecteuler.net/problem=42

import csv
import sys
from pathlib import Path

SCRIPT_DIR = Path(sys.argv[0]).resolve().parent


def compute_triangle_number(n: int) -> int:
    return int((n * (n + 1)) / 2)


def compute_triangle_numbers(upper_limit: int) -> set[int]:
    triangle_numbers = set()
    n = 1
    triangle_number = 1
    while triangle_number <= upper_limit:
        triangle_numbers.add(triangle_number)
        n += 1
        triangle_number = compute_triangle_number(n)
    return triangle_numbers


def label_triangle_words(triangle_numbers: set[int]) -> dict[str, bool]:
    triangle_words = {}
    with open(SCRIPT_DIR / "words_nums.csv", mode="r", newline="") as csv_file:
        reader = csv.DictReader(csv_file, fieldnames=["word", "num"])
        for row in reader:
            word = row["word"]
            num = int(row["num"])
            triangle_words[word] = num in triangle_numbers
    return triangle_words


def count_triangle_words(triangle_words: dict[str, bool]) -> int:
    return len([word for word in triangle_words if triangle_words[word]])


def write_output(triangle_words: dict[str, bool]) -> None:
    with open(SCRIPT_DIR / "triangle_words.csv", mode="w") as f:
        for word in triangle_words:
            yes_or_no = "YES" if triangle_words[word] else "NO"
            f.write(f"{word},{yes_or_no}\n")


def main():
    upper_limit = int(sys.argv[1])
    triangle_numbers = compute_triangle_numbers(upper_limit)
    triangle_words = label_triangle_words(triangle_numbers)
    count = count_triangle_words(triangle_words)
    print(f"Number of triangle words: {count}")
    write_output(triangle_words)


if __name__ == "__main__":
    main()
