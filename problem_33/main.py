# Digit cancelling fractions
# https://projecteuler.net/problem=33

from fractions import Fraction


class MyFraction:
    numerator: int
    denominator: int
    simplified: Fraction

    def __init__(self, numerator: int, denominator: int) -> None:
        self.numerator = numerator
        self.denominator = denominator
        self.simplified = (
            Fraction(numerator, denominator) if denominator != 0 else Fraction(1e15)
        )

    def __str__(self) -> str:
        return f"{self.numerator}/{self.denominator}"

    def __eq__(self, other: "MyFraction") -> bool:
        return self.simplified == other.simplified

    def is_trivial(self) -> bool:
        return self == MyFraction(self.numerator // 10, self.denominator // 10)

    def common_digits(self) -> set[int]:
        digits_num = {int(d) for d in str(self.numerator)}
        digits_den = {int(d) for d in str(self.denominator)}
        return digits_num.intersection(digits_den)

    def remove_digit(self, digit: int) -> "MyFraction":
        if digit not in self.common_digits():
            return self
        digit_str = str(digit)
        numerator = int(str(self.numerator).replace(digit_str, "", 1))
        denominator = int(str(self.denominator).replace(digit_str, "", 1))
        return MyFraction(numerator, denominator)

    def is_digit_cancelling(self) -> bool:
        if self.is_trivial():
            return False
        for digit in self.common_digits():
            if self == self.remove_digit(digit):
                return True
        return False


def find_solutions() -> list[MyFraction]:
    solutions = []
    two_digit_numbers = list(range(10, 100))
    for denominator in two_digit_numbers:
        for numerator in (n for n in two_digit_numbers if n < denominator):
            f = MyFraction(numerator, denominator)
            if f.is_digit_cancelling():
                solutions.append(f)
    return solutions


def compute_product(solutions: list[MyFraction]) -> Fraction:
    product = Fraction(1)
    for solution in solutions:
        product *= solution.simplified
    return product


if __name__ == "__main__":
    solutions = find_solutions()
    print("Digit cancelling fractions:")
    for solution in solutions:
        print(solution)

    product = compute_product(solutions)
    print(f"Product: {product.numerator}/{product.denominator}")
    print(f"Denominator: {product.denominator}")
