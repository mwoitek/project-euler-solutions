# Powerful digit counts
# https://projecteuler.net/problem=63

const log_10 = log(Float64(10))

function test_lower_bound(i::Int, n::Int)
    i_flt = Float64(i)
    n_flt = Float64(n)
    log(i_flt) > ((n_flt - 1) / n_flt) * log_10
end

function solutions_for_n_digits(n::Int)
    n == 1 && return 9
    test_func = i::Int -> test_lower_bound(i, n)
    length(filter(test_func, 1:9))
end

function main()
    solutions_itr = Iterators.map(solutions_for_n_digits, Iterators.countfrom(1, 1))
    answer = sum(Iterators.takewhile(n -> n > 0, solutions_itr))
    println("Answer: $(answer)")
end

main()
