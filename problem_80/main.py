# Square root digital expansion
# https://projecteuler.net/problem=80

from decimal import Decimal
from decimal import localcontext
from itertools import count
from itertools import takewhile

UPPER_LIMIT = 100
PRECISION = 100


def compute_irrational_roots(upper_limit: int, precision: int) -> list[str]:
    squares = set(
        takewhile(
            lambda s: s <= upper_limit,
            (i**2 for i in count(1)),
        )
    )
    with localcontext() as ctx:
        ctx.prec = precision + 10  # Add 10 to make sure it's precise enough
        irrational_roots = [
            Decimal(str(number)).sqrt()
            for number in filter(lambda n: n not in squares, range(1, upper_limit + 1))
        ]
        return list(map(lambda r: str(r)[: precision + 1], irrational_roots))


def compute_sum_of_digits(irrational_roots: list[str]) -> int:
    sum_of_digits = 0
    for root in irrational_roots:
        digits = root.replace(".", "")
        sum_of_digits += sum(map(int, digits))
    return sum_of_digits


if __name__ == "__main__":
    irrational_roots = compute_irrational_roots(UPPER_LIMIT, PRECISION)
    sum_of_digits = compute_sum_of_digits(irrational_roots)
    print(f"Total digital sum: {sum_of_digits}")
