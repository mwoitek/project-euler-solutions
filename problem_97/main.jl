# Large non-Mersenne prime
# https://projecteuler.net/problem=97

# julia main.jl 10

function main()
    a = UInt128(28433)
    b = UInt128(7830457)
    num_digits = parse(UInt128, ARGS[1])
    m = UInt128(10)^num_digits
    c = powermod(UInt128(2), b, m)
    last_digits = mod(a * c + one(UInt128), m)
    println("Last $(num_digits) digits: $(last_digits)")
end

main()
