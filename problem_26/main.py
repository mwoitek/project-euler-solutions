# Reciprocal cycles
# https://projecteuler.net/problem=26

from itertools import count
from itertools import takewhile

MAX_DENOMINATOR = 1000


def denominators_terminating_fracs(max_denominator: int) -> list[int]:
    powers_2 = list(
        takewhile(
            lambda p: p < max_denominator,
            (2**i for i in count(0)),
        )
    )
    powers_5 = list(
        takewhile(
            lambda p: p < max_denominator,
            (5**i for i in count(0)),
        )
    )
    denominators = filter(
        lambda d: 1 < d < max_denominator,
        (p2 * p5 for p2 in powers_2 for p5 in powers_5),
    )
    return sorted(list(denominators))


def denominators_repeating_fracs(max_denominator: int) -> list[int]:
    denominators_terminating = set(denominators_terminating_fracs(max_denominator))
    return list(
        filter(
            lambda d: d not in denominators_terminating,
            range(2, max_denominator),
        )
    )


def compute_repeating_fracs(d: int) -> tuple[str, int]:
    dividend = 10
    pos_dict = {}
    pos = 0
    digits = []
    while dividend not in pos_dict:
        pos_dict[dividend] = pos
        pos += 1
        q, r = divmod(dividend, d)
        digits.append(q)
        dividend = 10 * r
    pos_dividend = pos_dict[dividend]
    decimal_repr = "".join(
        [
            "0.",
            "".join(map(str, digits[:pos_dividend])),
            "(",
            "".join(map(str, digits[pos_dividend:])),
            ")",
        ]
    )
    period = pos - pos_dividend
    return decimal_repr, period


if __name__ == "__main__":
    denominators_repeating = denominators_repeating_fracs(MAX_DENOMINATOR)
    repeating_fracs = list(map(compute_repeating_fracs, denominators_repeating))
    d, (decimal_repr, period) = max(
        zip(denominators_repeating, repeating_fracs), key=lambda p: p[1][1]
    )
    print("Answer:")
    print(f"Denominator: {d}")
    print(f"Decimal representation: {decimal_repr}")
    print(f"Period: {period}")
