# Counting rectangles
# https://projecteuler.net/problem=85

# julia main.jl 500

const approx_count = 2 * 10^6

# https://math.stackexchange.com/questions/1240264/how-many-rectangles-or-triangles
number_of_rectangles(w::UInt, h::UInt) = (w * (w + 1) * h * (h + 1)) ÷ oftype(w, 4)

struct Grid
    width::UInt
    height::UInt
    rectangles::UInt
end

Base.isless(x::Grid, y::Grid) =
    abs(approx_count - x.rectangles) < abs(approx_count - y.rectangles)

function create_grid(width::UInt, height::UInt)
    rectangles = number_of_rectangles(width, height)
    Grid(width, height, rectangles)
end

compute_area(g::Grid) = g.width * g.height

function get_answer(max_width::UInt)
    d = (max_width * (max_width + 1)) ÷ 2
    grids = Vector{Grid}(undef, d)
    i = 1
    for w in UInt(1):max_width
        for h in UInt(1):w
            grids[i] = create_grid(w, h)
            i += 1
        end
    end
    sort!(grids)
    compute_area(first(grids))
end

function main()
    max_width = parse(UInt, ARGS[1])
    answer = get_answer(max_width)
    println("Answer: ", answer)
end

main()
