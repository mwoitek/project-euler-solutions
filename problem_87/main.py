# Prime power triples
# https://projecteuler.net/problem=87

from itertools import product
from itertools import takewhile

from more_itertools import unique_everseen

PRIMES_FILE_NAME = "/home/woitek/repos/project_euler/primes/primes.txt"
UPPER_LIMIT = 5 * 10**7


def get_prime_powers(exponent: int, upper_limit: int) -> list[int]:
    with open(PRIMES_FILE_NAME, "r") as file:
        primes = map(int, file)
        prime_powers = map(lambda p: p**exponent, primes)
        return list(takewhile(lambda p: p < upper_limit, prime_powers))


def get_answer(upper_limit: int) -> int:
    prime_powers = [
        get_prime_powers(2, upper_limit),
        get_prime_powers(3, upper_limit),
        get_prime_powers(4, upper_limit),
    ]
    triples = product(*prime_powers)
    sums = map(sum, triples)
    return sum(map(lambda s: s < upper_limit, unique_everseen(sums)))


if __name__ == "__main__":
    answer = get_answer(UPPER_LIMIT)
    print(f"Answer: {answer}")
