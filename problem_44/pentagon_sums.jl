include("utils.jl")

function get_pentagon_sums(pentagon_numbers::Vector{UInt})
    pentagon_sums = Tuple{Int,Int,UInt}[]
    len = length(pentagon_numbers)
    for (j, k) in ((j, k) for j = 1:len, k = 1:len if j ≤ k)
        s = pentagon_numbers[j] + pentagon_numbers[k]
        is_pentagonal(s, pentagon_numbers) && push!(pentagon_sums, (j, k, s))
    end
    pentagon_sums
end

function main()
    pentagon_numbers = read_numbers()
    pentagon_sums = get_pentagon_sums(pentagon_numbers)
    write_output(pentagon_sums, "pentagon_sums.txt")
end

main()
