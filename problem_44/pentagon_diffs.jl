# Pentagon numbers
# https://projecteuler.net/problem=44

include("utils.jl")

function get_candidates()
    lines = open("pentagon_sums.txt", "r") do f
        readlines(f)
    end
    candidates = Tuple{Int,Int}[]
    for line in lines
        pair = parse.(Int, split(line, ",")[1:2])
        push!(candidates, (pair[1], pair[2]))
    end
    candidates
end

function get_pentagon_diffs(pentagon_numbers::Vector{UInt}, candidates::Vector{Tuple{Int,Int}})
    pentagon_diffs = Tuple{Int,Int,UInt}[]
    for (j, k) in candidates
        d = pentagon_numbers[k] - pentagon_numbers[j]
        is_pentagonal(d, pentagon_numbers) && push!(pentagon_diffs, (j, k, d))
    end
    pentagon_diffs
end

function main()
    pentagon_numbers = read_numbers()
    candidates = get_candidates()
    pentagon_diffs = get_pentagon_diffs(pentagon_numbers, candidates)
    write_output(pentagon_diffs, "pentagon_diffs.txt")
end

main()
