compute_pentagon_number(n::UInt) = div(n * (3 * n - 1), 2)

function compute_pentagon_numbers(list_size::UInt)
    [compute_pentagon_number(n) for n = 1:list_size]
end

function write_output(pentagon_numbers::Vector{UInt})
    open("pentagon_numbers.txt", "w") do f
        for pentagon_number in pentagon_numbers
            write(f, "$pentagon_number\n")
        end
    end
end

function main()
    list_size = parse(UInt, ARGS[1])
    pentagon_numbers = compute_pentagon_numbers(list_size)
    write_output(pentagon_numbers)
end

main()
