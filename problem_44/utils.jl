function read_numbers()
    pentagon_numbers = open("pentagon_numbers.txt", "r") do f
        parse.(UInt, readlines(f))
    end
    pentagon_numbers
end

function is_pentagonal(num::UInt, pentagon_numbers::Vector{UInt})
    num ≤ pentagon_numbers[end] && num ∈ pentagon_numbers
end

function write_output(tpls::Vector{Tuple{Int,Int,UInt}}, file_name::String)
    open(file_name, "w") do f
        for (j, k, n) in tpls
            write(f, "$j,$k,$n\n")
        end
    end
end
