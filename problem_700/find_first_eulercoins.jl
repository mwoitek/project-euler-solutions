# Finds first 33 Eulercoins
# julia find_first_eulercoins.jl 5000000000
# 270.73s user 0.35s system 100% cpu 4:30.91 total

function compute_term(n::T)::T where {T<:Integer}
    a::T = 1504170715041707
    m::T = 4503599627370517
    mod(widemul(a, n), m)
end

function find_first_eulercoins(n_max::T) where {T<:Integer}
    eulercoins = T[1504170715041707]
    for term in Iterators.map(compute_term, oftype(n_max, 2):n_max)
        term < last(eulercoins) && push!(eulercoins, term)
    end
    eulercoins
end

function write_eulercoins(eulercoins, file_name)
    open(file_name, "w") do file
        foreach(ec -> println(file, ec), eulercoins)
    end
end

function main()
    n_max = parse(Int, ARGS[1])
    eulercoins = find_first_eulercoins(n_max)
    write_eulercoins(eulercoins, "eulercoins.txt")
end

main()
