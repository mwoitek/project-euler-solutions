#!/bin/bash
julia find_first_eulercoins.jl 5000000000
last_eulercoin="$(tail -n1 eulercoins.txt)"
x_max=$(("$last_eulercoin" - 1))
julia inverse_sequence.jl "$x_max"
csv_file="inverse_sequence_${x_max}.csv"
julia find_eulercoins.jl "$csv_file" eulercoins.txt "$last_eulercoin"
rm "$csv_file"
echo '0' >>eulercoins.txt
julia main.jl
