# Finds remaining Eulercoins
# julia find_eulercoins.jl inverse_sequence_8850626.csv eulercoins.txt 8850627
# 34.87s user 13.25s system 100% cpu 47.776 total

function candidates_generator(file_name::String)
    Channel{Int}() do ch
        open(file_name, "r") do file
            for line in eachline(file)
                term = parse(Int, last(split(line, ",")))
                put!(ch, term)
            end
        end
    end
end

function find_eulercoins(last_eulercoin::Int, file_name::String)
    eulercoins = Int[last_eulercoin]
    for term in candidates_generator(file_name)
        term < last(eulercoins) && push!(eulercoins, term)
    end
    eulercoins[2:end]
end

function write_eulercoins(eulercoins::Vector{Int}, file_name::String)
    open(file_name, "a") do file
        foreach(ec -> println(file, ec), eulercoins)
    end
end

function main()
    in_file = ARGS[1]
    out_file = ARGS[2]
    last_eulercoin = parse(Int, ARGS[3])
    eulercoins = find_eulercoins(last_eulercoin, in_file)
    write_eulercoins(eulercoins, out_file)
end

main()
