# Eulercoin
# https://projecteuler.net/problem=700

function get_eulercoins()
    eulercoins = open("eulercoins.txt", "r") do file
        parse.(UInt128, readlines(file))
    end
    eulercoins
end

function main()
    eulercoins = get_eulercoins()
    sum_eulercoins = sum(eulercoins)
    println("Sum of Eulercoins: ", sum_eulercoins)
end

main()
