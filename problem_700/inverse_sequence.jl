multiplicative_inverse(a::Int) = invmod(a, 4503599627370517)

function compute_term(x::Int)::Int
    a_inv = multiplicative_inverse(1504170715041707)
    m = 4503599627370517
    mod(widemul(a_inv, x), m)
end

function generate_sequence(x_max::Int)
    terms = Iterators.map(compute_term, 1:x_max)
    seq = collect(Iterators.zip(terms, 1:x_max))
    sort!(seq, by = first)
    seq
end

function write_sequence(seq, file_name)
    open(file_name, "w") do file
        foreach(p -> println(file, p[1], ",", p[2]), seq)
    end
end

function main()
    x_max = parse(Int, ARGS[1])
    seq = generate_sequence(x_max)
    file_name = "inverse_sequence_$(x_max).csv"
    write_sequence(seq, file_name)
end

main()
