# Triangle containment
# https://projecteuler.net/problem=102

using LinearAlgebra
using StaticArrays

const dpi = Float64(2 * pi)

function vertices_generator(ch::Channel)
    lines = open("p102_triangles.txt", "r") do file
        readlines(file)
    end
    for line in lines
        numbers = SVector{6}(parse.(Int, split(line, ",")))
        a = SVector{2}(numbers[1:2])
        b = SVector{2}(numbers[3:4])
        c = SVector{2}(numbers[5:6])
        put!(ch, (a, b, c))
    end
end

function angle_between_vectors(vec_1::SVector{2,Int}, vec_2::SVector{2,Int})
    dot_product = dot(vec_1, vec_2)
    norm_1::Float64 = norm(vec_1)
    norm_2::Float64 = norm(vec_2)
    cosine = Float64(dot_product) / (norm_1 * norm_2)
    acos(cosine)
end

function test_triangle(vertices::Tuple{SVector{2,Int},SVector{2,Int},SVector{2,Int}})
    angle_12 = angle_between_vectors(vertices[1], vertices[2])
    angle_13 = angle_between_vectors(vertices[1], vertices[3])
    angle_23 = angle_between_vectors(vertices[2], vertices[3])
    isapprox(angle_12 + angle_13 + angle_23, dpi)
end

function count_triangles()
    cnt = Threads.Atomic{Int}(0)
    Threads.foreach(Channel(vertices_generator)) do vertices
        test_triangle(vertices) && Threads.atomic_add!(cnt, 1)
    end
    cnt[]
end

function main()
    cnt = count_triangles()
    println("Number of triangles: ", cnt)
end

main()
