# Path sum: two ways
# https://projecteuler.net/problem=81

function read_matrix(file_name::String)
    lines = open(file_name, "r") do file
        readlines(file)
    end
    num_lines = length(lines)
    matrix = zeros(Int, num_lines, num_lines)
    for (i, line) in enumerate(lines)
        matrix[i, :] = parse.(Int, split(line, ","))
    end
    matrix
end

function compute_minimal_path_sum(matrix::Matrix{Int})
    sums_matrix = deepcopy(matrix)
    num_rows, num_cols = size(sums_matrix)
    foreach(r -> sums_matrix[r, 1] += sums_matrix[r - 1, 1], 2:num_rows)
    foreach(c -> sums_matrix[1, c] += sums_matrix[1, c - 1], 2:num_cols)
    for r in 2:num_rows
        for c in 2:num_cols
            sums_matrix[r, c] += min(sums_matrix[r - 1, c], sums_matrix[r, c - 1])
        end
    end
    sums_matrix[end, end]
end

function main()
    file_name = ARGS[1]
    matrix = read_matrix(file_name)
    minimal_path_sum = compute_minimal_path_sum(matrix)
    println("Minimal path sum: $(minimal_path_sum)")
end

main()
