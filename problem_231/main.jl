# The prime factorisation of binomial coefficients
# https://projecteuler.net/problem=231

using Primes: factor

const f_0 = 15 * 10^6 + 1
const f_1 = 2 * 10^7
const f_2 = 5 * 10^6

function simplify_coefficient()
    factors_num = collect(f_0:f_1)
    factors_den = collect(2:f_2)
    for d in 2:f_2
        m = d * cld(f_0, d)
        while m ≤ f_1 && factors_num[m - 15000000] ≠ m
            m += d
        end
        m > f_1 && continue
        factors_num[m - 15000000] = m ÷ d
        factors_den[d - 1] = 1
    end
    (factors_num, filter(d -> factors_den[d - 1] ≠ 1, 2:f_2))
end

function factor_coefficient()
    factors_dict = Dict()
    factors_num, factors_den = simplify_coefficient()
    for factor_num in factors_num
        for (prime, multiplicity) in pairs(factor(factor_num))
            if haskey(factors_dict, prime)
                factors_dict[prime] += multiplicity
            else
                factors_dict[prime] = multiplicity
            end
        end
    end
    for factor_den in factors_den
        for (prime, multiplicity) in pairs(factor(factor_den))
            factors_dict[prime] -= multiplicity
        end
    end
    factors_dict
end

function sum_prime_factors()
    factors_dict = factor_coefficient()
    terms = Iterators.map(p -> p.first * p.second, pairs(factors_dict))
    sum(terms)
end

function main()
    sum_ = sum_prime_factors()
    println("Sum of prime factors: ", sum_)
end

main()
