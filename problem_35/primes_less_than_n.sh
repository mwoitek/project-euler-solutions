#!/bin/sh
n="$1"
awk -v n="$n" '$1<n { print $1 }' primes.txt >"primes_less_than_${n}.txt"
