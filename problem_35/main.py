# Circular primes
# https://projecteuler.net/problem=35


def read_numbers(file_name: str) -> set[int]:
    numbers = set()
    with open(file_name, mode="r") as file:
        for line in file:
            number = int(line.strip())
            numbers.add(number)
    return numbers


def get_cyclic_permutations(number: int) -> set[int]:
    number_str = str(number)
    if len(number_str) == 1:
        return set()
    permutation = number_str[1:] + number_str[0]
    permutations = set()
    while permutation != number_str:
        permutations.add(int(permutation))
        permutation = permutation[1:] + permutation[0]
    return permutations


def is_circular_prime(prime: int, primes: set[int]) -> bool:
    for permutation in get_cyclic_permutations(prime):
        if permutation not in primes:
            return False
    return True


def get_circular_primes(primes: set[int]) -> set[int]:
    circular_primes = set()
    for prime in primes:
        if is_circular_prime(prime, primes):
            circular_primes.add(prime)
    return circular_primes


def write_output(circular_primes: set[int], file_name: str) -> None:
    with open(file_name, mode="w") as file:
        for prime in sorted(circular_primes):
            file.write(f"{prime}\n")


def main():
    primes = read_numbers("primes_less_than_1000000.txt")
    circular_primes = get_circular_primes(primes)
    print(f"Number of circular primes: {len(circular_primes)}")
    write_output(circular_primes, "circular_primes_less_than_1000000.txt")


if __name__ == "__main__":
    main()
