# Quadratic primes
# https://projecteuler.net/problem=27

from csv import DictReader
from operator import itemgetter
from typing import Callable

from utils import read_numbers

Coeffs = tuple[int, int]
QuadFunc = Callable[[int], int]


def read_candidates_for_a_and_b() -> list[Coeffs]:
    with open("candidates_for_a_and_b.csv", mode="r", newline="") as csv_file:
        reader = DictReader(csv_file, fieldnames=["a", "b"])
        return [(int(row["a"]), int(row["b"])) for row in reader]


def create_quadratic_function(a: int, b: int) -> QuadFunc:
    def quadratic_function(n: int) -> int:
        return n**2 + a * n + b

    return quadratic_function


def create_quadratic_functions(candidates: list[Coeffs]) -> list[QuadFunc]:
    return [create_quadratic_function(a, b) for a, b in candidates]


def compute_count(quadratic_function: QuadFunc, primes: set[int]) -> int:
    n = 2
    while quadratic_function(n) in primes:
        n += 1
    return n


def compute_counts(candidates: list[Coeffs], primes: set[int]) -> list[int]:
    quadratic_functions = create_quadratic_functions(candidates)
    return list(map(lambda f: compute_count(f, primes), quadratic_functions))


def find_max_coefficients(candidates: list[Coeffs], counts: list[int]) -> Coeffs:
    return max(zip(candidates, counts), key=itemgetter(1))[0]


def write_coefficients_counts(candidates: list[Coeffs], counts: list[int]) -> None:
    with open("counts.csv", mode="w") as csv_file:
        for (a, b), count in zip(candidates, counts):
            csv_file.write(f"{a},{b},{count}\n")


def main():
    candidates = read_candidates_for_a_and_b()
    primes = read_numbers("primes_less_than_1000000.txt")
    counts = compute_counts(candidates, primes)
    a, b = find_max_coefficients(candidates, counts)
    print(f"Coefficients: a = {a}, b = {b}")
    max_count = counts[candidates.index((a, b))]
    print(f"Number of primes: {max_count}")
    print(f"Product: a * b = {a * b}")
    write_coefficients_counts(candidates, counts)


if __name__ == "__main__":
    main()
