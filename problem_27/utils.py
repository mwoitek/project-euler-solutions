def read_numbers(file_name: str) -> set[int]:
    with open(file_name, mode="r") as file:
        return {int(line.strip()) for line in file}
