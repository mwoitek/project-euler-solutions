from utils import read_numbers

UPPER_LIMIT = 1000
Coeffs = tuple[int, int]


def get_candidates_for_b(primes: set[int], upper_limit: int) -> list[int]:
    return [b for b in sorted(primes) if b <= upper_limit]


def is_sum_prime(a: int, b: int, primes: set[int]) -> bool:
    s = a + b + 1
    return s > 0 and s in primes


def get_candidates_for_a_and_b(primes: set[int], upper_limit: int) -> list[Coeffs]:
    candidates = []
    bs = get_candidates_for_b(primes, upper_limit)
    for b in bs:
        for a in range(-(upper_limit - 1), upper_limit):
            if is_sum_prime(a, b, primes):
                candidates.append((a, b))
    return candidates


def write_output(candidates: list[Coeffs], file_name: str) -> None:
    with open(file_name, mode="w") as file:
        for a, b in candidates:
            file.write(f"{a},{b}\n")


def main():
    primes = read_numbers("primes_less_than_1000000.txt")
    candidates = get_candidates_for_a_and_b(primes, UPPER_LIMIT)
    write_output(candidates, "candidates_for_a_and_b.csv")


if __name__ == "__main__":
    main()
