# Reversible prime squares
# https://projecteuler.net/problem=808

# julia main.jl 50

using Primes: isprime

const primes_file_name = "/home/woitek/repos/project_euler/primes/primes.txt"

reverse_number(number::Int128) = parse(Int128, join(digits(number)))

function prime_squared_generator()
    Channel{Tuple{Int128,Int128}}() do ch
        open(primes_file_name, "r") do f
            for line in eachline(f)
                p2 = parse(Int128, line)^2
                put!(ch, (p2, reverse_number(p2)))
            end
        end
    end
end

function is_reversible((p2, reverse_p2)::Tuple{Int128,Int128})
    p2 == reverse_p2 && return false
    i = isqrt(reverse_p2)
    i * i == reverse_p2 && isprime(i)
end

function get_reversible_prime_squares(num::Int)
    reversible_tpls = Iterators.filter(is_reversible, prime_squared_generator())
    reversible_nums = Iterators.map(first, reversible_tpls)
    collect(Iterators.take(reversible_nums, num))
end

function main()
    num = parse(Int, ARGS[1])
    reversible_prime_squares = get_reversible_prime_squares(num)
    println("Sum: ", sum(reversible_prime_squares))
end

main()
