# Investigating a Prime Pattern
# https://projecteuler.net/problem=146

# julia -t 2 main.jl 149999999
# 273.39s user 0.48s system 191% cpu 2:22.76 total

using Primes: isprime, nextprime
using Transducers: Filter, foldxt

# https://mathworld.wolfram.com/DiophantineEquation2ndPowers.html
function is_candidate(n)
    n_squared = n * n

    s = n_squared + 1
    (s % 4 ≠ 1 || !isprime(s)) && return false

    s = n_squared + 9
    (s % 4 ≠ 1 || !isprime(s)) && return false

    s = n_squared + 3
    (s % 6 ≠ 1 || !isprime(s)) && return false

    s = n_squared + 27
    (s % 6 ≠ 1 || !isprime(s)) && return false

    s = n_squared + 7
    (s % 14 ∉ [1, 9, 11] || !isprime(s)) && return false

    isprime(n_squared + 13)
end

function is_solution(n)
    n_squared = n * n
    primes_1 = n_squared .+ [1, 3, 7, 9, 13, 27]
    primes_2 = Iterators.map(p -> nextprime(p + 1), primes_1[1:(end - 1)])
    prime_pairs = Iterators.zip(primes_1[2:end], primes_2)
    all(Iterators.map(p -> p[1] == p[2], prime_pairs))
end

compute_sum(n_max) =
    foldxt(+, 10:n_max |> Filter(is_candidate) |> Filter(is_solution); init = 0)

function main()
    n_max = parse(Int, ARGS[1])
    answer = compute_sum(n_max)
    println("Answer: ", answer)
end

main()
