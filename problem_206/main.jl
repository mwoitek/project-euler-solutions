# Concealed Square
# https://projecteuler.net/problem=206

const sqrt_min = (UInt ∘ isqrt)(1020304050607080900)
const log_min = (log ∘ sqrt ∘ Float64)(1020304050607080900)
const log_max = (log ∘ sqrt ∘ Float64)(1929394959697989990)

function get_candidates()
    itr = Iterators.dropwhile(
        n -> (log ∘ Float64)(n) < log_min,
        Iterators.countfrom(sqrt_min, one(UInt)),
    )
    Iterators.takewhile(n -> (log ∘ Float64)(n) ≤ log_max, itr)
end

function find_solution()
    reg_exp = r"1\d{1}2\d{1}3\d{1}4\d{1}5\d{1}6\d{1}7\d{1}8\d{1}9\d{1}0"
    squares = Iterators.map(n -> (n, string(UInt128(n)^2)), get_candidates())
    itr = Iterators.dropwhile(s -> isnothing(match(reg_exp, s[2])), squares)
    (first ∘ collect)(Iterators.take(itr, 1))
end

function main()
    n, n_squared = find_solution()
    println("Answer:")
    println("n = $(n)")
    println("n^2 = $(n_squared)")
end

main()
