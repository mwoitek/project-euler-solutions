// Multiples of 3 or 5
// https://projecteuler.net/problem=1

#include <iostream>
#include <numeric>
#include <unordered_set>

const unsigned int UPPER_LIMIT = 1000;

void getMultiplesOfN(const unsigned int N, std::unordered_set<unsigned int>& multiples);

int main() {
  std::unordered_set<unsigned int> multiples = {};

  getMultiplesOfN(5, multiples);
  getMultiplesOfN(3, multiples);

  const unsigned int SUM = std::accumulate(multiples.begin(), multiples.end(), (unsigned int)0);
  std::cout << "Sum: " << SUM << '\n';

  return 0;
}

void getMultiplesOfN(const unsigned int N, std::unordered_set<unsigned int>& multiples) {
  for (unsigned int multiple = N; multiple < UPPER_LIMIT; multiple += N) {
    multiples.insert(multiple);
  }
}
