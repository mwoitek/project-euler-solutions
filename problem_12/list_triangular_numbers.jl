function compute_triangular_number(n::Int)
    Int((n * (n + 1)) / 2)
end

function create_list(list_size::Int)
    triangular_numbers = Int[]
    count = 0
    n = 1
    while count < list_size
        push!(triangular_numbers, compute_triangular_number(n))
        count += 1
        n += 1
    end
    triangular_numbers
end

function write_output(triangular_numbers::Vector{Int})
    open("triangular_numbers.txt", "w") do f
        for triangular_number in triangular_numbers
            write(f, "$triangular_number\n")
        end
    end
end

list_size = parse(Int, ARGS[1])
triangular_numbers = create_list(list_size)
write_output(triangular_numbers)
