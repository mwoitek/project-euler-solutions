# Highly divisible triangular number
# https://projecteuler.net/problem=12

function read_numbers()
    numbers = open("triangular_numbers.txt", "r") do f
        parse.(Int, readlines(f))
    end
    numbers
end

function count_divisors(num::Int)
    num == 1 && return 1
    count = 2
    d = 2
    while d^2 <= num
        if num % d == 0
            count += d != num / d ? 2 : 1
        end
        d += 1
    end
    count
end

function get_highly_divisible_number(triangular_numbers::Vector{Int}, num_divisors::Int)
    i = 1
    len = length(triangular_numbers)
    while i <= len && count_divisors(triangular_numbers[i]) <= num_divisors
        i += 1
    end
    i <= len ? triangular_numbers[i] : -1
end

triangular_numbers = read_numbers()
num_divisors = parse(Int, ARGS[1])
answer = get_highly_divisible_number(triangular_numbers, num_divisors)
if answer != -1
    println("Highly divisible triangular number: $answer")
else
    println("Could not find the number!")
end
