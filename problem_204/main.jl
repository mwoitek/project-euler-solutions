# Generalised Hamming Numbers
# https://projecteuler.net/problem=204

# julia --threads 4 main.jl 1000000000 100
# 9451.90s user 345.00s system 313% cpu 52:00.65 total

import Primes

function is_hamming_number(number::Int, type::Int = 5)
    prime_factors = Primes.factor(Set, number)
    all(Iterators.map(≤(type), prime_factors))
end

function count_hamming_numbers(upper_limit::Int, type::Int = 5)
    cnt = Threads.Atomic{Int}(0)
    Threads.@threads for number in 1:upper_limit
        is_hamming_number(number, type) && Threads.atomic_add!(cnt, 1)
    end
    cnt[]
end

function main()
    upper_limit = parse(Int, ARGS[1])
    type = parse(Int, ARGS[2])
    cnt = count_hamming_numbers(upper_limit, type)
    println("Count of generalised Hamming numbers of type $(type): $(cnt)")
end

main()
