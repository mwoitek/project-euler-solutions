# Number letter counts
# https://projecteuler.net/problem=17

function create_numbers_dict()
    numbers_dict = Dict(
        1 => "one",
        2 => "two",
        3 => "three",
        4 => "four",
        5 => "five",
        6 => "six",
        7 => "seven",
        8 => "eight",
        9 => "nine",
        10 => "ten",
        11 => "eleven",
        12 => "twelve",
        13 => "thirteen",
        14 => "fourteen",
        15 => "fifteen",
        16 => "sixteen",
        17 => "seventeen",
        18 => "eighteen",
        19 => "nineteen",
        20 => "twenty",
        30 => "thirty",
        40 => "forty",
        50 => "fifty",
        60 => "sixty",
        70 => "seventy",
        80 => "eighty",
        90 => "ninety",
        1000 => "one thousand"
    )

    for i = 21:99
        if haskey(numbers_dict, i)
            continue
        end
        last_digit = i % 10
        numbers_dict[i] = "$(numbers_dict[i-last_digit])-$(numbers_dict[last_digit])"
    end

    for i = 100:999
        i_str = string(i)
        digit_1 = parse(Int, i_str[1])
        numbers_dict[i] = "$(numbers_dict[digit_1]) hundred"
        digits_23 = parse(Int, i_str[2:3])
        if digits_23 > 0
            numbers_dict[i] *= " and $(numbers_dict[digits_23])"
        end
    end

    numbers_dict
end

function count_letters(str::String)
    length([char for char in str if char != ' ' && char != '-'])
end

numbers_dict = create_numbers_dict()
num_letters = sum(count_letters.(values(numbers_dict)))
println("Total number of letters: $num_letters")
