# Permuted multiples
# https://projecteuler.net/problem=52

# julia main.jl 6 150000

function test_number(number::Int, max_factor::Int)
    number_digits = sort(digits(number))
    prods = map(f -> f * number, 2:max_factor)
    all(map(p -> sort(digits(p)) == number_digits, prods))
end

function find_solutions(max_factor::Int, upper_limit::Int)
    has_property = n::Int -> test_number(n, max_factor)
    Iterators.filter(has_property, 1:upper_limit)
end

function main()
    max_factor = parse(Int, ARGS[1])
    upper_limit = parse(Int, ARGS[2])
    solutions = find_solutions(max_factor, upper_limit)
    min_solution = minimum(solutions)
    println("Answer: $(min_solution)")
end

main()
