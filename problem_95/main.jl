# Amicable chains
# https://projecteuler.net/problem=95

using DataStructures: OrderedSet
using Transducers

function read_sums()
    sums_dict = Dict{Int,Int}()
    open("sum_proper_divisors_1_1000000.csv", "r") do file
        for line in eachline(file)
            n, sum_proper_divisors = parse.(Int, split(line, ","))
            sums_dict[n] = sum_proper_divisors
        end
    end
    sums_dict
end

function create_sequence(first_term::Int, sums_dict::Dict{Int,Int})
    seq = OrderedSet{Int}()
    term = first_term
    while 0 < term ≤ 1000000 && term ∉ seq
        push!(seq, term)
        term = sums_dict[term]
    end
    collect(seq)
end

function get_sequence_info(first_term::Int, sums_dict::Dict{Int,Int})
    seq = create_sequence(first_term, sums_dict)
    is_valid = first_term == sums_dict[last(seq)]
    len = is_valid ? length(seq) : -1
    smallest = is_valid ? minimum(seq) : -1
    (is_valid, len, smallest)
end

function find_solution()
    sums_dict = read_sums()
    info_tpls =
        1:1000000 |>
        Map(Base.Fix2(get_sequence_info, sums_dict)) |>
        Filter(first) |>
        Map((is_valid, len, smallest)::Tuple{Bool,Int,Int} -> (len, smallest)) |>
        collect
    _, idx_longest = findmax(first, info_tpls)
    last(info_tpls[idx_longest])
end

function main()
    solution = find_solution()
    println("Solution: ", solution)
end

main()
