# julia sum_proper_divisors.jl 1 1000000

using Primes: factor
using Transducers

σ(n::Int) = foldl(
    *,
    n |> factor |> pairs |> Map((p, m)::Pair{Int,Int} -> (p^(m + 1) - 1) ÷ (p - 1));
    init = 1,
)

sum_proper_divisors(n::Int) = σ(n) - n

create_list(n_min::Int, n_max::Int) =
    collect(Zip(Map(identity), Map(sum_proper_divisors)), n_min:n_max)

function write_list(lst::Vector{Tuple{Int,Int}}, file_name::String)
    open(file_name, "w") do file
        foreach(t -> println(file, first(t), ",", last(t)), lst)
    end
end

function main()
    n_min = parse(Int, ARGS[1])
    n_max = parse(Int, ARGS[2])
    lst = create_list(n_min, n_max)
    file_name = "sum_proper_divisors_$(n_min)_$(n_max).csv"
    write_list(lst, file_name)
end

main()
