# Diophantine equation
# https://projecteuler.net/problem=66

# julia main.jl 1000

function get_values_of_d(upper_limit::UInt)
    squares = Set{UInt}(
        collect(
            Iterators.takewhile(
                ≤(upper_limit),
                Iterators.map(n -> n^2, Iterators.countfrom(1, 1)),
            ),
        ),
    )
    filter(n -> n ∉ squares, one(UInt):upper_limit)
end

# Only works if number is irrational
function continued_fraction_coefficients(number::Real)
    r = number
    Channel() do channel
        while true
            coefficient = (UInt ∘ floor)(r)
            put!(channel, coefficient)
            r = 1 / (r - coefficient)
        end
    end
end

function compute_convergents(number::Real)
    h1, h2 = BigInt(0), BigInt(1)
    k1, k2 = BigInt(1), BigInt(0)
    Channel() do channel
        for coefficient in continued_fraction_coefficients(number)
            h3 = h1 + coefficient * h2
            k3 = k1 + coefficient * k2
            put!(channel, (x = h3, y = k3))
            h1, h2 = h2, h3
            k1, k2 = k2, k3
        end
    end
end

is_solution(x::Integer, y::Integer, d::Integer) = x^2 - d * y^2 == 1

function solutions_pell_equation(d::UInt)
    Channel() do channel
        for c in compute_convergents((sqrt ∘ BigFloat)(d))
            is_solution(c.x, c.y, d) && put!(channel, c)
        end
    end
end

minimal_solution_pell_equation(d::UInt) =
    (first ∘ collect)(Iterators.take(solutions_pell_equation(d), 1))

function main()
    upper_limit = parse(UInt, ARGS[1])
    ds = get_values_of_d(upper_limit)

    solutions = map(minimal_solution_pell_equation, ds)
    xs = map(s -> s.x, solutions)
    x_max, idx_max = findmax(xs)
    d_max = ds[idx_max]

    println("Answer:")
    println("D = $(d_max)")
    println("x = $(x_max)")
end

main()
