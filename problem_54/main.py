# Poker hands
# https://projecteuler.net/problem=54

from enum import Enum
from enum import IntEnum
from functools import total_ordering
from itertools import pairwise

from more_itertools import windowed

CardValue = IntEnum(
    "CardValue",
    ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"],
)

CardSuit = Enum("CardSuit", ["C", "D", "H", "S"])

HandRank = IntEnum(
    "HandRank",
    [
        "High Card",
        "One Pair",
        "Two Pairs",
        "Three of a Kind",
        "Straight",
        "Flush",
        "Full House",
        "Four of a Kind",
        "Straight Flush",
        "Royal Flush",
    ],
)


@total_ordering
class Card:
    def __init__(self, value, suit):
        self.value = CardValue[value]
        self.suit = CardSuit[suit]

    def __str__(self):
        return f"{self.value._name_}{self.suit._name_}"

    def __eq__(self, other):
        return self.value == other.value and self.suit == other.suit

    def __lt__(self, other):
        return self.value < other.value


class Hand:
    def __init__(self, hand_str):
        self.cards = sorted([Card(c[0], c[1]) for c in hand_str.split(" ")])

    def __str__(self):
        return " ".join(map(str, self.cards))

    def is_straight(self):
        values = map(lambda c: c.value, self.cards)
        return all(map(lambda p: p[0] + 1 == p[1], pairwise(values)))

    def is_flush(self):
        suits = map(lambda c: c.suit, self.cards)
        return all(map(lambda p: p[0] == p[1], pairwise(suits)))

    def is_straight_flush(self):
        return self.is_straight() and self.is_flush()

    def is_royal_flush(self):
        return self.cards[0].value == CardValue["T"] and self.is_straight_flush()

    @staticmethod
    def same_value(cards_itr):
        values = map(lambda c: c.value, cards_itr)
        return all(map(lambda p: p[0] == p[1], pairwise(values)))

    def is_one_pair(self):
        return any(map(Hand.same_value, windowed(self.cards, 2)))

    def is_three_of_a_kind(self):
        return any(map(Hand.same_value, windowed(self.cards, 3)))

    def is_four_of_a_kind(self):
        return any(map(Hand.same_value, windowed(self.cards, 4)))

    def is_full_house(self):
        return any(
            [
                Hand.same_value(self.cards[:2]) and Hand.same_value(self.cards[2:]),
                Hand.same_value(self.cards[:3]) and Hand.same_value(self.cards[3:]),
            ]
        )

    def is_two_pairs(self):
        test_1 = Hand.same_value(self.cards[:2])
        if test_1 and Hand.same_value(self.cards[2:-1]):
            return True
        test_2 = Hand.same_value(self.cards[3:])
        if test_1 and test_2:
            return True
        return test_2 and Hand.same_value(self.cards[1:3])

    def classify(self):
        methods_dict = {
            "is_royal_flush": HandRank["Royal Flush"],
            "is_straight_flush": HandRank["Straight Flush"],
            "is_flush": HandRank["Flush"],
            "is_straight": HandRank["Straight"],
            "is_four_of_a_kind": HandRank["Four of a Kind"],
            "is_full_house": HandRank["Full House"],
            "is_three_of_a_kind": HandRank["Three of a Kind"],
            "is_two_pairs": HandRank["Two Pairs"],
            "is_one_pair": HandRank["One Pair"],
        }
        for method, rank in methods_dict.items():
            if getattr(self, method)():
                self.rank = rank
                break
        if not hasattr(self, "rank"):
            self.rank = HandRank["High Card"]
        return self.rank

    def highest_value(self):
        match self.rank._name_:
            case "Four of a Kind":
                return self.cards[1].value
            case "Full House" | "Three of a Kind":
                three_cards = next(filter(Hand.same_value, windowed(self.cards, 3)))
                return three_cards[0].value  # type: ignore
            case "Two Pairs":
                pairs = filter(Hand.same_value, windowed(self.cards, 2))
                pair_1, pair_2 = next(pairs), next(pairs)
                return max(pair_1[0].value, pair_2[0].value)  # type: ignore
            case "One Pair":
                pair = next(filter(Hand.same_value, windowed(self.cards, 2)))
                return pair[0].value  # type: ignore
            case _:
                return self.cards[-1].value


def get_winner(game_str):
    hand_1 = Hand(game_str[:14])
    hand_2 = Hand(game_str[15:])
    rank_1 = hand_1.classify()
    rank_2 = hand_2.classify()
    if rank_1 != rank_2:
        return 1 if rank_1 > rank_2 else 2
    highest_value_1 = hand_1.highest_value()
    highest_value_2 = hand_2.highest_value()
    if highest_value_1 != highest_value_2:
        return 1 if highest_value_1 > highest_value_2 else 2
    uniq_values_1 = set(map(lambda c: c.value, hand_1.cards))
    uniq_values_2 = set(map(lambda c: c.value, hand_2.cards))
    max_1 = max(uniq_values_1)
    max_2 = max(uniq_values_2)
    while max_1 == max_2:
        uniq_values_1.remove(max_1)
        uniq_values_2.remove(max_2)
        max_1 = max(uniq_values_1)
        max_2 = max(uniq_values_2)
    return 1 if max_1 > max_2 else 2


def get_answer():
    cnt = 0
    with open("p054_poker.txt", "r") as file:
        for line in file:
            winner = get_winner(line)
            cnt += 1 if winner == 1 else 0
    return cnt


if __name__ == "__main__":
    answer = get_answer()
    print(f"Answer: {answer}")
