# Square digit chains
# https://projecteuler.net/problem=92

using Memoize

const squares_dict = Dict(i => i^2 for i in 0:9)
const upper_limit = 9999999

sum_square_of_digits(number::Int) = sum(d -> squares_dict[d], digits(number))

@memoize function last_number_in_chain(number::Int)
    (number == 89 || number == 1) && return number
    (last_number_in_chain ∘ sum_square_of_digits)(number)
end

function main()
    answer = count(n -> n == 89, Iterators.map(last_number_in_chain, 1:upper_limit))
    println("Answer: $(answer)")
end

main()
