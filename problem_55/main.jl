# Lychrel numbers
# https://projecteuler.net/problem=55

const max_iterations = 49
const upper_limit = 9999

function reverse_and_add(number::BigInt)
    reversed_number = parse(BigInt, (reverse ∘ string)(number))
    number + reversed_number
end

function is_palindrome(number::BigInt)
    number_str = string(number)
    number_str == reverse(number_str)
end

function is_lychrel(number::BigInt)
    number_to_test = number
    for _ in 1:max_iterations
        number_to_test = reverse_and_add(number_to_test)
        is_palindrome(number_to_test) && return false
    end
    true
end

function main()
    answer = count(identity, Iterators.map(is_lychrel, zero(BigInt):upper_limit))
    println("Answer: $(answer)")
end

main()
