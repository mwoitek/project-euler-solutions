struct Point
    x::Int
    y::Int
    Point(coords::String) = new(parse.(Int, split(coords, ","))...)
end

Base.:(==)(p1::Point, p2::Point) = p1.x == p2.x && p1.y == p2.y

Base.isless(p1::Point, p2::Point) = p1.x < p2.x || (p1.x == p2.x && p1.y < p2.y)

distance(p1::Point, p2::Point) = sqrt((p1.x - p2.x)^2 + (p1.y - p2.y)^2)
