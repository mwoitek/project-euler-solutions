using Printf: @printf

include("points.jl")

function read_points(num_points = nothing)
    lines = eachline("list_of_points.csv")
    points_itr = Iterators.map(Point, lines)
    itr = isa(num_points, Integer) ? Iterators.take(points_itr, num_points) : points_itr
    (sort ∘ collect)(itr)
end

function shortest_distance(points::Vector{Point})
    pairs_of_points = Iterators.product(points, points)
    distinct_pairs =
        Iterators.filter((p1, p2)::Tuple{Point,Point} -> isless(p1, p2), pairs_of_points)
    distances =
        Iterators.map((p1, p2)::Tuple{Point,Point} -> distance(p1, p2), distinct_pairs)
    minimum(distances)
end

function main()
    points = read_points(14)
    sd = shortest_distance(points)
    @printf "Shortest distance: %.9f\n" sd
end

main()
