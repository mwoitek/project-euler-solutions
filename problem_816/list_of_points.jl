function random_number_generator(ch::Channel)
    s = 290797
    while true
        put!(ch, s)
        s = powermod(s, 2, 50515093)
    end
end

function write_points(num_points)
    rng = Channel(random_number_generator)
    open("list_of_points.csv", "w") do file
        for _ in 1:num_points
            x = take!(rng)
            y = take!(rng)
            println(file, x, ",", y)
        end
    end
end

function main()
    num_points = parse(Int, ARGS[1])
    write_points(num_points)
end

main()
