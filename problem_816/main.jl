# Shortest distance among points
# https://projecteuler.net/problem=816

# https://cp-algorithms.com/geometry/nearest_points.html

using Printf: @printf

include("points.jl")

function read_points(num_points = nothing)
    lines = eachline("list_of_points.csv")
    points_itr = Iterators.map(Point, lines)
    itr = isa(num_points, Integer) ? Iterators.take(points_itr, num_points) : points_itr
    (sort ∘ collect)(itr)
end

mutable struct MinDist
    dist::Float64
    MinDist() = new(floatmax(Float64))
end

function update_min_dist!(md::MinDist, dist::Float64)
    if dist < md.dist
        md.dist = dist
    end
end

function shortest_distance_naive(points::Vector{Point})
    pairs_of_points = Iterators.product(points, points)
    distinct_pairs =
        Iterators.filter((p1, p2)::Tuple{Point,Point} -> isless(p1, p2), pairs_of_points)
    distances =
        Iterators.map((p1, p2)::Tuple{Point,Point} -> distance(p1, p2), distinct_pairs)
    minimum(distances)
end

function sort_slice!(points::Vector{Point}, r::UnitRange)
    points[r] = sort(points[r], by = p -> p.y)
end

midpoint(low::Int, high::Int) = low + (high - low) ÷ 2

function merge_sorted!(points::Vector{Point}, low::Int, mid::Int, high::Int)
    merged = similar(points, high - low + 1)
    i = 1
    j = low
    k = mid + 1
    while j ≤ mid && k ≤ high
        if points[j].y ≤ points[k].y
            merged[i] = points[j]
            j += 1
        else
            merged[i] = points[k]
            k += 1
        end
        i += 1
    end
    merged[i:end] = j > mid ? points[k:high] : points[j:mid]
    points[low:high] = merged
end

function shortest_distance(points::Vector{Point})
    md = MinDist()

    function rec(low::Int, high::Int)
        if high - low ≤ 3 # 4 points or less
            dist = shortest_distance_naive(points[low:high])
            update_min_dist!(md, dist)
            sort_slice!(points, low:high)
            return
        end

        mid = midpoint(low, high)
        x_mid = points[mid].x
        rec(low, mid)
        rec(mid + 1, high)
        merge_sorted!(points, low, mid, high)

        b = filter(p -> abs(p.x - x_mid) < md.dist, points[low:high])
        for p1 in b
            c = Iterators.filter(p2 -> p1 ≠ p2 && 0 ≤ p1.y - p2.y < md.dist, b)
            dist = minimum(
                Iterators.map(p2 -> distance(p1, p2), c);
                init = floatmax(Float64),
            )
            update_min_dist!(md, dist)
        end
    end

    rec(firstindex(points), lastindex(points))
    md.dist
end

function main()
    points = read_points()
    sd = shortest_distance(points)
    @printf "Shortest distance: %.9f\n" sd
end

main()
