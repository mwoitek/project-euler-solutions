# How many reversible numbers are there below one-billion?
# https://projecteuler.net/problem=145

# julia --threads 3 main.jl 999999999

function reverse_number(number::Int)
    number_str = (join ∘ digits)(number)
    parse(Int, number_str), startswith(number_str, "0")
end

function is_reversible(number::Int)
    reversed_number, starts_with_zero = reverse_number(number)
    starts_with_zero && return false
    sum_ = number + reversed_number
    all(Iterators.map(d -> mod(d, 2) != 0, digits(sum_)))
end

function count_reversible_numbers(upper_limit::Int)
    cnt = Threads.Atomic{Int}(0)
    Threads.@threads for number in 1:upper_limit
        is_reversible(number) && Threads.atomic_add!(cnt, 1)
    end
    cnt[]
end

function main()
    upper_limit = parse(Int, ARGS[1])
    cnt = count_reversible_numbers(upper_limit)
    println("Count of reversible numbers: ", cnt)
end

main()
