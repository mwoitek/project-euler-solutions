# Largest product in a grid
# https://projecteuler.net/problem=11

function read_numbers()
    lines = open("grid.txt", "r") do f
        readlines(f)
    end
    num_lines = length(lines)

    grid = zeros(Int, num_lines, num_lines)
    for (row, line) in enumerate(lines)
        grid[row, :] = parse.(Int, split(line))
    end
    grid
end

function compute_max_product_right(grid::AbstractArray{Int,2}, slice_size::Int)
    prods = Int[]
    dim = size(grid)[1]
    size_1 = slice_size - 1
    last_col = dim - size_1

    for row = 1:dim
        for col = 1:last_col
            vals = [grid[row, col+i] for i = 0:size_1]
            push!(prods, prod(vals))
        end
    end

    maximum(prods)
end

function compute_max_product_down(grid::AbstractArray{Int,2}, slice_size::Int)
    compute_max_product_right(transpose(grid), slice_size)
end

is_valid_index(idx::Tuple{Int,Int}, dim::Int) = idx[1] ≤ dim && 1 ≤ idx[2] && idx[2] ≤ dim

function compute_max_product_diagonals(grid::AbstractArray{Int,2}, slice_size::Int)
    prods = Int[]
    dim = size(grid)[1]
    size_1 = slice_size - 1

    for row = 1:dim
        for col = 1:dim
            prod_1 = is_valid_index((row + size_1, col + size_1), dim) ? prod([grid[row+i, col+i] for i = 0:size_1]) : -1
            prod_2 = is_valid_index((row + size_1, col - size_1), dim) ? prod([grid[row+i, col-i] for i = 0:size_1]) : -1
            push!(prods, max(prod_1, prod_2))
        end
    end

    maximum(prods)
end

function compute_max_product(grid::AbstractArray{Int,2}, slice_size::Int)
    maximum([
        compute_max_product_right(grid, slice_size),
        compute_max_product_down(grid, slice_size),
        compute_max_product_diagonals(grid, slice_size)
    ])
end

function main()
    grid = read_numbers()
    slice_size = 4
    max_product = compute_max_product(grid, slice_size)
    println("Largest product: $max_product")
end

main()
