# Lexicographic permutations
# https://projecteuler.net/problem=24

from itertools import permutations
from typing import Optional


def get_nth_permutation(num_digits: int, n: int) -> Optional[str]:
    for c, p in enumerate(permutations(range(num_digits)), start=1):
        if c == n:
            return "".join([str(d) for d in p])
    return None


def main():
    permutation = get_nth_permutation(10, 10**6)
    if permutation is not None:
        print(f"Permutation: {permutation}")
    else:
        print("Could not find permutation!")


if __name__ == "__main__":
    main()
