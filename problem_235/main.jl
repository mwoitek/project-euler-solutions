# An Arithmetic Geometric sequence
# https://projecteuler.net/problem=235

# https://en.wikipedia.org/wiki/Arithmetico-geometric_sequence#Sum_of_the_terms

using Printf

f(r::AbstractFloat) =
    r^5000 * (4700 * r - 4701) - 2 * 10^11 * r^2 + 400000000300 * r - 200000000299

f_prime(r::AbstractFloat) =
    100 * (3 * r^4999 * (78349 * r - 78350) - 4 * 10^9 * r + 4000000003)

sequence_sum(r::AbstractFloat) =
    (897 + 14103 * r^5000) / (1 - r) - 3 * r * (1 - r^5000) / (1 - r)^2

function newton_method(r_0::AbstractFloat, num_iterations::Int)
    r = r_0
    for _ in 1:num_iterations
        r -= f(r) / f_prime(r)
    end
    r
end

function main()
    r_0 = BigFloat(1.1)
    num_iterations = 10000
    r = newton_method(r_0, num_iterations)
    println("r = ", r)
    println("Sequence sum = ", sequence_sum(r))
    @printf "Answer: %.12f\n" r
end

main()
