# Roman numerals
# https://projecteuler.net/problem=89

from operator import itemgetter

CONVERSION_DICT = {
    "I": 1,
    "IV": 4,
    "V": 5,
    "IX": 9,
    "X": 10,
    "XL": 40,
    "L": 50,
    "XC": 90,
    "C": 100,
    "CD": 400,
    "D": 500,
    "CM": 900,
    "M": 1000,
}


def read_numerals() -> list[str]:
    with open("p089_roman.txt", "r") as file:
        return [line.strip() for line in file.readlines()]


def roman_to_int(roman: str) -> int:
    num = 0
    last_letter = "I"
    for letter in roman[::-1]:
        if CONVERSION_DICT[letter] < CONVERSION_DICT[last_letter]:
            num -= CONVERSION_DICT[letter]
        else:
            num += CONVERSION_DICT[letter]
        last_letter = letter
    return num


def int_to_roman(num: int) -> str:
    roman = ""
    rem = num
    while rem > 0:
        itr = filter(lambda p: p[1] <= rem, CONVERSION_DICT.items())
        k, v = max(itr, key=itemgetter(1))
        roman += k
        rem -= v
    return roman


def count_saved_characters() -> int:
    romans = read_numerals()
    count_before = sum(map(len, romans))
    itr_1 = map(roman_to_int, romans)
    itr_2 = map(int_to_roman, itr_1)
    count_after = sum(map(len, itr_2))
    return count_before - count_after


if __name__ == "__main__":
    count = count_saved_characters()
    print(f"Answer: {count}")
