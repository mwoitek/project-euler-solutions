# Totient maximum
# https://projecteuler.net/problem=69

# julia main.jl list_prime_factors_2_1000000.txt

function create_prime_factors_dict(file_name::String)
    prime_factors_dict = Dict{Int,Vector{Int}}()
    lines = open(file_name, "r") do file
        readlines(file)
    end
    for line in lines
        line_parts = split(line, ":")
        number = parse(Int, line_parts[1])
        prime_factors = parse.(Int, split(line_parts[2], ","))
        prime_factors_dict[number] = prime_factors
    end
    prime_factors_dict
end

# https://en.wikipedia.org//wiki/Euler's_totient_function#Euler's_product_formula
function compute_totient_function(number::Int, prime_factors_dict::Dict{Int,Vector{Int}})
    product =
        prod(map(p -> one(Rational{Int}) - one(Int) // p, prime_factors_dict[number]))
    numerator(number * product)
end

function get_list_of_totient_values(prime_factors_dict::Dict{Int,Vector{Int}})
    phi = n::Int -> compute_totient_function(n, prime_factors_dict)
    Iterators.map(phi, keys(prime_factors_dict))
end

function get_max_ratio(prime_factors_dict::Dict{Int,Vector{Int}})
    totient_values = get_list_of_totient_values(prime_factors_dict)
    ratios =
        Iterators.map(p -> p[1] / p[2], zip(keys(prime_factors_dict), totient_values))
    reduce((x, y) -> x[2] >= y[2] ? x : y, zip(keys(prime_factors_dict), ratios))
end

function main()
    file_name = ARGS[1]
    prime_factors_dict = create_prime_factors_dict(file_name)
    n, ratio = get_max_ratio(prime_factors_dict)
    println("Answer:")
    println("n = $(n)")
    println("ratio = $(ratio)")
end

main()
