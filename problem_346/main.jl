# Strong Repunits
# https://projecteuler.net/problem=346

# julia main.jl 1000000000000
# 1.32s user 0.40s system 129% cpu 1.325 total

using IterTools: distinct, iterated

max_base(upper_limit::Int) = ceil(Int, (sqrt(4 * upper_limit - 3) - 1) / 2) - 1

function repunits_generator(b::Int, upper_limit::Int)
    repunits = Iterators.dropwhile(≤(b + 1), iterated(r -> b * r + 1, 1))
    Iterators.takewhile(<(upper_limit), repunits)
end

function strong_repunits(upper_limit::Int)
    b_max = max_base(upper_limit)
    repunits = Iterators.map(Base.Fix2(repunits_generator, upper_limit), 2:b_max)
    distinct(Iterators.flatten(repunits))
end

sum_strong_repunits(upper_limit::Int) = sum(strong_repunits(upper_limit); init = 1)

function main()
    upper_limit = parse(Int, ARGS[1])
    answer = sum_strong_repunits(upper_limit)
    println("Answer: ", answer)
end

main()
