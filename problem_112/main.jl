# Bouncy numbers
# https://projecteuler.net/problem=112

# julia main.jl 99 100

function compare_digit_pairs(number::Int, operator::Function)
    reversed_digits = digits(number)
    digit_pairs = Iterators.zip(reversed_digits[1:(end - 1)], reversed_digits[2:end])
    all(Iterators.map(p -> operator(p[1], p[2]), digit_pairs))
end

is_increasing(number::Int) = compare_digit_pairs(number, ≥)
is_decreasing(number::Int) = compare_digit_pairs(number, ≤)

is_bouncy(number::Int) = !is_increasing(number) && !is_decreasing(number)

function find_solution(proportion::Rational{Int})
    num_bouncy = 0
    for number in Iterators.countfrom(100, 1)
        num_bouncy += is_bouncy(number) ? 1 : 0
        num_bouncy // number == proportion && return number
    end
end

function main()
    num = parse(Int, ARGS[1])
    den = parse(Int, ARGS[2])
    proportion = num // den
    solution = find_solution(proportion)
    println("Solution: ", solution)
end

main()
