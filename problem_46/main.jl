# Goldbach's other conjecture
# https://projecteuler.net/problem=46

include("solution.jl")
include("utils.jl")

function get_composite_odd_numbers(primes::Set{UInt})
    odd_numbers = collect(UInt, 3:2:maximum(primes)-2)
    filter(n -> !(n ∈ primes), odd_numbers)
end

function find_solution(odd_number::UInt, primes::Vector{UInt})
    for prime in (p for p in primes if p < odd_number)
        i = one(UInt)
        t = prime + UInt(2) * i * i
        while t < odd_number
            i += one(UInt)
            t = prime + UInt(2) * i * i
        end
        t == odd_number && return Solution(odd_number, prime, i)
    end
    Solution(odd_number, 0, 0)
end

function find_solutions(composite_odd_numbers::Vector{UInt}, primes::Vector{UInt})
    solutions = Solution[]
    for odd_number in composite_odd_numbers
        s = find_solution(odd_number, primes)
        if is_valid_solution(s)
            push!(solutions, s)
        else
            break
        end
    end
    solutions
end

function find_smallest_odd(composite_odd_numbers::Vector{UInt}, solutions::Vector{Solution})
    last_ok_odd = solutions[end].odd_number
    idx = findfirst(isequal(last_ok_odd), composite_odd_numbers)
    composite_odd_numbers[idx+1]
end

function main()
    primes_arr = read_numbers("primes_less_than_10000.txt")
    primes_set = Set(primes_arr)
    composite_odd_numbers = get_composite_odd_numbers(primes_set)
    solutions = find_solutions(composite_odd_numbers, primes_arr)
    write_output(solutions, "solutions.txt")
    smallest_odd = find_smallest_odd(composite_odd_numbers, solutions)
    println("Smallest composite odd: $smallest_odd")
end

main()
