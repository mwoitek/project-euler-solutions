struct Solution
    odd_number::UInt
    prime::UInt
    i::UInt
end

function Base.show(io::IO, s::Solution)
    print(io, "$(s.odd_number) = $(s.prime) + 2 * $(s.i)^2")
end

function is_valid_solution(s::Solution)
    s.odd_number == s.prime + UInt(2) * s.i * s.i
end
