function read_numbers(file_name::String)
    numbers = open(file_name, "r") do file
        parse.(UInt, readlines(file))
    end
    numbers
end

function write_output(list, file_name::String)
    open(file_name, "w") do file
        for elem in list
            write(file, "$elem\n")
        end
    end
end
