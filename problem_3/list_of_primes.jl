function is_prime(num::Int64)
    if num <= 1
        return false
    elseif num <= 3
        return true
    end

    if num % 2 == 0 || num % 3 == 0
        return false
    end

    d = 5
    while d^2 <= num
        if num % d == 0 || num % (d + 2) == 0
            return false
        end
        d += 6
    end

    return true
end

function create_list_of_primes(list_size::Int64)
    primes = Int64[2]
    count = 1
    num = 3
    while count < list_size
        if is_prime(num)
            push!(primes, num)
            count += 1
        end
        num += 2
    end
    primes
end

function write_output(primes::Vector{Int64})
    open("primes.txt", "w") do f
        for prime in primes
            write(f, "$prime\n")
        end
    end
end

list_size = parse(Int64, ARGS[1])
primes = create_list_of_primes(list_size)
write_output(primes)
