# Largest prime factor
# https://projecteuler.net/problem=3

function get_prime_factors(num::Int64)
    factors = Int64[]

    primes = open("primes.txt", "r") do f
        parse.(Int64, readlines(f))
    end

    i = 1
    n = num

    while n > 1
        p = primes[i]
        if n % p == 0
            push!(factors, p)
            n /= p
        end
        i += 1
    end

    factors
end

num = 600851475143
factors = get_prime_factors(num)
println("Largest prime factor: $(factors[length(factors)])")
