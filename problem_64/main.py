# Odd period square roots
# https://projecteuler.net/problem=64

# python3 main.py 10000

from dataclasses import dataclass
from itertools import count
from itertools import takewhile
from math import floor
from math import sqrt
from sys import argv


@dataclass
class Triplet:
    m: int
    d: int
    a: int


def get_non_squares(max_radicand: int) -> list[int]:
    squares_itr = map(lambda n: n**2, count(0))
    squares_set = set(takewhile(lambda s: s <= max_radicand, squares_itr))
    return list(filter(lambda n: n not in squares_set, range(max_radicand + 1)))


# https://en.wikipedia.org/wiki/Periodic_continued_fraction#Canonical_form_and_repetend
def compute_period(radicand: int) -> int:
    a_0 = floor(sqrt(radicand))
    triplet = Triplet(0, 1, a_0)
    triplets = []
    while triplet not in triplets:
        triplets.append(triplet)
        m = triplet.d * triplet.a - triplet.m
        d = (radicand - m**2) // triplet.d
        a = floor((a_0 + m) / d)
        triplet = Triplet(m, d, a)
    assert triplets[-1].a == 2 * a_0
    return len(triplets) - 1


if __name__ == "__main__":
    max_radicand = int(argv[1])
    non_squares = get_non_squares(max_radicand)
    periods = map(compute_period, non_squares)
    num_odd_periods = sum(map(lambda p: p % 2 != 0, periods))
    print(f"Number of continued fractions with an odd period: {num_odd_periods}")
