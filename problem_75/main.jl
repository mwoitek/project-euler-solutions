# Singular integer right triangles
# https://projecteuler.net/problem=75

# 838.42s user 0.93s system 99% cpu 14:00.01 total

const max_perimeter = 1500000
const n_max = 500000

initialize_counts_dict() = Dict(p => 0 for p in 12:2:max_perimeter)

function get_pairs_of_divisors(n::Int)
    pairs_of_divisors = Tuple{Int,Int}[(n, 1)]
    q, r = divrem(n, 2)
    r == 0 && push!(pairs_of_divisors, (q, 2))
    stp = r == 0 ? 1 : 2
    d_max = isqrt(n) - 1
    for d in 3:stp:d_max
        q, r = divrem(n, d)
        r == 0 && push!(pairs_of_divisors, (q, d))
    end
    pairs_of_divisors
end

# https://arxiv.org/abs/1711.02379
function generate_triples(n::Int)
    if iseven(n)
        pairs_of_divisors = get_pairs_of_divisors((n ÷ 2)^2)
        triples = map((q, d)::Tuple{Int,Int} -> (n, q - d, q + d), pairs_of_divisors)
    else
        pairs_of_divisors = get_pairs_of_divisors(n^2)
        triples = map(
            (q, d)::Tuple{Int,Int} -> (n, (q - d) ÷ 2, (q + d) ÷ 2),
            pairs_of_divisors,
        )
    end
    filter((a, b, _)::Tuple{Int,Int,Int} -> a < b, triples)
end

function count_perimeters()
    counts_dict = initialize_counts_dict()
    for n in 3:n_max
        for triple in generate_triples(n)
            perimeter = sum(triple)
            perimeter > max_perimeter && continue
            counts_dict[perimeter] += 1
        end
    end
    counts_dict
end

function main()
    counts_dict = count_perimeters()
    solution = count(isequal(1), values(counts_dict))
    println("Solution: ", solution)
end

main()
