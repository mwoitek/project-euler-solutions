# Optimum polynomial
# https://projecteuler.net/problem=101

from functools import partial
from itertools import count
from itertools import dropwhile
from math import prod
from typing import Callable

from more_itertools import first
from sympy import Expr
from sympy import Integer
from sympy import Symbol
from sympy import degree
from sympy import expand
from sympy import lambdify


def tenth_degree_polynomial() -> Expr:
    sum_ = Integer(0)
    x = Symbol("x")
    for i in map(Integer, range(11)):
        sum_ += (-x) ** i
    return sum_


def generate_sequence(generating_func: Callable[[int], int], num_terms: int) -> list[int]:
    return list(map(generating_func, range(1, num_terms + 1)))


def lagrange_basis_polynomials(degree_: int) -> list[Expr]:
    x = Symbol("x")
    xs = list(range(1, degree_ + 2))

    def lagrange_basis_polynomial(i: int) -> Expr:
        return expand(
            prod([(x - xs[j]) / (xs[i] - xs[j]) for j in range(degree_ + 1) if i != j])
        )

    return list(map(lagrange_basis_polynomial, range(degree_ + 1)))


def lagrange_interpolating_polynomial(
    generating_func: Callable[[int], int],
    degree_: int,
) -> Expr:
    ys = generate_sequence(generating_func, degree_ + 1)
    basis_polynomials = lagrange_basis_polynomials(degree_)
    return expand(sum(map(lambda p: p[0] * p[1], zip(ys, basis_polynomials))))


def first_incorrect_term(generating_func: Callable[[int], int], degree_: int) -> int:
    interpolating_polynomial = lambdify(
        Symbol("x"),
        lagrange_interpolating_polynomial(generating_func, degree_),
    )
    seq_1 = (interpolating_polynomial(i) for i in count(degree_ + 2))
    seq_2 = (generating_func(i) for i in count(degree_ + 2))
    return first(dropwhile(lambda p: p[0] == p[1], zip(seq_1, seq_2)))[0]


if __name__ == "__main__":
    x = Symbol("x")
    # func_expr = x**3
    func_expr = tenth_degree_polynomial()
    func_lambdified = lambdify(x, func_expr)
    fit = partial(first_incorrect_term, func_lambdified)
    degrees = range(int(degree(func_expr)))
    sum_of_fits = sum(map(fit, degrees))
    print(f"Sum of FITs: {sum_of_fits}")
