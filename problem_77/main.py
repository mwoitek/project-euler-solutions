# Prime summations
# https://projecteuler.net/problem=77

from itertools import count
from itertools import dropwhile
from itertools import takewhile
from math import prod

from sympy import Symbol
from sympy import expand
from sympy import series

PRIMES_FILE_NAME = "/home/woitek/repos/project_euler/primes/primes.txt"


def get_primes(upper_limit: int) -> list[int]:
    with open(PRIMES_FILE_NAME, "r") as file:
        primes = map(int, file)
        return list(takewhile(lambda p: p <= upper_limit, primes))


def count_summations(number: int) -> int:
    primes = get_primes(number)
    x = Symbol("x")
    factors = map(lambda p: series(1 / (1 - x**p), n=number + 1), primes)
    generating_function = expand(prod(factors))
    return generating_function.as_coefficients_dict()[x**number]


def get_answer(num_ways: int) -> int:
    counts = map(lambda n: (n, count_summations(n)), count(2))
    itr = dropwhile(lambda c: c[1] <= num_ways, counts)
    return next(itr)[0]


if __name__ == "__main__":
    num_ways = 5000
    answer = get_answer(num_ways)
    print(f"Answer: {answer}")
