# Prime digit replacements
# https://projecteuler.net/problem=51

# julia main.jl 2 1000000
# 8.45s user 0.36s system 104% cpu 8.433 total

using Combinatorics: combinations
using IterTools: iterated
using Primes: isprime, nextprime

const family_size = 8

function primes_itr(lo::T, hi::T) where {T<:Integer}
    itr = iterated(p -> nextprime(p + oneunit(lo)), nextprime(lo))
    Iterators.takewhile(≤(hi), itr)
end

get_digits(n::Integer) = reverse(digits(typeof(n), n))

function join_digits(digits_arr::Vector{T}) where {T<:Integer}
    parse(T, join(digits_arr))
end

function get_solution(p::Integer)
    digits_arr = get_digits(p)
    candidate_digits = similar(digits_arr)
    for idxs in combinations(collect(Int, eachindex(digits_arr)))
        v = @view digits_arr[idxs]
        all(Iterators.map(isequal(first(v)), v)) || continue
        primes = Set(similar(digits_arr, 0))
        candidate_digits = copy(digits_arr)
        for digit in zero(p):oftype(p, 9)
            candidate_digits[idxs] .= digit
            first(candidate_digits) == zero(p) && continue
            candidate = join_digits(candidate_digits)
            isodd(candidate) && isprime(candidate) && push!(primes, candidate)
        end
        length(primes) == family_size && return minimum(primes)
    end
    zero(p)
end

function main()
    lo = parse(UInt, ARGS[1])
    hi = parse(typeof(lo), ARGS[2])
    primes = primes_itr(lo, hi)
    solutions_itr = Iterators.map(get_solution, primes)
    solutions_set = Set(Iterators.filter(≠(zero(lo)), solutions_itr))
    answer = minimum(solutions_set)
    println("Answer: ", answer)
end

main()
