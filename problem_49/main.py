# Prime permutations
# https://projecteuler.net/problem=49

from itertools import combinations
from itertools import permutations

from more_itertools import pairwise


def read_primes() -> set[int]:
    with open("primes_less_than_10000.txt", mode="r") as file:
        return {int(line.strip()) for line in file}


def filter_by_num_digits(numbers: set[int], num_digits: int) -> list[int]:
    return list(
        filter(
            lambda n: len(str(n)) == num_digits,
            sorted(numbers),
        )
    )


def get_permutations(number: int) -> set[int]:
    return {int("".join(p)) for p in permutations(str(number))}


def get_permutations_num_digits(number: int, num_digits: int) -> list[int]:
    return filter_by_num_digits(
        get_permutations(number),
        num_digits,
    )


def is_prime(number: int, primes: set[int]) -> bool:
    return number in primes


def filter_primes(numbers: list[int], primes: set[int]) -> list[int]:
    return list(
        filter(
            lambda n: is_prime(n, primes),
            numbers,
        )
    )


def get_prime_permutations(
    number: int,
    num_digits: int,
    primes: set[int],
) -> list[int]:
    return filter_primes(
        get_permutations_num_digits(number, num_digits),
        primes,
    )


def is_arithmetic_sequence(numbers: list[int]) -> bool:
    if len(numbers) < 3:
        return False
    diff = numbers[1] - numbers[0]
    return all(
        map(
            lambda p: p[1] - p[0] == diff,
            pairwise(numbers[1:]),
        )
    )


if __name__ == "__main__":
    NUM_DIGITS = 4
    SEQ_LENGTH = 3

    primes = read_primes()
    four_digit_primes = filter_by_num_digits(primes, NUM_DIGITS)

    solutions = set()
    for prime in four_digit_primes:
        permutations_ = get_prime_permutations(prime, NUM_DIGITS, primes)
        for idxs in combinations(range(len(permutations_)), SEQ_LENGTH):
            numbers = [permutations_[idx] for idx in idxs]
            if is_arithmetic_sequence(numbers):
                solutions.add(",".join([str(n) for n in numbers]))

    print("Solutions:")
    for solution in sorted(solutions):
        print(solution)

    solutions_lst = list(sorted(solutions))
    answer = solutions_lst[1].replace(",", "")
    print(f"Answer: {answer}")
