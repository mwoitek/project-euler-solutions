# Prime square remainders
# https://projecteuler.net/problem=123

using Transducers

const primes_file_name = "/home/woitek/repos/project_euler/primes/primes.txt"

function primes_generator(ch::Channel{Tuple{Int,Int}})
    open(primes_file_name, "r") do file
        for (n, line) in Iterators.enumerate(eachline(file))
            p = parse(Int, line)
            put!(ch, (n, p))
        end
    end
end

function compute_remainder((n, p)::Tuple{Int,Int})
    p2 = widemul(p, p)
    t1 = powermod(p - 1, n, p2)
    t2 = powermod(p + 1, n, p2)
    mod(t1 + t2, p2)
end

function find_solution(num_to_exceed::Int)
    arr =
        Channel{Tuple{Int,Int}}(primes_generator) |>
        Zip(Map(first), Map(compute_remainder)) |>
        Filter(>(num_to_exceed) ∘ last) |>
        Take(1) |>
        collect
    !isempty(arr) ? (first ∘ first)(arr) : -1
end

function main()
    num_to_exceed = parse(Int, ARGS[1])
    n = find_solution(num_to_exceed)
    println("Answer:")
    println("n = ", n)
end

main()
