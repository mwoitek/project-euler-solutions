# Consecutive prime sum
# https://projecteuler.net/problem=50

function read_primes()
    primes = open("primes_less_than_1000000.txt", "r") do file
        parse.(Int, readlines(file))
    end
    primes
end

function find_prime_sums()
    prime_sums = Tuple{Int,Int}[]
    primes_lst = read_primes()
    primes_set = Set(primes_lst)
    i = 1
    while i ≤ lastindex(primes_lst)
        j = i + 1
        while j ≤ lastindex(primes_lst)
            s = sum(primes_lst[i:j])
            s > last(primes_lst) && break
            s ∈ primes_set && push!(prime_sums, (s, j - i + 1))
            j += 1
        end
        i += 1
    end
    prime_sums
end

function main()
    prime_sums = find_prime_sums()
    num_terms, idx = findmax(last, prime_sums)
    prime = first(prime_sums[idx])
    println("Answer:")
    println("Prime: ", prime)
    println("Number of terms: ", num_terms)
end

main()
