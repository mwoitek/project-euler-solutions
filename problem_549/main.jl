# Divisibility of factorials
# https://projecteuler.net/problem=549

# julia -t4 main.jl 100000000
# 397.16s user 11.60s system 335% cpu 2:01.69 total

using Primes: factor
using Transducers: Map, foldxt

function smallest_multiple(prime, multiplicity)
    multiple = prime
    power_of_prime = prime^multiplicity
    product = mod(prime, power_of_prime)
    while product ≠ 0
        multiple += prime
        product = mod(multiple * product, power_of_prime)
    end
    multiple
end

function s(n)
    m = -1
    factors_dict = factor(n)
    for (prime, multiplicity) in pairs(factors_dict)
        multiple = smallest_multiple(prime, multiplicity)
        if multiple > m
            m = multiple
        end
    end
    m
end

compute_sum(n) = foldxt(+, Map(s), 2:n; init = 0)

function main()
    n = parse(Int, ARGS[1])
    answer = compute_sum(n)
    println("Answer: ", answer)
end

main()
