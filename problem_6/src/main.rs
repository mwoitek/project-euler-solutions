/*
Sum square difference
https://projecteuler.net/problem=6
*/

fn main() {
    const UPPER_LIMIT: u32 = 100;
    let sum: u32 = compute_sum(UPPER_LIMIT);
    let sum_squares: u32 = compute_sum_squares(UPPER_LIMIT);
    let diff: u32 = sum * sum - sum_squares;
    println!("Difference: {}", diff);
}

fn compute_sum(upper_limit: u32) -> u32 {
    let mut sum: u32 = 0;
    for num in 1..=upper_limit {
        sum += num;
    }
    sum
}

fn compute_sum_squares(upper_limit: u32) -> u32 {
    let mut sum: u32 = 0;
    for num in 1..=upper_limit {
        sum += num * num;
    }
    sum
}
