# Truncatable primes
# https://projecteuler.net/problem=37

include("utils.jl")

is_prime(number::UInt, primes::Set{UInt}) = number ∈ primes

function test_left_to_right(number::UInt, primes::Set{UInt})
    number_str = string(number)
    len = length(number_str)
    idx = 2
    while idx ≤ len && is_prime(parse(UInt, number_str[idx:end]), primes)
        idx += 1
    end
    idx > len
end

function test_right_to_left(number::UInt, primes::Set{UInt})
    number_str = string(number)
    idx = length(number_str) - 1
    while idx ≥ 1 && is_prime(parse(UInt, number_str[begin:idx]), primes)
        idx -= 1
    end
    idx < 1
end

function is_truncatable_prime(number::UInt, primes::Set{UInt})
    test_left_to_right(number, primes) && test_right_to_left(number, primes)
end

function get_truncatable_primes()
    primes = Set(read_numbers("primes_less_than_2000000.txt"))
    truncatable_primes = filter(n -> n > 7 && is_truncatable_prime(n, primes), primes)
    (sort ∘ collect)(truncatable_primes)
end

function main()
    truncatable_primes = get_truncatable_primes()
    println("Sum: $(sum(truncatable_primes))")
    write_numbers(truncatable_primes, "truncatable_primes.txt")
end

main()
