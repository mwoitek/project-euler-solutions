function read_numbers(file_name::String)
    numbers = open(file_name, "r") do file
        parse.(UInt, readlines(file))
    end
    numbers
end

function write_numbers(numbers::Vector{UInt}, file_name::String)
    open(file_name, "w") do file
        for number in numbers
            write(file, "$number\n")
        end
    end
end
