# Counting fractions in a range
# https://projecteuler.net/problem=73

const min_fraction = 1 // 3
const max_fraction = 1 // 2
const max_denominator = 12000

function reduced_proper_fractions(d::Integer)
    num_1 = oneunit(d)
    Channel() do channel
        foreach(
            n -> put!(channel, n // d),
            Iterators.filter(n -> gcd(n, d) == num_1, num_1:(d - num_1)),
        )
    end
end

function get_all_fractions(max_d::Integer)
    num_2 = convert(typeof(max_d), 2)
    itrs = Iterators.map(reduced_proper_fractions, num_2:max_d)
    Iterators.flatten(itrs)
end

function main()
    fractions = get_all_fractions(max_denominator)
    answer = count(f -> min_fraction < f < max_fraction, fractions)
    println("Answer: $(answer)")
end

main()
