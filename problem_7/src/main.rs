// 10001st prime
// https://projecteuler.net/problem=7

fn main() {
    let primes: Vec<u64> = create_list_of_primes(10001);
    println!("Answer: {}", primes.last().unwrap());
}

fn is_prime(num: u64) -> bool {
    if num <= 1 {
        return false;
    } else if num <= 3 {
        return true;
    }

    if num % 2 == 0 || num % 3 == 0 {
        return false;
    }

    let mut d: u64 = 5;
    while d * d <= num {
        if num % d == 0 || num % (d + 2) == 0 {
            return false;
        }
        d += 6;
    }

    true
}

fn create_list_of_primes(list_size: u16) -> Vec<u64> {
    let mut primes: Vec<u64> = vec![2];
    let mut count: u16 = 1;
    let mut num: u64 = 3;

    while count < list_size {
        if is_prime(num) {
            primes.push(num);
            count += 1;
        }
        num += 2;
    }

    primes
}
