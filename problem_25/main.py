# 1000-digit Fibonacci number
# https://projecteuler.net/problem=25

# python3 main.py 1000 5000

from collections.abc import Iterator
from functools import cache
from sys import argv


@cache
def fibonacci(n: int) -> int:
    if n < 2:
        return n
    return fibonacci(n - 2) + fibonacci(n - 1)


def fibonacci_sequence(max_n: int) -> Iterator[int]:
    return map(fibonacci, range(max_n + 1))


def compute_num_digits(number: int) -> int:
    return len(str(number))


def find_solution(num_digits: int, max_n: int) -> int:
    for idx, number in enumerate(fibonacci_sequence(max_n)):
        if compute_num_digits(number) == num_digits:
            return idx
    return -1


if __name__ == "__main__":
    num_digits = int(argv[1])
    max_n = int(argv[2])
    solution = find_solution(num_digits, max_n)
    if solution >= 0:
        print(f"Index: {solution}")
    else:
        print("Could not find solution!")
