# Integer right triangles
# https://projecteuler.net/problem=39

using DataStructures: OrderedDict
include("triple.jl")

function read_triples(file_name::String)
    triples = Triple[]
    lines = open(file_name, "r") do file
        readlines(file)
    end
    for line in lines
        numbers = parse.(Int, split(line, ","))
        triple = Triple(numbers[1], numbers[2], numbers[3])
        push!(triples, triple)
    end
    sort(triples)
end

function find_solutions(triples::Vector{Triple}, perimeter::Int)
    solutions = Triple[]
    for triple in triples
        scale::Int = 1
        p = compute_perimeter(triple, scale)
        while p < perimeter
            scale += 1
            p = compute_perimeter(triple, scale)
        end
        p == perimeter && push!(solutions, scale_triple(triple, scale))
    end
    solutions
end

function count_solutions(triples::Vector{Triple}, max_perimeter::Int)
    counts_dict = OrderedDict{Int,Int}()
    for perimeter::Int = 12:max_perimeter # 12 = perimeter of (3,4,5)
        counts_dict[perimeter] = length(find_solutions(triples, perimeter))
    end
    counts_dict
end

function write_output(counts_dict::OrderedDict{Int,Int}, file_name::String)
    open(file_name, "w") do file
        for (perimeter, count) in counts_dict
            write(file, "$(perimeter),$(count)\n")
        end
    end
end

function main()
    in_file = ARGS[1]
    triples = read_triples(in_file)
    max_perimeter::Int = 1000

    counts_dict = count_solutions(triples, max_perimeter)
    out_file = "counts_$(max_perimeter).txt"
    write_output(counts_dict, out_file)

    answer = reduce(
        (p1, p2) -> counts_dict[p1] ≥ counts_dict[p2] ? p1 : p2,
        keys(counts_dict)
    )
    println("Answer:")
    println("Perimeter: $(answer)")
    println("Number of solutions: $(counts_dict[answer])")
end

main()
