# Divisor Square Sum
# https://projecteuler.net/problem=211

# julia -t 4 main.jl 64000000
# 430.03s user 8.76s system 326% cpu 2:14.52 total

using Primes: factor
using Transducers

# https://mathworld.wolfram.com/DivisorFunction.html
function sigma_factor((p, m)::Pair{T,U}) where {T<:Integer,U<:Integer}
    α = convert(T, m)
    num = p^(T(2) * (α + one(T))) - one(T)
    den = p^2 - one(T)
    num ÷ den
end

function σ_2(n::T) where {T<:Integer}
    foldl(*, factor(n) |> pairs |> Map(sigma_factor); init = one(T))
end

function is_perfect_square(x::T) where {T<:Integer}
    x == isqrt(x)^2
end

function compute_sum(n_max::T) where {T<:Integer}
    foldxt(+, one(T):(n_max - one(T)) |> Filter(is_perfect_square ∘ σ_2); init = zero(T))
end

function main()
    n_max = parse(Int128, ARGS[1])
    s = compute_sum(n_max)
    println("Answer:")
    println("Sum = ", s)
end

main()
