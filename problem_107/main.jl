# Minimal network
# https://projecteuler.net/problem=107

# julia main.jl p107_network.txt
# 1.54s user 0.37s system 126% cpu 1.516 total

using DataStructures

function fill_matrix!(m::Matrix{Int}, file_name::String)
    for (i, line) in enumerate(eachline(file_name))
        fixed_line = replace(line, "-" => "0")
        m[i, begin:end] = parse.(Int, split(fixed_line, ","))
    end
    return
end

function read_adjacency_matrix(file_name::String)
    num_lines = countlines(file_name)
    m = Matrix{Int}(undef, num_lines, num_lines)
    fill_matrix!(m, file_name)
    m
end

compute_initial_total_weight(m::Matrix{Int}) = sum(m) ÷ 2

function get_sorted_edges(m::Matrix{Int})
    edges_dict = Dict{NTuple{2,Int},Int}()
    d = size(m, 1)
    for j in 1:d
        for i in 1:d
            if i < j && m[i, j] > 0
                edges_dict[(i, j)] = m[i, j]
            end
        end
    end
    edges = collect(NTuple{2,Int}, keys(edges_dict))
    OrderedDict(e => edges_dict[e] for e in sort(edges; by = k -> edges_dict[k]))
end

function kruskal(num_vertices::Int, edges_dict::OrderedDict{NTuple{2,Int},Int})
    min_span_tree = NTuple{2,Int}[]
    ds = DisjointSets{Int}(num_vertices)
    foreach(u -> push!(ds, u), 1:num_vertices)
    for (u, v) in keys(edges_dict)
        if !in_same_set(ds, u, v)
            push!(min_span_tree, (u, v))
            union!(ds, u, v)
        end
    end
    min_span_tree
end

compute_final_total_weight(
    min_span_tree::Vector{NTuple{2,Int}},
    edges_dict::OrderedDict{NTuple{2,Int},Int},
) = sum(e -> edges_dict[e], min_span_tree)

function main()
    file_name = ARGS[1]
    m = read_adjacency_matrix(file_name)
    total_weight_before = compute_initial_total_weight(m)
    num_vertices = size(m, 1)
    edges_dict = get_sorted_edges(m)
    min_span_tree = kruskal(num_vertices, edges_dict)
    total_weight_after = compute_final_total_weight(min_span_tree, edges_dict)
    answer = total_weight_before - total_weight_after
    println("Answer: ", answer)
end

main()
