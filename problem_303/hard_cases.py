from functools import partial
from functools import reduce
from itertools import count
from itertools import takewhile

from more_itertools import distinct_permutations
from more_itertools import iterate

N_MAX = 10000


def generate_all_nines(n_max: int) -> list[int]:
    all_nines = map(lambda d: 10**d - 1, count(1))
    return list(takewhile(lambda n: n <= n_max, all_nines))


def f_all_nines(n: int) -> int:
    num_digits = len(str(n))
    return int("1" * num_digits + "2" * (4 * num_digits))


def f_multiples_of_all_niner(n: int, n_max: int) -> dict[int, int]:
    fs = {n: f_all_nines(n)}

    def select_permutation(factor: int) -> int:
        permutations = map(lambda t: int("".join(t)), distinct_permutations(str(fs[n])))
        return next(filter(lambda p: p % (factor * n) == 0, permutations))

    for factor in takewhile(lambda f: f * n <= n_max, count(2)):
        try:
            fs[factor * n] = select_permutation(factor)
        except StopIteration:
            continue

    for k in list(fs.keys()):
        for m in takewhile(lambda m: m <= n_max, iterate(lambda x: 5 * x, 5 * k)):
            fs[m] = 10 * fs[m // 5]

    for k in list(fs.keys()):
        for m in takewhile(lambda m: m <= n_max, iterate(lambda x: 10 * x, 10 * k)):
            fs[m] = 10 * fs[m // 10]

    assert all(map(lambda t: t[1] % t[0] == 0, fs.items()))
    return {k: fs[k] for k in sorted(fs)}


def f_hard_cases(n_max: int) -> dict[int, int]:
    all_nines = generate_all_nines(n_max)
    dicts = map(partial(f_multiples_of_all_niner, n_max=n_max), all_nines)
    fs = reduce(lambda x, y: x | y, dicts, {})
    fs[9899] = 9899 * 1122559978
    fs[9989] = 9989 * 200441708
    return {k: fs[k] for k in sorted(fs)}


def read_simple_cases() -> dict[int, int]:
    simple_cases_dict = dict()
    with open("simple_cases.csv", "r") as file:
        for line in file:
            line_parts = map(int, line.split(","))
            k, v = next(line_parts), next(line_parts)
            simple_cases_dict[k] = v
    return simple_cases_dict


def get_results(n_max: int) -> dict[int, int]:
    fs_hard = f_hard_cases(n_max)
    fs_simple = read_simple_cases()
    keys_hard = set(fs_hard.keys())
    keys_simple = set(fs_simple.keys())
    common_keys = keys_hard.intersection(keys_simple)
    assert all(map(lambda k: fs_hard[k] == fs_simple[k], common_keys))
    results_dict = fs_hard | fs_simple
    return {k: results_dict[k] for k in sorted(results_dict)}


def write_results(n_max: int) -> None:
    fs = get_results(n_max)
    with open("solved.csv", "w") as file:
        for k, v in fs.items():
            file.write(f"{k},{v}\n")
    with open("unsolved.txt", "w") as file:
        for n in filter(lambda n: n not in fs, range(1, n_max + 1)):
            file.write(f"{n}\n")


if __name__ == "__main__":
    write_results(N_MAX)
