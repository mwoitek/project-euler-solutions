#!/bin/bash
# Multiples with small digits
# https://projecteuler.net/problem=303
n_max=10000
max_iter=40000000
julia simple_cases.jl "$n_max" "$max_iter"
python3 hard_cases.py
trash-put simple_cases.csv hard_ns.txt
unsolved="$(cat unsolved.txt)"
if [[ -n "$unsolved" ]]; then
  echo 'There are unsolved cases!'
  exit 1
fi
trash-put unsolved.txt
julia sum.jl
mv -f solved.csv fs.csv
