using IterTools: iterated

not_have_property(n::Integer) = any(Iterators.map(>(2), digits(n)))

function f(n::Integer, max_iter = 1000000)
    n_wide = widen(n)
    multiples = Iterators.take(iterated(x -> x + n_wide, n_wide), max_iter)
    least_multiple =
        Iterators.dropwhile(not_have_property, multiples) |>
        Base.Fix2(Iterators.take, 1) |>
        Base.Fix1(collect, typeof(n_wide))
    isempty(least_multiple) ? zero(n_wide) : first(least_multiple)
end
