include("f.jl")

function list_of_fs(n_max::UInt, max_iter = 1000000)
    fs = Vector{UInt128}(undef, n_max)
    foreach(n -> fs[n] = f(n, max_iter), oneunit(n_max):n_max)
    fs
end

function write_simple_cases(fs::Vector{UInt128})
    open("simple_cases.csv", "w") do file
        for (n, f_of_n) in enumerate(fs)
            f_of_n == zero(f_of_n) && continue
            println(file, n, ",", f_of_n)
        end
    end
end

function write_hard_ns(fs::Vector{UInt128})
    open("hard_ns.txt", "w") do file
        for (n, f_of_n) in enumerate(fs)
            f_of_n == zero(f_of_n) && println(file, n)
        end
    end
end

function main()
    n_max = parse(UInt, ARGS[1])
    max_iter = parse(Int, ARGS[2])
    fs = list_of_fs(n_max, max_iter)
    write_simple_cases(fs)
    write_hard_ns(fs)
end

main()
