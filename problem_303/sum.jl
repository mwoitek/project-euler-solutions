function read_fs()
    fs = Dict()
    line_parts = Vector{String}(undef, 2)
    for line in eachline("solved.csv")
        line_parts = split(line, ",")
        k = parse(UInt, line_parts[1])
        v = parse(UInt128, line_parts[2])
        fs[k] = v
    end
    fs
end

function compute_sum()
    fs = read_fs()
    ratios = Vector{UInt}(undef, length(fs))
    for (k, v) in pairs(fs)
        ratios[k] = UInt(v ÷ k)
    end
    sum(ratios)
end

function main()
    answer = compute_sum()
    println("Answer: ", answer)
end

main()
