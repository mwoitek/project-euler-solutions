# Numbers for which no three consecutive digits have a sum greater than a given value
# https://projecteuler.net/problem=164

# julia main.jl 20
# 593.54s user 21.57s system 99% cpu 10:16.08 total

using DataStructures: Stack
using IterTools: partition
using Memoize: @memoize

struct State
    num_digits::Int
    prev_prev::Char
    prev::Char
end

has_allowed_sum(tpl::NTuple{3,Int}) = sum(tpl) ≤ 9

has_property(tpl::Tuple{Vararg{Int}}) =
    all(Iterators.map(has_allowed_sum, partition(tpl, 3, 1)))

function digit_sequences(num_digits::Int)
    ds = [0:9 for _ in 1:num_digits]
    itr = Iterators.filter(has_property, Iterators.product(ds...))
    Iterators.map(join, itr)
end

function next_digit_dict()
    nxt = Dict{String,Vector{Char}}()
    for seq in digit_sequences(3)
        k = seq[1:2]
        if haskey(nxt, k)
            push!(nxt[k], seq[3])
        else
            nxt[k] = Char[seq[3]]
        end
    end
    nxt
end

function count_numbers(num_digits::Int)
    num_digits ≤ 8 && return count(s -> !startswith(s, '0'), digit_sequences(num_digits))
    nxt = next_digit_dict()

    @memoize function hlpr(last_digits::String)
        cnt = 0
        stck = Stack{State}()
        push!(stck, State(8, last_digits[1], last_digits[2]))
        while !isempty(stck)
            st = pop!(stck)
            if st.num_digits == num_digits
                cnt += 1
                continue
            end
            for curr in nxt[st.prev_prev * st.prev]
                push!(stck, State(st.num_digits + 1, st.prev, curr))
            end
        end
        cnt
    end

    seqs = Iterators.filter(s -> !startswith(s, '0'), digit_sequences(8))
    sum(s -> hlpr(s[(end - 1):end]), seqs)
end

function main()
    num_digits = parse(Int, ARGS[1])
    answer = count_numbers(num_digits)
    println("Answer: ", answer)
end

main()
