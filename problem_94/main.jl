# Almost equilateral triangles
# https://projecteuler.net/problem=94

# https://oeis.org/A102341

const c_max = 5 * 10^8 - 1

function pell_equation_solutions(ch::Channel)
    c, d = 7, 4
    while c ≤ c_max
        put!(ch, c)
        c, d = 2 * c + 3 * d, c + 2 * d
    end
end

function sum_perimeters()
    sum_ = 0
    for c in Channel(pell_equation_solutions)
        sum_ += mod(c + 2, 3) == 0 ? 2 * (c + 1) : 0
        sum_ += mod(c - 2, 3) == 0 ? 2 * (c - 1) : 0
    end
    sum_
end

function main()
    sum_ = sum_perimeters()
    println("Sum of perimeters: ", sum_)
end

main()
