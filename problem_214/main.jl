# Totient Chains
# https://projecteuler.net/problem=214

# 63.42s user 0.65s system 100% cpu 1:03.85 total

using IterTools: iterated
using Primes: totient

const chain_length = 25
const primes_file_name = "/home/woitek/repos/project_euler/primes/primes.txt"
const upper_limit = 40000000

function read_primes()
    primes_itr = Iterators.map(Base.Fix1(parse, Int), eachline(primes_file_name))
    collect(Int, Iterators.takewhile(<(upper_limit), primes_itr))
end

function totients_powers_of_primes!(totients::Vector{Int}, primes_arr::Vector{Int})
    for p in primes_arr
        powers = Iterators.takewhile(<(upper_limit), iterated(x -> x * p, p))
        foreach(pp -> totients[pp] = (p - 1) * (pp ÷ p), powers)
    end
end

function compute_totients(primes_arr::Vector{Int})
    totients = zeros(Int, last(primes_arr))
    totients_powers_of_primes!(totients, primes_arr)
    for (n, t) in enumerate(totients)
        t ≠ 0 && continue
        totients[n] = totient(n)
    end
    totients
end

function compute_chain_length(n::Int, totients::Vector{Int})
    chain = Iterators.takewhile(>(1), iterated(x -> totients[x], n))
    cnt = 1
    foreach(_ -> cnt += 1, chain)
    cnt
end

function main()
    primes_arr = read_primes()
    totients = compute_totients(primes_arr)
    chain_lengths = Iterators.map(Base.Fix2(compute_chain_length, totients), primes_arr)
    itr_1 = Iterators.zip(primes_arr, chain_lengths)
    itr_2 = Iterators.filter(isequal(chain_length) ∘ last, itr_1)
    answer = sum(first, itr_2)
    println("Answer: ", answer)
end

main()
