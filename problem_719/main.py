# Number Splitting
# https://projecteuler.net/problem=719

# python3 main.py 1000000000000
# 6383.28s user 4.27s system 99% cpu 1:46:43.28 total

from itertools import count
from itertools import takewhile
from operator import itemgetter
from sys import argv

from more_itertools import partitions


def squares_generator(upper_limit):
    squares = map(lambda x: (x, x**2), count(4))
    return takewhile(lambda t: t[1] <= upper_limit, squares)


def list_to_int(lst):
    return int("".join(lst))


def is_s_number(b, p):
    pts = partitions(str(p))
    _ = next(pts)
    return any(
        map(
            lambda pt: sum(map(list_to_int, pt)) == b,
            pts,
        )
    )


def compute_sum(upper_limit):
    squares = squares_generator(upper_limit)
    s_numbers = filter(lambda t: is_s_number(*t), squares)
    return sum(map(itemgetter(1), s_numbers))


if __name__ == "__main__":
    upper_limit = int(argv[1])
    answer = compute_sum(upper_limit)
    print(f"Answer: {answer}")
