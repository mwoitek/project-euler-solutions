# Counting fractions
# https://projecteuler.net/problem=72

# julia main.jl list_prime_factors_2_1000000.txt 1000000

using DataStructures

function create_prime_factors_dict(file_name::String)
    prime_factors_dict = OrderedDict{Int,Vector{Int}}()
    lines = open(file_name, "r") do file
        readlines(file)
    end
    for line in lines
        line_parts = split(line, ":")
        number = parse(Int, line_parts[1])
        prime_factors = parse.(Int, split(line_parts[2], ","))
        prime_factors_dict[number] = prime_factors
    end
    prime_factors_dict
end

# https://en.wikipedia.org//wiki/Euler's_totient_function#Euler's_product_formula
function compute_totient_function(
    number::Int,
    prime_factors_dict::OrderedDict{Int,Vector{Int}},
)
    product =
        prod(map(p -> one(Rational{Int}) - one(Int) // p, prime_factors_dict[number]))
    numerator(number * product)
end

function get_list_of_totient_values(prime_factors_dict::OrderedDict{Int,Vector{Int}})
    phi = n::Int -> compute_totient_function(n, prime_factors_dict)
    map(phi, (collect ∘ keys)(prime_factors_dict))
end

function main()
    file_name = ARGS[1]
    n = parse(Int, ARGS[2])
    prime_factors_dict = create_prime_factors_dict(file_name)
    totient_values = get_list_of_totient_values(prime_factors_dict)
    num_fractions = sum(totient_values[1:(n - 1)])
    println("Number of fractions = $(num_fractions)")
end

main()
