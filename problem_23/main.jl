# Non-abundant sums
# https://projecteuler.net/problem=23

function read_numbers()
    numbers = open("abundant_numbers.txt", "r") do f
        parse.(Int, readlines(f))
    end
    numbers
end

function compute_abundant_sums(abundant_numbers::Vector{Int}, upper_limit::Int)
    sums = Set{Int}()
    for num_1 in abundant_numbers
        for num_2 in abundant_numbers
            s = num_1 + num_2
            s ≤ upper_limit && push!(sums, s)
        end
    end
    sums
end

function get_non_abundant_sums(abundant_sums::Set{Int}, upper_limit::Int)
    [num for num = 1:upper_limit if !(num ∈ abundant_sums)]
end

function main()
    abundant_numbers = read_numbers()
    upper_limit = parse(Int, ARGS[1])
    abundant_sums = compute_abundant_sums(abundant_numbers, upper_limit)
    non_abundant_sums = get_non_abundant_sums(abundant_sums, upper_limit)
    println("Sum: $(sum(non_abundant_sums))")
end

main()
