function is_abundant(num::Int)
    num < 2 && return false
    sum_div = 1
    d = 2
    while d^2 <= num
        if num % d == 0
            q = div(num, d)
            sum_div += d ≠ q ? d + q : d
        end
        d += 1
    end
    sum_div > num
end

function create_list(lower_limit::Int, upper_limit::Int)
    [num for num = lower_limit:upper_limit if is_abundant(num)]
end

function write_output(abundant_numbers::Vector{Int})
    open("abundant_numbers.txt", "w") do f
        for abundant_number in abundant_numbers
            write(f, "$abundant_number\n")
        end
    end
end

lower_limit = parse(Int, ARGS[1])
upper_limit = parse(Int, ARGS[2])
abundant_numbers = create_list(lower_limit, upper_limit)
write_output(abundant_numbers)
