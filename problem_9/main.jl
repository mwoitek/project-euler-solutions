# Special Pythagorean triplet
# https://projecteuler.net/problem=9

include("triple.jl")

function read_triples(file_name::String)
    triples = Triple[]
    lines = open(file_name, "r") do file
        readlines(file)
    end
    for line in lines
        numbers = parse.(Int, split(line, ","))
        triple = Triple(numbers[1], numbers[2], numbers[3])
        push!(triples, triple)
    end
    sort(triples)
end

function find_solutions(triples::Vector{Triple}, perimeter::Int)
    solutions = Triple[]
    for triple in triples
        scale::Int = 1
        p = compute_perimeter(triple, scale)
        while p < perimeter
            scale += 1
            p = compute_perimeter(triple, scale)
        end
        p == perimeter && push!(solutions, scale_triple(triple, scale))
    end
    solutions
end

function main()
    file_name = ARGS[1]
    triples = read_triples(file_name)
    perimeter::Int = 1000

    solutions = find_solutions(triples, perimeter)
    solution = solutions[1]
    println("Solution: $(solution)")

    product = prod([solution.a, solution.b, solution.c])
    println("Product: $(product)")
end

main()
