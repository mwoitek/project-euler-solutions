struct Triple
    a::Int
    b::Int
    c::Int
end

function Base.isless(x::Triple, y::Triple)
    x.a < y.a && x.b < y.b && x.c < y.c
end

function Base.show(io::IO, t::Triple)
    print(io, "$(t.a),$(t.b),$(t.c)")
end

function compute_perimeter(t::Triple, scale::Int=1)
    sum(scale * [t.a, t.b, t.c])
end

function scale_triple(t::Triple, scale::Int=1)
    Triple(scale * t.a, scale * t.b, scale * t.c)
end
