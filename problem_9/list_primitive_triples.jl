const A = [
    1 -2 2
    2 -1 2
    2 -2 3
]
const B = [
    1 2 2
    2 1 2
    2 2 3
]
const C = [
    -1 2 2
    -2 1 2
    -2 2 3
]

function generate_tree(height::Int)
    tree = Vector{Int}[[3; 4; 5]]
    for i = 1:height
        last_idx = length(tree)
        for j = 3^(i-1):-1:1
            triple = tree[last_idx-j+1]
            push!(tree, A * triple)
            push!(tree, B * triple)
            push!(tree, C * triple)
        end
    end
    tree
end

function write_output(tree::Vector{Vector{Int}})
    file_name = "primitive_triples_$(length(tree)).txt"
    open(file_name, "w") do file
        for triple in tree
            write(file, "$(triple[1]),$(triple[2]),$(triple[3])\n")
        end
    end
end

function main()
    height = parse(Int, ARGS[1])
    tree = generate_tree(height)
    write_output(tree)
end

main()
