# Sub-string divisibility
# https://projecteuler.net/problem=43

import utils

DIVISORS = [2, 3, 5, 7, 11, 13, 17]


def has_property(number: int) -> bool:
    number_str = str(number)
    for i in range(1, 8):
        number_slice = number_str[i : i + 3]
        if int(number_slice) % DIVISORS[i - 1] != 0:
            return False
    return True


def test_all_numbers(pandigital_numbers: list[int]) -> list[bool]:
    return list(map(has_property, pandigital_numbers))


def get_solutions() -> list[int]:
    pandigital_numbers = utils.read_numbers("pandigital_numbers.txt")
    test_results = test_all_numbers(pandigital_numbers)
    return [
        pandigital_numbers[i] for i in range(len(pandigital_numbers)) if test_results[i]
    ]


def main():
    solutions = get_solutions()
    print(f"Sum: {sum(solutions)}")
    utils.write_numbers(solutions, "solutions.txt")


if __name__ == "__main__":
    main()
