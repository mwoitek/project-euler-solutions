from itertools import permutations

from utils import write_numbers


def generate_pandigital_numbers() -> list[int]:
    pandigital_numbers = []
    digits = [str(d) for d in range(10)]
    for permutation in permutations(digits):
        if permutation[0] != "0":
            pandigital_number = int("".join(permutation))
            pandigital_numbers.append(pandigital_number)
    return pandigital_numbers


def main():
    pandigital_numbers = generate_pandigital_numbers()
    write_numbers(pandigital_numbers, "pandigital_numbers.txt")


if __name__ == "__main__":
    main()
