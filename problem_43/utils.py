def read_numbers(file_name: str) -> list[int]:
    with open(file_name, mode="r") as file:
        return [int(line.strip()) for line in file]


def write_numbers(numbers: list[int], file_name: str) -> None:
    with open(file_name, mode="w") as file:
        for number in numbers:
            file.write(f"{number}\n")
