// Lattice paths
// https://projecteuler.net/problem=15

#include <iostream>

using std::size_t;

template <typename T>
T** createMatrix(const size_t matrixDim);
template <typename T>
void initializeNumPathsMatrix(T** numPathsMatrix, const size_t matrixDim);
template <typename T>
void fillNumPathsMatrix(T** numPathsMatrix, const size_t matrixDim);
template <typename T>
void destroyMatrix(T** matrix, const size_t matrixDim);
template <typename T>
T computeNumPaths(const size_t latticeDim);

int main() {
  const size_t latticeDim = 20;

  const auto numPaths = computeNumPaths<unsigned long long>(latticeDim);
  std::cout << "Number of paths: " << numPaths << '\n';

  return 0;
}

template <typename T>
T** createMatrix(const size_t matrixDim) {
  T** matrix = new T*[matrixDim];

  for (size_t i = 0; i < matrixDim; i++) {
    matrix[i] = new T[matrixDim];
  }

  return matrix;
}

template <typename T>
void initializeNumPathsMatrix(T** numPathsMatrix, const size_t matrixDim) {
  for (size_t i = 0; i < matrixDim; i++) {
    numPathsMatrix[0][i] = T(1);
    numPathsMatrix[i][0] = T(1);
  }
}

template <typename T>
void fillNumPathsMatrix(T** numPathsMatrix, const size_t matrixDim) {
  initializeNumPathsMatrix(numPathsMatrix, matrixDim);

  for (size_t i = 1; i < matrixDim; i++) {
    for (size_t j = 1; j < matrixDim; j++) {
      numPathsMatrix[i][j] = numPathsMatrix[i - 1][j] + numPathsMatrix[i][j - 1];
    }
  }
}

template <typename T>
void destroyMatrix(T** matrix, const size_t matrixDim) {
  for (size_t i = 0; i < matrixDim; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
}

template <typename T>
T computeNumPaths(const size_t latticeDim) {
  const size_t matrixDim = latticeDim + 1;
  T** numPathsMatrix = createMatrix<T>(matrixDim);
  fillNumPathsMatrix(numPathsMatrix, matrixDim);
  const T numPaths = numPathsMatrix[latticeDim][latticeDim];
  destroyMatrix(numPathsMatrix, matrixDim);
  return numPaths;
}
