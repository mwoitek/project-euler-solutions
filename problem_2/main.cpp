// Even Fibonacci numbers
// https://projecteuler.net/problem=2

#include <array>
#include <forward_list>
#include <iostream>
#include <numeric>

void computeLastTerm(std::array<unsigned int, 3>& fibTriple);
void shiftTriple(std::array<unsigned int, 3>& fibTriple);
unsigned int popFirstTerm(std::array<unsigned int, 3>& fibTriple);
unsigned int computeLastPopFirst(std::array<unsigned int, 3>& fibTriple);
unsigned int computeNextEvenTerm(std::array<unsigned int, 3>& fibTriple);

std::forward_list<unsigned int> computeEvenTerms(const unsigned int upperLimit);
std::uint64_t sumEvenTerms(const std::forward_list<unsigned int>& evenTerms);

int main() {
  const unsigned int UPPER_LIMIT = 4000000;

  const std::forward_list<unsigned int> evenTerms = computeEvenTerms(UPPER_LIMIT);
  const std::uint64_t sum = sumEvenTerms(evenTerms);
  std::cout << "Sum: " << sum << '\n';

  return 0;
}

void computeLastTerm(std::array<unsigned int, 3>& fibTriple) {
  fibTriple[2] = fibTriple[0] + fibTriple[1];
}

void shiftTriple(std::array<unsigned int, 3>& fibTriple) {
  fibTriple[0] = fibTriple[1];
  fibTriple[1] = fibTriple[2];
}

unsigned int popFirstTerm(std::array<unsigned int, 3>& fibTriple) {
  const unsigned int firstTerm = fibTriple[0];
  shiftTriple(fibTriple);
  return firstTerm;
}

unsigned int computeLastPopFirst(std::array<unsigned int, 3>& fibTriple) {
  computeLastTerm(fibTriple);
  return popFirstTerm(fibTriple);
}

unsigned int computeNextEvenTerm(std::array<unsigned int, 3>& fibTriple) {
  unsigned int term;
  for (unsigned int i = 0; i < 3; i++) {
    term = computeLastPopFirst(fibTriple);
  }
  return term;
}

std::forward_list<unsigned int> computeEvenTerms(const unsigned int upperLimit) {
  std::forward_list<unsigned int> evenTerms;
  std::array<unsigned int, 3> fibTriple = {1, 1};
  unsigned int evenTerm = computeNextEvenTerm(fibTriple);

  while (evenTerm <= upperLimit) {
    evenTerms.push_front(evenTerm);
    evenTerm = computeNextEvenTerm(fibTriple);
  }

  return evenTerms;
}

std::uint64_t sumEvenTerms(const std::forward_list<unsigned int>& evenTerms) {
  return std::accumulate(evenTerms.begin(), evenTerms.end(), (std::uint64_t)0);
}
