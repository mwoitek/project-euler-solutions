# Square remainders
# https://projecteuler.net/problem=120

function r_closure(a)
    b1, b2 = a - 1, a + 1
    a_squared = a^2
    r = let b1 = b1
        b2 = b2
        a_squared = a_squared
        n -> mod(powermod(b1, n, a_squared) + powermod(b2, n, a_squared), a_squared)
    end
    r
end

function compute_r_max(a)
    r = r_closure(a)
    i = 0
    j = 1
    tortoise = r(i)
    hare = r(j)
    rs = Set([tortoise])
    while tortoise ≠ hare
        i += 1
        j += 2
        tortoise = r(i)
        hare = r(j)
        push!(rs, tortoise)
    end
    maximum(rs)
end

compute_sum() = sum(Iterators.map(compute_r_max, 3:1000))

function main()
    sum_ = compute_sum()
    println("Sum: ", sum_)
end

main()
