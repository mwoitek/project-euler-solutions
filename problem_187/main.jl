# Semiprimes
# https://projecteuler.net/problem=187

# julia --threads 3 main.jl 99999999

using Primes

function read_primes()
    primes = open("primes_less_than_100000000.txt", "r") do file
        parse.(Int, readlines(file))
    end
    Set(primes)
end

function composite_number_generator(upper_limit::Int)
    primes = read_primes()
    Channel() do ch
        for number in 4:upper_limit
            !(number ∈ primes) && put!(ch, number)
        end
    end
end

function is_semiprime(number::Int)
    factors_dict = Primes.factor(number)
    num_prime_factors = length(keys(factors_dict))
    num_prime_factors == 1 && return all(Iterators.map(isequal(2), values(factors_dict)))
    num_prime_factors == 2 && return all(Iterators.map(isequal(1), values(factors_dict)))
    false
end

function count_semiprimes(upper_limit::Int)
    cnt = Threads.Atomic{Int}(0)
    composite_numbers = composite_number_generator(upper_limit)
    Threads.foreach(composite_numbers) do composite_number
        is_semiprime(composite_number) && Threads.atomic_add!(cnt, 1)
    end
    cnt[]
end

function main()
    upper_limit = parse(Int, ARGS[1])
    cnt = count_semiprimes(upper_limit)
    println("Number of semiprimes: ", cnt)
end

main()
