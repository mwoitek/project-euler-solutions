# Largest exponential
# https://projecteuler.net/problem=99

struct Power
    base::Int
    exp::Int
end

function Base.isless(x::Power, y::Power)
    log_base_x = log(convert(Float64, x.base))
    log_base_y = log(convert(Float64, y.base))
    convert(Float64, x.exp) * log_base_x < convert(Float64, y.exp) * log_base_y
end

function read_powers()
    powers = Power[]
    lines = open("p099_base_exp.txt", "r") do file
        readlines(file)
    end
    for line in lines
        line_parts = split(line, ",")
        base = parse(Int, line_parts[1])
        exp = parse(Int, line_parts[2])
        push!(powers, Power(base, exp))
    end
    powers
end

function main()
    powers = read_powers()
    power, line_num = findmax(powers)
    println("Answer:")
    println("Power: $(power.base)^$(power.exp)")
    println("Line number: $(line_num)")
end

main()
