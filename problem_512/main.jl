# Sums of totients of powers
# https://projecteuler.net/problem=512

# julia -t4 main.jl 500000000
# 3422.94s user 3.65s system 321% cpu 17:46.25 total

using Primes: totient
using Transducers: Map, foldxt

# https://stackoverflow.com/questions/1522825/calculating-sum-of-geometric-series-mod-m
function geometric_series(r, n, m)
    s = 0
    r = mod(r, m)
    t = 1
    while n > 0
        if isodd(n)
            s = mod(muladd(r, s, t), m)
        end
        t = mod((1 + r) * t, m)
        r = mod(r * r, m)
        n = fld(n, 2)
    end
    s
end

# I'm using the fact that, for a given n, all the totients under
# the summation sign have a common factor totient(n)/n.
f(n) = mod(totient(n) * geometric_series(n, n, n + 1), n + 1)

g(n) = foldxt(+, Map(f), 1:n; init = 0)

function main()
    n = parse(Int, ARGS[1])
    answer = g(n)
    println("Answer: ", answer)
end

main()
