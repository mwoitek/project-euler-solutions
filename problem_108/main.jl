# Diophantine reciprocals I
# https://projecteuler.net/problem=108

# julia main.jl 1000

function count_solutions(n::Int)
    n_squared = n * n
    iseven(n) && return count(d -> mod(n_squared, d) == 0, 3:(n - 1)) + 3
    count(d -> mod(n_squared, d) == 0, 3:2:(n - 2)) + 2
end

function counts_generator(first_n::Int = 1)
    Channel() do channel
        foreach(
            n -> put!(channel, (n, count_solutions(n))),
            Iterators.countfrom(first_n, 1),
        )
    end
end

function main()
    lower_limit = parse(Int, ARGS[1])
    itr = Iterators.dropwhile(p -> p[2] ≤ lower_limit, counts_generator())
    n, num_solutions = (first ∘ collect)(Iterators.take(itr, 1))
    println("Answer:")
    println("n = $(n)")
    println("Number of solutions = $(num_solutions)")
end

main()
