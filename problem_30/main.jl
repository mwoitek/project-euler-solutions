# Digit fifth powers
# https://projecteuler.net/problem=30

# julia main.jl 5 500000

include("utils.jl")

function create_powers_dict(exponent::UInt8)
    z = Int('0')
    digits = [Char(i + z) for i = 0:9]
    powers = convert.(UInt, [i^exponent for i = 0:9])
    Dict(digits[i] => powers[i] for i = 1:10)
end

function sum_powers_of_digits(number::UInt, powers_dict::Dict{Char,UInt})
    sum([powers_dict[d] for d in string(number)])
end

function has_property(number::UInt, powers_dict::Dict{Char,UInt})
    number == sum_powers_of_digits(number, powers_dict)
end

function find_solutions(exponent::UInt8, upper_limit::UInt)
    solutions = UInt[]
    powers_dict = create_powers_dict(exponent)
    number = UInt(10)
    while number <= upper_limit
        has_property(number, powers_dict) && push!(solutions, number)
        number += one(UInt)
    end
    solutions
end

function main()
    exponent = parse(UInt8, ARGS[1])
    upper_limit = parse(UInt, ARGS[2])
    solutions = find_solutions(exponent, upper_limit)
    if !isempty(solutions)
        println("Sum: $(sum(solutions))")
        write_numbers(solutions, "solutions_exponent_$(exponent).txt")
    else
        println("No solution was found!")
    end
end

main()
