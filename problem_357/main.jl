# Prime generating integers
# https://projecteuler.net/problem=357

# julia -t 2 main.jl 100000000
# 29.23s user 0.41s system 192% cpu 15.414 total

using Primes: isprime

function test_number(n)
    divisors = Iterators.filter(d -> mod(n, d) == 0, 1:isqrt(n))
    sums = Iterators.map(d -> d + n ÷ d, divisors)
    all(Iterators.map(isprime, sums))
end

function compute_sum(n_max)
    sum_ = Threads.Atomic{Int}(0)
    Threads.@threads for n in 1:n_max
        test_number(n) && Threads.atomic_add!(sum_, n)
    end
    sum_[]
end

function main()
    n_max = parse(Int, ARGS[1])
    sum_ = compute_sum(n_max)
    println("Sum: ", sum_)
end

main()
