#include "NumericalSpiral.h"

#include <cmath>
#include <fstream>
#include <iomanip>

NumericalSpiral::NumericalSpiral(const int dim) {
  if (dim < 3) {
    throw std::invalid_argument("dim must satisfy dim >= 3");
  } else if (dim % 2 == 0) {
    throw std::invalid_argument("dim must be odd");
  }

  this->dim = dim;
  createMatrix();
}

NumericalSpiral::~NumericalSpiral() { destroyMatrix(); }

void NumericalSpiral::createMatrix() {
  const size_t matrixDim = static_cast<size_t>(dim);

  matrix = new int*[matrixDim];
  for (size_t i = 0; i < matrixDim; i++) {
    matrix[i] = new int[matrixDim];
  }
}

void NumericalSpiral::destroyMatrix() {
  for (int i = 0; i < dim; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
}

std::array<int, 4> NumericalSpiral::computeLimits(const int filledBorders) {
  std::array<int, 4> limits;

  limits[0] = filledBorders;
  limits[1] = filledBorders + 1;
  limits[2] = dim - limits[1];
  limits[3] = limits[2] - 1;

  return limits;
}

void NumericalSpiral::fillTop(const std::array<int, 4>& limits) {
  const int row = limits[0];

  for (int col = limits[3]; col >= row; col--) {
    matrix[row][col] = matrix[row][col + 1] - 1;
  }
}

void NumericalSpiral::fillLeft(const std::array<int, 4>& limits) {
  const int col = limits[0];

  for (int row = limits[1]; row <= limits[2]; row++) {
    matrix[row][col] = matrix[row - 1][col] - 1;
  }
}

void NumericalSpiral::fillBottom(const std::array<int, 4>& limits) {
  const int row = limits[2];

  for (int col = limits[1]; col <= row; col++) {
    matrix[row][col] = matrix[row][col - 1] - 1;
  }
}

void NumericalSpiral::fillRight(const std::array<int, 4>& limits) {
  const int col = limits[2];

  for (int row = limits[3]; row >= limits[1]; row--) {
    matrix[row][col] = matrix[row + 1][col] - 1;
  }
}

void NumericalSpiral::fillBorder(const int filledBorders) {
  const std::array<int, 4> limits = computeLimits(filledBorders);

  const int tmp = dim - 2 * filledBorders;
  matrix[limits[0]][limits[2]] = tmp * tmp;

  fillTop(limits);
  fillLeft(limits);
  fillBottom(limits);
  fillRight(limits);
}

void NumericalSpiral::fillMatrix() {
  const int bordersToFill = (dim - 1) / 2;

  for (int filledBorders = 0; filledBorders < bordersToFill; filledBorders++) {
    fillBorder(filledBorders);
  }

  matrix[bordersToFill][bordersToFill] = 1;
}

void NumericalSpiral::printMatrixToFile() {
  std::stringstream fileName;
  fileName << "numerical_spiral_" << dim << ".txt";

  std::ofstream outFile;
  outFile.open(fileName.str());

  const int width = static_cast<int>(std::ceil(std::log10(matrix[0][dim - 1] + 1)));

  for (int row = 0; row < dim; row++) {
    for (int col = 0; col < dim - 1; col++) {
      outFile << std::setw(width) << matrix[row][col] << ' ';
    }
    outFile << std::setw(width) << matrix[row][dim - 1] << '\n';
  }

  outFile.close();
}

int NumericalSpiral::sumMainDiagonal() {
  int sum = 0;

  for (int i = 0; i < dim; i++) {
    sum += matrix[i][i];
  }

  return sum;
}

int NumericalSpiral::sumSecondaryDiagonal() {
  int sum = 0;
  int row = 0;
  int col = dim - 1;

  while (row < dim) {
    sum += matrix[row][col];
    row++;
    col--;
  }

  return sum;
}

int NumericalSpiral::sumDiagonals() { return sumMainDiagonal() + sumSecondaryDiagonal() - 1; }
