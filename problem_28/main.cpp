// Number spiral diagonals
// https://projecteuler.net/problem=28

#include <iostream>

#include "NumericalSpiral.h"

int main(int argc, char** argv) {
  if (argc == 1) {
    std::cout << "dimension of the numerical spiral must be passed as an argument\n";
    return 1;
  }
  const std::string arg1(argv[1]);

  int dim;
  try {
    dim = std::stoi(arg1);
  } catch (const std::invalid_argument&) {
    std::cout << "could not convert argument to an integer\n";
    return 1;
  } catch (const std::out_of_range&) {
    std::cout << "argument value cannot be stored in an int variable\n";
    return 1;
  }

  try {
    NumericalSpiral ns(dim);
    ns.fillMatrix();

    const int sum = ns.sumDiagonals();
    std::cout << "Sum: " << sum << '\n';

    /* ns.printMatrixToFile(); */
  } catch (const std::exception& e) {
    std::cout << e.what() << '\n';
    return 1;
  }

  return 0;
}
