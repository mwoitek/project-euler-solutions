#ifndef NUMERICALSPIRAL_H
#define NUMERICALSPIRAL_H

#include <array>

class NumericalSpiral {
 private:
  int dim;
  int** matrix;
  void createMatrix();
  void destroyMatrix();
  std::array<int, 4> computeLimits(const int filledBorders);
  void fillTop(const std::array<int, 4>& limits);
  void fillLeft(const std::array<int, 4>& limits);
  void fillBottom(const std::array<int, 4>& limits);
  void fillRight(const std::array<int, 4>& limits);
  void fillBorder(const int filledBorders);
  int sumMainDiagonal();
  int sumSecondaryDiagonal();

 public:
  explicit NumericalSpiral(const int dim);
  ~NumericalSpiral();
  void fillMatrix();
  void printMatrixToFile();
  int sumDiagonals();
};

#endif  // PROBLEM_28_NUMERICALSPIRAL_H_
