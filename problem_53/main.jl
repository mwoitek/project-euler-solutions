# Combinatoric selections
# https://projecteuler.net/problem=53

# julia main.jl 100 1000000

function binomial_coefficients_for_n(n::BigInt)
    Iterators.map(k -> binomial(n, k), zero(BigInt):n)
end

function all_binomial_coefficients(max_n::BigInt)
    itrs = Iterators.map(binomial_coefficients_for_n, one(BigInt):max_n)
    Iterators.flatten(itrs)
end

function main()
    max_n = parse(BigInt, ARGS[1])
    lower_limit = parse(BigInt, ARGS[2])
    coefficients = all_binomial_coefficients(max_n)
    answer = count(c -> c > lower_limit, coefficients)
    println("Answer: $(answer)")
end

main()
