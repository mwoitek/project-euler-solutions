# Counting Sundays
# https://projecteuler.net/problem=19

from datetime import date
from datetime import timedelta


def get_first_sunday(first_date: date) -> date:
    date_ = first_date
    one_day = timedelta(days=1)
    while date_.weekday() != 6:  # Sunday is 6
        date_ += one_day
    return date_


def get_all_sundays(first_date: date, last_date: date) -> list[date]:
    sundays = []
    date_ = get_first_sunday(first_date)
    one_week = timedelta(weeks=1)
    while date_ <= last_date:
        sundays.append(date_)
        date_ += one_week
    return sundays


def is_first_of_the_month(date_: date) -> bool:
    return date_.day == 1


def count_sundays(first_date: date, last_date: date) -> int:
    count = 0
    sundays = get_all_sundays(first_date, last_date)
    for _ in filter(is_first_of_the_month, sundays):
        count += 1
    return count


def main():
    first_date = date.fromisoformat("1901-01-01")
    last_date = date.fromisoformat("2000-12-31")
    print(f"Count: {count_sundays(first_date, last_date)}")


if __name__ == "__main__":
    main()
