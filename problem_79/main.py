# Passcode derivation
# https://projecteuler.net/problem=79

from collections import defaultdict


def read_numbers() -> list[str]:
    with open("p079_keylog.txt", "r") as f:
        return [n.strip() for n in f.readlines()]


def compute_digit_counts(numbers: list[str], column: int) -> dict[str, int]:
    counts = defaultdict(lambda: 0)
    for digit in map(lambda n: n[column], numbers):
        counts[digit] += 1
    fltr = filter(lambda d: counts[d] > 0, map(str, range(10)))
    return {d: counts[d] for d in fltr}


def compute_digit_pair_counts(numbers: list[str]) -> defaultdict[str, int]:
    counts = defaultdict(lambda: 0)
    for number in numbers:
        counts[number[:2]] += 1
        counts[number[1:]] += 1
    return counts


def compute_counts_next_digit(
    digit: str, digit_pair_counts: defaultdict[str, int]
) -> dict[str, int]:
    ds = list(map(str, range(10)))
    pairs = map(lambda d: digit + d, ds)
    counts = map(lambda p: digit_pair_counts[p], pairs)
    return {d: c for d, c in zip(ds, counts) if c > 0}


def most_likely_digits_after(
    digit: str, digit_pair_counts: defaultdict[str, int]
) -> list[str]:
    counts = compute_counts_next_digit(digit, digit_pair_counts)
    max_count = max(counts.values(), default=0)
    if max_count == 0:
        return []
    return list(filter(lambda d: counts[d] == max_count, counts.keys()))


def test_passcode(passcode: str, numbers: list[str]) -> bool:
    for number in numbers:
        idx_1 = 0
        idx_2 = passcode.find(number[idx_1])
        while idx_2 != -1 and idx_1 < 2:
            idx_1 += 1
            idx_2 = passcode.find(number[idx_1], idx_2 + 1)
        if idx_1 < 2:
            return False
    return True


if __name__ == "__main__":
    numbers = read_numbers()

    digits = sorted(set("".join(numbers)))
    print(f"Digits that were used: {digits}")
    print(f"Number of digits that were used: {len(digits)}")
    print()

    for column in range(3):
        print(
            f"Digit counts for column {column + 1}: {compute_digit_counts(numbers, column)}"
        )
    print()

    digit_pair_counts = compute_digit_pair_counts(numbers)
    for d in digits:
        print(
            f"Counts for digits after {d}: {compute_counts_next_digit(d, digit_pair_counts)}"
        )
        print(
            f"Most likely digits after {d}: {most_likely_digits_after(d, digit_pair_counts)}"
        )
        print()

    guess = "73162890"
    if test_passcode(guess, numbers):
        print("Guess is correct!")
    else:
        print("Guess is wrong.")
