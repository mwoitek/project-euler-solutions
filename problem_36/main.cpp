// Double-base palindromes
// https://projecteuler.net/problem=36

#include <iostream>
#include <list>
#include <numeric>
#include <sstream>
#include <vector>

using std::list;
using std::string;
using std::vector;
using ullong = unsigned long long;

bool isPalindrome(const string& str);
template <typename UInt>
bool isPalindrome10(const UInt num);
template <typename UInt>
string convertToBinary(UInt num);
template <typename UInt>
bool isPalindrome2(const UInt num);
template <typename UInt>
bool isPalindromeBothBases(const UInt num);
template <typename UInt>
vector<UInt> findPalindromes(const UInt numMax);
template <typename UInt>
ullong sumPalindromes(const vector<UInt>& palindromes);

int main() {
  const unsigned NUM_MAX = 1000000;

  const auto palindromes = findPalindromes(NUM_MAX);
  const auto sum = sumPalindromes(palindromes);
  std::cout << "Sum of palindromes: " << sum << '\n';

  return 0;
}

bool isPalindrome(const string& str) {
  list<char> chars;
  std::copy(str.begin(), str.end(), std::back_inserter(chars));

  list<char> reverseChars;
  std::copy(str.begin(), str.end(), std::front_inserter(reverseChars));

  return chars == reverseChars;
}

template <typename UInt>
bool isPalindrome10(const UInt num) {
  const string strNum = std::to_string(num);
  return isPalindrome(strNum);
}

template <typename UInt>
string convertToBinary(UInt num) {
  if (num == 0) {
    return "0";
  }

  using digit_t = std::uint8_t;
  list<digit_t> digits;
  while (num > 0) {
    digits.push_front((digit_t)(num % 2));
    num /= 2;
  }

  std::stringstream strNum;
  for (const digit_t digit : digits) {
    strNum << (unsigned)digit;
  }
  return strNum.str();
}

template <typename UInt>
bool isPalindrome2(const UInt num) {
  const string strNum = convertToBinary(num);
  return isPalindrome(strNum);
}

template <typename UInt>
bool isPalindromeBothBases(const UInt num) {
  return isPalindrome10(num) && isPalindrome2(num);
}

template <typename UInt>
vector<UInt> findPalindromes(const UInt numMax) {
  vector<UInt> palindromes;

  for (UInt num = UInt(0); num <= numMax; num++) {
    if (isPalindromeBothBases(num)) {
      palindromes.push_back(num);
    }
  }

  return palindromes;
}

template <typename UInt>
ullong sumPalindromes(const vector<UInt>& palindromes) {
  return std::accumulate(palindromes.begin(), palindromes.end(), (ullong)0);
}
