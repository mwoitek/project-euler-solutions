include("utils.jl")

compute_pentagon_number(n::UInt) = div(n * (3 * n - 1), 2)

function main()
    list_size = parse(UInt, ARGS[1])
    pentagon_numbers = create_list(compute_pentagon_number, list_size)
    write_output(pentagon_numbers, "pentagon_numbers.txt")
end

main()
