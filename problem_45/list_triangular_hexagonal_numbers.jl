include("utils.jl")

# https://mathworld.wolfram.com/TriangularNumber.html
compute_triangular_number(n::UInt) = div(n * (n + 1), 2)
compute_hexagonal_number(n::UInt) = compute_triangular_number(2 * n - 1)

function main()
    list_size = parse(UInt, ARGS[1])
    triangular_hexagonal_numbers = create_list(compute_hexagonal_number, list_size)
    write_output(triangular_hexagonal_numbers, "triangular_hexagonal_numbers.txt")
end

main()
