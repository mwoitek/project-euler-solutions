# Triangular, pentagonal, and hexagonal
# https://projecteuler.net/problem=45

include("utils.jl")

function is_triangular_and_hexagonal(number::UInt, triangular_hexagonal_numbers::Set{UInt})
    number ∈ triangular_hexagonal_numbers
end

function get_solutions(triangular_hexagonal_numbers::Set{UInt}, pentagon_numbers::Vector{UInt})
    solutions = UInt[]
    for number in pentagon_numbers
        if is_triangular_and_hexagonal(number, triangular_hexagonal_numbers)
            push!(solutions, number)
        end
    end
    solutions
end

function main()
    triangular_hexagonal_numbers = Set(read_numbers("triangular_hexagonal_numbers.txt"))
    pentagon_numbers = read_numbers("pentagon_numbers.txt")
    solutions = get_solutions(triangular_hexagonal_numbers, pentagon_numbers)
    write_output(solutions, "solutions.txt")
end

main()
