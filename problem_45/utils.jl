function create_list(func::Function, list_size::UInt)
    numbers = UInt[]
    count = zero(UInt)
    n = one(UInt)
    while count < list_size
        push!(numbers, func(n))
        count += one(UInt)
        n += one(UInt)
    end
    numbers
end

function read_numbers(file_name::String)
    numbers = open(file_name, "r") do file
        parse.(UInt, readlines(file))
    end
    numbers
end

function write_output(numbers::Vector{UInt}, file_name::String)
    open(file_name, "w") do file
        for number in numbers
            write(file, "$number\n")
        end
    end
end
