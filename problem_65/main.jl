# Convergents of e
# https://projecteuler.net/problem=65

# julia main.jl 100

function get_coefficients(num_coefficients::UInt)
    coefficients = UInt[2, 1]
    k = one(UInt)
    while length(coefficients) < num_coefficients
        append!(coefficients, UInt[2 * k, 1, 1])
        k += one(UInt)
    end
    coefficients[1:num_coefficients]
end

# https://en.wikipedia.org/wiki/Continued_fraction#Infinite_continued_fractions_and_convergents
function compute_convergents(num_convergents::UInt)
    hs = BigInt[0, 1]
    ks = BigInt[1, 0]
    coefficients = get_coefficients(num_convergents)
    for coefficient in coefficients
        push!(hs, coefficient * hs[end] + hs[end - 1])
        push!(ks, coefficient * ks[end] + ks[end - 1])
    end
    map(p -> p[1] // p[2], zip(hs[3:end], ks[3:end]))
end

function main()
    num_convergents = parse(UInt, ARGS[1])
    convergents = compute_convergents(num_convergents)
    convergent = last(convergents)
    sum_digits = (sum ∘ digits ∘ numerator)(convergent)
    println("Sum of digits in the numerator: $(sum_digits)")
end

main()
