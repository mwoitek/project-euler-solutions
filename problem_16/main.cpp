// Power digit sum
// https://projecteuler.net/problem=16

#include <iostream>
#include <list>
#include <numeric>

std::list<std::uint8_t> multiplyByTwo(std::list<std::uint8_t>& num);
std::list<std::uint8_t> computePowerOfTwo(const unsigned int exponent);
std::uint64_t sumDigits(const std::list<std::uint8_t>& powerOfTwo);

int main() {
  const unsigned int EXPONENT = 1000;

  const std::list<std::uint8_t> powerOfTwo = computePowerOfTwo(EXPONENT);
  const std::uint64_t sum = sumDigits(powerOfTwo);
  std::cout << "Sum: " << sum << '\n';

  return 0;
}

std::list<std::uint8_t> multiplyByTwo(std::list<std::uint8_t>& num) {
  std::list<std::uint8_t> result;

  std::uint8_t tmp;
  std::uint8_t digit;
  std::uint8_t carry = 0;

  num.push_back(0);

  while (!num.empty()) {
    tmp = 2 * num.front() + carry;
    digit = tmp % 10;
    result.push_back(digit);
    carry = (tmp - digit) / 10;
    num.pop_front();
  }

  if (result.back() == 0) {
    result.pop_back();
  }

  return result;
}

std::list<std::uint8_t> computePowerOfTwo(const unsigned int exponent) {
  std::list<std::uint8_t> power = {1};

  for (unsigned int i = 0; i < exponent; i++) {
    power = multiplyByTwo(power);
  }

  return power;
}

std::uint64_t sumDigits(const std::list<std::uint8_t>& powerOfTwo) {
  return std::accumulate(powerOfTwo.begin(), powerOfTwo.end(), (std::uint64_t)0);
}
