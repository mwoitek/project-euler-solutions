// Amicable numbers
// https://projecteuler.net/problem=21

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

using std::pair;
using std::size_t;
using std::vector;

template <typename UInt>
UInt computeSumDivisors(const UInt num);
template <typename UInt>
vector<UInt> computeSumsDivisors(const UInt numMax);
template <typename UInt>
vector<pair<UInt, UInt>> findAmicablePairs(const vector<UInt>& sums, const UInt numMax);
template <typename UInt>
UInt computeSumAmicableNums(const vector<pair<UInt, UInt>>& pairs);
template <typename UInt>
void printPairsToFile(const vector<pair<UInt, UInt>>& pairs, const UInt numMax);

int main() {
  const unsigned NUM_MAX = 10000;

  const auto sums = computeSumsDivisors(NUM_MAX);
  const auto pairs = findAmicablePairs(sums, NUM_MAX);

  const auto sum = computeSumAmicableNums(pairs);
  std::cout << "Sum of amicable numbers: " << sum << '\n';

  printPairsToFile(pairs, NUM_MAX);

  return 0;
}

template <typename UInt>
UInt computeSumDivisors(const UInt num) {
  UInt sum = UInt(1);
  UInt d = UInt(2);

  while (d * d < num) {
    if (num % d == 0) {
      sum += d + num / d;
    }
    d++;
  }

  if (d * d == num) {
    sum += d;
  }

  return sum;
}

template <typename UInt>
vector<UInt> computeSumsDivisors(const UInt numMax) {
  vector<UInt> sums((size_t)numMax - 1);

  for (UInt num = UInt(2); num <= numMax; num++) {
    sums[(size_t)num - 2] = computeSumDivisors(num);
  }

  return sums;
}

template <typename UInt>
vector<pair<UInt, UInt>> findAmicablePairs(const vector<UInt>& sums, const UInt numMax) {
  vector<pair<UInt, UInt>> pairs;
  const UInt idxMax = numMax - 1;

  for (UInt i = UInt(0); i < idxMax; i++) {
    const UInt a = i + 2;
    const UInt b = sums[(size_t)i];

    if (a >= b) {
      continue;
    }

    const UInt j = b - 2;
    if ((j < idxMax) && (sums[(size_t)j] == a)) {
      pairs.push_back(std::make_pair(a, b));
    }
  }

  return pairs;
}

template <typename UInt>
UInt computeSumAmicableNums(const vector<pair<UInt, UInt>>& pairs) {
  const auto sumFunc = [](const UInt sum, const pair<UInt, UInt>& p) {
    return sum + p.first + p.second;
  };
  return std::accumulate(pairs.begin(), pairs.end(), UInt(0), sumFunc);
}

template <typename UInt>
void printPairsToFile(const vector<pair<UInt, UInt>>& pairs, const UInt numMax) {
  std::stringstream fileName;
  fileName << "amicable_numbers_" << numMax << ".txt";

  const int width = (int)std::ceil(std::log10((double)numMax + 1));

  std::ofstream outFile;
  outFile.open(fileName.str());

  for (const auto& p : pairs) {
    outFile << std::setw(width) << p.first << ' ';
    outFile << std::setw(width) << p.second << '\n';
  }

  outFile.close();
}
