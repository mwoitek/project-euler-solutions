# Powers of Two
# https://projecteuler.net/problem=686

# julia main.jl 123 678910
# 13.20s user 0.37s system 102% cpu 13.184 total

using IterTools: nth

function get_first_digits(expo::Int, num_digits::Int)
    f, _ = modf(expo * log10(2))
    trunc(Int, 10.0^(f + num_digits - 1))
end

function p(l::Int, n::Int)
    expos = Iterators.countfrom(0, 1)
    first_digits = Iterators.map(Base.Fix2(get_first_digits, ndigits(l)), expos)
    pairs = Iterators.filter(isequal(l) ∘ last, Iterators.zip(expos, first_digits))
    nth(Iterators.map(first, pairs), n)
end

function main()
    l = parse(Int, ARGS[1])
    n = parse(Int, ARGS[2])
    answer = p(l, n)
    println("Answer: ", answer)
end

main()
