# Su Doku
# https://projecteuler.net/problem=96

# 4150.60s user 1.31s system 99% cpu 1:09:20.00 total
# This code works, but it is slow. Find a better solution later.

from collections.abc import Iterable
from typing import Optional

import numpy as np
import numpy.typing as npt
from more_itertools import take

NUM_GRIDS = 50


def lines_to_arr(lines: Iterable[str]) -> npt.NDArray[np.int_]:
    arr = np.empty((9, 9), dtype=np.int_)
    for i, line in enumerate(lines):
        arr[i] = list(map(int, line))
    return arr


def read_grids() -> npt.NDArray[np.int_]:
    grids = np.empty((NUM_GRIDS, 9, 9), dtype=np.int_)
    with open("p096_sudoku.txt", "r") as file:
        lines = map(lambda l: l.strip(), file)
        important_lines = filter(lambda l: not l.startswith("Grid"), lines)
        for i in range(NUM_GRIDS):
            grids[i, :, :] = lines_to_arr(take(9, important_lines))
    return grids


def check_row_or_column(grid: npt.NDArray[np.int_], axis: int, idx: int) -> bool:
    slice = grid[idx, :] if axis == 0 else grid[:, idx]
    non_zero = slice[slice != 0]
    _, counts = np.unique(non_zero, return_counts=True)
    return np.all(counts == 1).astype(bool)


def check_all_rows_or_columns(grid: npt.NDArray[np.int_], axis: int) -> bool:
    return all(map(lambda idx: check_row_or_column(grid, axis, idx), range(9)))


def check_box(grid: npt.NDArray[np.int_], top_left: tuple[int, int]) -> bool:
    row, col = top_left
    box = grid[row : row + 3, col : col + 3]
    flat_box = box.flatten()
    non_zero = flat_box[flat_box != 0]
    _, counts = np.unique(non_zero, return_counts=True)
    return np.all(counts == 1).astype(bool)


def check_all_boxes(grid: npt.NDArray[np.int_]) -> bool:
    idxs = ((3 * i, 3 * j) for i in range(3) for j in range(3))
    return all(map(lambda idx: check_box(grid, idx), idxs))


def is_valid(grid: npt.NDArray[np.int_]) -> bool:
    return (
        check_all_rows_or_columns(grid, 0)
        and check_all_rows_or_columns(grid, 1)
        and check_all_boxes(grid)
    )


def find_empty(grid: npt.NDArray[np.int_]) -> Optional[tuple[int, int]]:
    idxs_vals = np.ndenumerate(grid)
    empty_pos = filter(lambda tpl: tpl[1] == 0, idxs_vals)
    try:
        idx, _ = next(empty_pos)
        return idx
    except StopIteration:
        return None


def change_and_test(grid: npt.NDArray[np.int_], idx: tuple[int, int], val: int) -> bool:
    grid[*idx] = val
    valid = is_valid(grid)
    if not valid:
        grid[*idx] = 0
    return valid


def find_solution(grid: npt.NDArray[np.int_]) -> bool:
    idx = find_empty(grid)
    if idx is None:
        return True
    for val in range(1, 10):
        valid = change_and_test(grid, idx, val)
        if not valid:
            continue
        if find_solution(grid):
            return True
        grid[*idx] = 0
    return False


def find_all_solutions(grids: npt.NDArray[np.int_]) -> None:
    for i in range(NUM_GRIDS):
        _ = find_solution(grids[i, :, :])
        print(f"Solved grid {i+1}!")


def compute_sum(grids: npt.NDArray[np.int_]) -> int:
    sum_ = 0
    for i in range(NUM_GRIDS):
        sum_ += int("".join(map(str, grids[i, 0, :3])))
    return sum_


if __name__ == "__main__":
    grids = read_grids()
    find_all_solutions(grids)
    answer = compute_sum(grids)
    print(f"Answer: {answer}")
