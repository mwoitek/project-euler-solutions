#!/bin/bash

for ((n = 1; n <= 9; n++)); do
  python3 list_pandigital_numbers.py "$n" || exit 1
done

out_file="pandigital_numbers.txt"
[[ -f "$out_file" ]] && trash-put "$out_file"

for ((n = 1; n <= 9; n++)); do
  file_name="pandigital_numbers_1_to_${n}.txt"
  cat "$file_name" >>"$out_file" && trash-put "$file_name"
done
