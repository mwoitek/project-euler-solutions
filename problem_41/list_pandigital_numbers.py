from itertools import permutations
from sys import argv


def get_pandigital_numbers(last_digit: int) -> list[int]:
    pandigital_numbers = []
    digits = "".join([str(d) for d in range(1, last_digit + 1)])
    for perm_tpl in permutations(digits):
        number = int("".join(perm_tpl))
        pandigital_numbers.append(number)
    return pandigital_numbers


def write_output(pandigital_numbers: list[int], file_name: str) -> None:
    with open(file_name, mode="w") as file:
        for number in pandigital_numbers:
            file.write(f"{number}\n")


if __name__ == "__main__":
    last_digit = int(argv[1])
    pandigital_numbers = get_pandigital_numbers(last_digit)
    file_name = f"pandigital_numbers_1_to_{last_digit}.txt"
    write_output(pandigital_numbers, file_name)
