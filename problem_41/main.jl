# Pandigital prime
# https://projecteuler.net/problem=41

using Primes

function read_numbers(file_name::String)
    numbers = open(file_name, "r") do file
        parse.(Int, readlines(file))
    end
    numbers
end

function get_pandigital_primes(pandigital_numbers::Vector{Int})
    filter(Primes.isprime, pandigital_numbers)
end

function write_output(pandigital_primes::Vector{Int}, file_name::String)
    open(file_name, "w") do file
        for prime in pandigital_primes
            write(file, "$(prime)\n")
        end
    end
end

function main()
    pandigital_numbers = read_numbers("pandigital_numbers.txt")
    pandigital_primes = get_pandigital_primes(pandigital_numbers)
    max_pandigital_prime = maximum(pandigital_primes)
    println("Largest pandigital prime: $(max_pandigital_prime)")
    write_output(pandigital_primes, "pandigital_primes.txt")
end

main()
