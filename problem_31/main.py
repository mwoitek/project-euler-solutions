# Coin sums
# https://projecteuler.net/problem=31

# python3 main.py 200

# https://en.wikipedia.org/wiki/Partition_(number_theory)#Restricted_part_size_or_number_of_parts
# https://math.stackexchange.com/questions/89240/prime-partition

from math import prod
from sys import argv

from sympy import Symbol
from sympy import expand
from sympy import series

COINS = [1, 2, 5, 10, 20, 50, 100, 200]

amount = int(argv[1])
x = Symbol("x")
factors = map(lambda c: series(1 / (1 - x**c), n=amount + 1), COINS)
generating_function = expand(prod(factors))
num_partitions = generating_function.as_coefficients_dict()[x**amount]
print(f"Number of partitions: {num_partitions}")
