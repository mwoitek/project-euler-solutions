/*
Summation of primes
https://projecteuler.net/problem=10
*/

use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let primes: Vec<u32> = read_numbers(String::from("primes_less_than_2000000.txt"));
    let primes_sum: u64 = compute_sum(primes);
    println!("Sum: {}", primes_sum);
}

fn read_numbers(file_name: String) -> Vec<u32> {
    let mut numbers: Vec<u32> = Vec::new();
    let file_path: String = format!("data/{}", file_name);
    let file: File = File::open(file_path).expect("Could not open file!");
    let reader: BufReader<File> = BufReader::new(file);
    for line in reader.lines() {
        let number_str: String = line.unwrap();
        let number: u32 = number_str.parse().unwrap();
        numbers.push(number);
    }
    numbers
}

fn compute_sum(numbers: Vec<u32>) -> u64 {
    let mut sum: u64 = 0;
    numbers.into_iter().for_each(|number| {
        sum += number as u64;
    });
    sum
}
