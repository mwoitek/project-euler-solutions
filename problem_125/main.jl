# Palindromic sums
# https://projecteuler.net/problem=125

# julia main.jl 100000000
# 8.66s user 0.60s system 104% cpu 8.884 total

using Memoize: @memoize

@memoize sum_of_squares(n) = (n * (n + 1) * (2 * n + 1)) ÷ 6

sum_of_squares(i, j) = sum_of_squares(j) - sum_of_squares(i - 1)

function is_palindrome(n)
    digits_ = digits(n)
    digits_ == reverse(digits_)
end

function compute_sum(upper_limit)
    palindromes = Set()
    n = trunc(typeof(upper_limit), sqrt(upper_limit - 1))
    for i in 1:(n - 1)
        for j in (i + 1):n
            s = sum_of_squares(i, j)
            s ≥ upper_limit && continue
            is_palindrome(s) && push!(palindromes, s)
        end
    end
    sum(palindromes)
end

function main()
    upper_limit = parse(Int, ARGS[1])
    answer = compute_sum(upper_limit)
    println("Answer: ", answer)
end

main()
