# Cubic permutations
# https://projecteuler.net/problem=62

# julia main.jl 5 10000

using DataStructures: OrderedDict

function push_line!(cubes_dict::OrderedDict{UInt128,UInt128}, line::String)
    line_parts = parse.(UInt128, split(line, ","))
    cube, permutation = line_parts[1], line_parts[2]
    push!(cubes_dict, cube => permutation)
end

function read_cubes_and_permutations(num_lines::Int = -1)
    cubes_dict = OrderedDict{UInt128,UInt128}()
    open("list_of_cubes.csv", "r") do file
        lines =
            num_lines ≠ -1 ? Iterators.take(eachline(file), num_lines) : eachline(file)
        foreach(Base.Fix1(push_line!, cubes_dict), lines)
    end
    cubes_dict
end

count_cubic_permutations(cube::UInt128, cubes_dict::OrderedDict{UInt128,UInt128}) =
    count(isequal(cubes_dict[cube]), values(cubes_dict))

function find_solution(num_permutations::Int, cubes_dict::OrderedDict{UInt128,UInt128})
    counts =
        Iterators.map(Base.Fix2(count_cubic_permutations, cubes_dict), keys(cubes_dict))
    itr = Iterators.filter(
        p -> last(p) == num_permutations,
        Iterators.zip(keys(cubes_dict), counts),
    )
    Iterators.take(itr, 1) |> collect |> first |> first
end

function main()
    num_permutations = parse(Int, ARGS[1])
    if length(ARGS) == 2
        num_lines = parse(Int, ARGS[2])
        cubes_dict = read_cubes_and_permutations(num_lines)
    else
        cubes_dict = read_cubes_and_permutations()
    end
    solution = find_solution(num_permutations, cubes_dict)
    println("Solution: ", solution)
end

main()
