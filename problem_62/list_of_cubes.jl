# julia list_of_cubes.jl 15000

function cube_generator(max_int::UInt128 = UInt128(2_000_000))
    Channel{UInt128}() do ch
        foreach(i -> put!(ch, i^3), zero(UInt128):max_int)
    end
end

arr_to_int(arr::Vector{UInt128}) = parse(UInt128, join(arr))

decreasing_permutation(number::UInt128) =
    digits(UInt128, number) |> (x -> sort(x, rev = true)) |> arr_to_int

function main()
    max_int = parse(UInt128, ARGS[1])
    open("list_of_cubes.csv", "w") do file
        foreach(
            c -> write(file, "$(c),$(decreasing_permutation(c))\n"),
            cube_generator(max_int),
        )
    end
end

main()
