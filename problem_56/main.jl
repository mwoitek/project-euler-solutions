# Powerful digit sum
# https://projecteuler.net/problem=56

# julia main.jl 99 99

function compute_powers(max_a::BigInt, max_b::BigInt)
    values_a = one(BigInt):max_a
    values_b = one(BigInt):max_b
    Iterators.map(p -> p[1]^p[2], Iterators.product(values_a, values_b))
end

sum_digits(number::BigInt) = sum(digits(number))

function main()
    max_a = parse(BigInt, ARGS[1])
    max_b = parse(BigInt, ARGS[2])
    powers = compute_powers(max_a, max_b)
    max_sum = maximum(sum_digits, powers)
    println("Maximum digital sum: $(max_sum)")
end

main()
