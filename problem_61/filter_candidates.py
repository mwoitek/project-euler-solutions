def read_figurate_numbers() -> list[tuple[str, int]]:
    figurate_numbers = []
    for s in range(3, 9):
        with open(f"figurate_numbers_{s}.txt", "r") as f:
            new_tpls = map(lambda l: (l.strip(), s), f.readlines())
            figurate_numbers.extend(new_tpls)
    return figurate_numbers


def get_candidates(figurate_numbers: list[tuple[str, int]]) -> list[tuple[str, int]]:
    candidates = []
    for n1, s in figurate_numbers:
        fltr = filter(lambda t: t[1] != s, figurate_numbers)
        nums = list(map(lambda t: t[0], fltr))
        cond = any(map(lambda n2: n2.startswith(n1[-2:]), nums))
        if cond and any(map(lambda n2: n2.endswith(n1[:2]), nums)):
            candidates.append((n1, s))
    return candidates


def write_candidates(candidates: list[tuple[str, int]]) -> None:
    with open("candidates.csv", "w") as f:
        f.writelines(map(lambda t: f"{t[0]},{t[1]}\n", candidates))


if __name__ == "__main__":
    figurate_numbers = read_figurate_numbers()
    candidates = get_candidates(figurate_numbers)
    write_candidates(candidates)
