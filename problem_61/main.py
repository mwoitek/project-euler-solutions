# Cyclical figurate numbers
# https://projecteuler.net/problem=61

from itertools import chain


def read_candidates() -> list[tuple[str, int]]:
    candidates = []
    with open("candidates.csv", "r") as f:
        for line in f:
            line_pts = line.split(",")
            candidates.append((line_pts[0], int(line_pts[1])))
    return candidates


def generate_sequences(
    length: int,
    start: tuple[str, int],
    candidates: list[tuple[str, int]],
) -> list[list[tuple[str, int]]]:
    sequences = []

    def helper(
        tpl: tuple[str, int],
        seq: list[tuple[str, int]],
        used: set[int],
    ) -> None:
        if len(seq) == length:
            sequences.append(seq)
        fltr = filter(lambda t: t[1] not in used, candidates)
        for t in filter(lambda t: t[0].startswith(tpl[0][-2:]), fltr):
            helper(t, [*seq, t], {*used, t[1]})

    helper(start, [start], {start[1]})
    return sequences


def is_valid_cycle(seq: list[tuple[str, int]]) -> bool:
    nums = set(map(lambda t: t[0], seq))
    if len(nums) < len(seq):
        return False
    first = seq[0][0]
    last = seq[-1][0]
    return first.startswith(last[-2:])


def get_valid_cycles(length: int) -> list[list[tuple[str, int]]]:
    candidates = read_candidates()
    all_sequences = map(lambda t: generate_sequences(length, t, candidates), candidates)
    valid_cycles = filter(is_valid_cycle, chain.from_iterable(all_sequences))
    return list(valid_cycles)


if __name__ == "__main__":
    cycle = get_valid_cycles(6)[0]
    print("Cycle:")
    for number, s in cycle:
        print(f"{number} -> s = {s}")
    numbers = map(lambda t: int(t[0]), cycle)
    print(f"Sum: {sum(numbers)}")
