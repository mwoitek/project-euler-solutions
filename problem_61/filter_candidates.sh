#!/bin/bash
num_digits=4
for ((s = 3; s < 9; s++)); do
  python3 list_of_figurate_numbers.py "$s" "$num_digits"
done
python3 filter_candidates.py
trash-put figurate_numbers*.txt
