from functools import partial
from itertools import count
from itertools import takewhile
from sys import argv


# https://en.wikipedia.org/wiki/Polygonal_number
def figurate_number(s: int, n: int) -> int:
    return ((s - 2) * n**2 - (s - 4) * n) // 2


def create_list(s: int, num_digits: int) -> list[int]:
    numbers = map(partial(figurate_number, s), count(1))
    itr = takewhile(lambda n: n <= 10**num_digits - 1, numbers)
    return list(filter(lambda n: n >= 10 ** (num_digits - 1), itr))


def write_list(numbers: list[int], file_name: str) -> None:
    with open(file_name, "w") as f:
        f.writelines(map(lambda n: f"{n}\n", numbers))


if __name__ == "__main__":
    s = int(argv[1])
    num_digits = int(argv[2])
    numbers = create_list(s, num_digits)
    file_name = f"figurate_numbers_{s}.txt"
    write_list(numbers, file_name)
