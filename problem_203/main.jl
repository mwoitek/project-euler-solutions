# Squarefree Binomial Coefficients
# https://projecteuler.net/problem=203

# julia main.jl 51

using Primes: primes

get_primes(upper_limit) = primes(upper_limit)

# https://en.wikipedia.org/wiki/Legendre's_formula
function compute_multiplicity(n, p)
    terms = Iterators.map(i -> fld(n, p^i), Iterators.countfrom(1, 1))
    non_zero_terms = Iterators.takewhile(>(0), terms)
    sum(non_zero_terms; init = 0)
end

function factor_factorial(n, primes_arr)
    prime_factors = filter(≤(n), primes_arr)
    multiplicities = Iterators.map(Base.Fix1(compute_multiplicity, n), prime_factors)
    Dict(p => m for (p, m) in Iterators.zip(prime_factors, multiplicities) if m > 0)
end

function factor_binomial_coefficient(n, k, primes_arr)
    factors_num = factor_factorial(n, primes_arr)
    factors_den =
        mergewith(+, factor_factorial(k, primes_arr), factor_factorial(n - k, primes_arr))
    factors_ratio = mergewith(-, factors_num, factors_den)
    Dict(p => m for (p, m) in pairs(factors_ratio) if m > 0)
end

is_squarefree(factors_dict) = all(Iterators.map(isequal(1), values(factors_dict)))

compute_binomial_coefficient(factors_dict) =
    reduce(*, (p^m for (p, m) in pairs(factors_dict)))

function sum_squarefree_coefficients(num_rows)
    ns_ks = Iterators.flatten(
        Iterators.map(n -> Iterators.map(k -> (n, k), 1:(n - 1)), 2:(num_rows - 1)),
    )
    primes_arr = get_primes(num_rows - 1)
    factorizations =
        Iterators.map(nk -> factor_binomial_coefficient(nk..., primes_arr), ns_ks)
    squarefree_coeffs = Iterators.filter(is_squarefree, factorizations)
    distinct_coeffs = Set(Iterators.map(compute_binomial_coefficient, squarefree_coeffs))
    sum(distinct_coeffs; init = 1)
end

function main()
    num_rows = parse(Int, ARGS[1])
    answer = sum_squarefree_coefficients(num_rows)
    println("Answer: ", answer)
end

main()
