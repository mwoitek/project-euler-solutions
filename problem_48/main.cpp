// Self powers
// https://projecteuler.net/problem=48

#include <iostream>

using ullong = unsigned long long;

template <typename Number, typename UInt>
Number myPow(Number b, UInt e);
template <typename UInt>
UInt modMultiply(UInt x, UInt y, UInt m);
template <typename UInt>
UInt modPow(UInt b, UInt e, UInt m);
template <typename UInt>
UInt lastDigitsSum(UInt numDigits, UInt lastExponent);

int main() {
  constexpr ullong numDigits = 10;
  constexpr ullong lastExponent = 1000;

  auto lastDigits = lastDigitsSum(numDigits, lastExponent);
  std::cout << "Last " << numDigits << " digits of the sum: " << lastDigits << '\n';

  return 0;
}

template <typename Number, typename UInt>
Number myPow(Number b, UInt e) {
  Number result = Number(1);

  while (e > 0) {
    if (e % 2 != 0) {
      result *= b;
    }

    b *= b;
    e /= 2;
  }

  return result;
}

template <typename UInt>
UInt modMultiply(UInt x, UInt y, UInt m) {
  UInt result = UInt(0);
  x %= m;
  y %= m;

  while (x > 0) {
    if (x % 2 != 0) {
      result = (result + y) % m;
    }

    y = (2 * y) % m;
    x /= 2;
  }

  return result;
}

template <typename UInt>
UInt modPow(UInt b, UInt e, UInt m) {
  UInt result = UInt(1);
  b %= m;

  while (e > 0) {
    if (e % 2 != 0) {
      result = modMultiply(result, b, m);
    }

    b = modMultiply(b, b, m);
    e /= 2;
  }

  return result;
}

template <typename UInt>
UInt lastDigitsSum(UInt numDigits, UInt lastExponent) {
  UInt sum = UInt(0);
  auto m = myPow(UInt(10), numDigits);

  for (UInt i = UInt(1); i <= lastExponent; i++) {
    sum = (sum + modPow(i, i, m)) % m;
  }

  return sum;
}
