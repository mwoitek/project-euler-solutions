# Cyclic numbers
# https://projecteuler.net/problem=358

# 55.58s user 0.64s system 100% cpu 55.876 total

const lo = 724637681
const hi = 735294118
const m = 10^5
const primes_file_name = "/home/woitek/repos/project_euler/primes/primes.txt"

function read_primes()
    itr_1 = Iterators.map(Base.Fix1(parse, Int), eachline(primes_file_name))
    itr_2 = Iterators.dropwhile(<(lo), itr_1)
    Iterators.takewhile(<(hi), itr_2)
end

function has_correct_beginning(p::Int)
    digits = Vector{Int}(undef, 11)
    r = 1
    for i in 1:11
        digits[i], r = divrem(10 * r, p)
    end
    join(digits) == "00000000137"
end

# https://en.wikipedia.org//wiki/Cyclic_number#Form_of_cyclic_numbers
function has_correct_end(p::Int)
    a = invmod(p, m)
    b = powermod(10, p - 1, m)
    mod(a * (b - 1), m) == 56789
end

has_correct_form(p::Int) = has_correct_beginning(p) && has_correct_end(p)

find_candidates() = Iterators.filter(has_correct_form, read_primes())

# https://en.wikipedia.org//wiki/Cyclic_number#Construction_of_cyclic_numbers
function is_cyclic_number(p::Int)
    r = 1
    t = 0
    while true
        t += 1
        r = mod(10 * r, p)
        r ≠ 1 || break
    end
    t == p - 1
end

function compute_sum(p::Int)
    sum_of_digits = 0
    r = 1
    while true
        d, r = divrem(10 * r, p)
        sum_of_digits += d
        r ≠ 1 || break
    end
    sum_of_digits
end

function main()
    candidates = find_candidates()
    prime = Iterators.filter(is_cyclic_number, candidates) |> first
    answer = compute_sum(prime)
    println("Answer: ", answer)
end

main()
