# Square root convergents
# https://projecteuler.net/problem=57

# julia main.jl 1000

function generate_rational_approximations(num_approximations::Int)
    approximations = Rational{BigInt}[]
    num = BigInt(3)
    den = BigInt(2)
    for _ in 1:num_approximations
        push!(approximations, num // den)
        den += num
        num = denominator(approximations[end]) + den
    end
    approximations
end

numerator_has_more_digits(fraction::Rational{BigInt}) =
    (ndigits ∘ numerator)(fraction) > (ndigits ∘ denominator)(fraction)

function main()
    num_approximations = parse(Int, ARGS[1])
    approximations = generate_rational_approximations(num_approximations)
    answer = count(numerator_has_more_digits, approximations)
    println("Number of fractions: $(answer)")
end

main()
