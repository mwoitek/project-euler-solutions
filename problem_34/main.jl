# Digit factorials
# https://projecteuler.net/problem=34

# julia main.jl 1000000

function create_factorials_dict()
    Dict(d => factorial(d) for d::Int = 0:9)
end

function sum_digit_factorials(number::Int, factorials_dict::Dict{Int,Int})
    sum(
        map(
            d -> factorials_dict[d],
            digits(number)
        )
    )
end

function find_solutions(upper_limit::Int)
    factorials_dict = create_factorials_dict()
    filter(
        n -> n == sum_digit_factorials(n, factorials_dict),
        collect(3:upper_limit)
    )
end

function main()
    upper_limit = parse(Int, ARGS[1])
    solutions = find_solutions(upper_limit)
    println("Solutions:")
    for solution in solutions
        println(solution)
    end
    println("Sum: $(sum(solutions))")
end

main()
