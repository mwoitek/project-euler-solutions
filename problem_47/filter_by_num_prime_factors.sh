#!/bin/bash
in_file="$1"
num_prime_factors="$2"
out_file="$(basename -s .txt "$in_file")_${num_prime_factors}.txt"
sed -n "/:${num_prime_factors}$/p" "$in_file" |
  cut -d ':' -f -2 >"$out_file"
