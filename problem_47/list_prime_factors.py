from collections.abc import Iterator
from concurrent.futures import ProcessPoolExecutor
from sys import argv

from sympy import composite
from sympy.ntheory import primefactors


def get_prime_factors(number: int) -> str:
    prime_factors = primefactors(number)
    return (
        str(number)
        + ":"
        + ",".join([str(f) for f in prime_factors])
        + ":"
        + str(len(prime_factors))
    )


def write_prime_factors(prime_factors: Iterator[str], file_name: str) -> None:
    with open(file_name, mode="w") as file:
        for pf in prime_factors:
            file.write(f"{pf}\n")


if __name__ == "__main__":
    idx_begin = int(argv[1])
    idx_end = int(argv[2])
    num_processes = int(argv[3])

    indexes = [i for i in range(idx_begin, idx_end + 1)]
    file_name = f"prime_factors_{idx_begin}_{idx_end}.txt"

    with ProcessPoolExecutor(max_workers=num_processes) as executor:
        composite_numbers = executor.map(composite, indexes)
        prime_factors = executor.map(get_prime_factors, composite_numbers)
        write_prime_factors(prime_factors, file_name)
