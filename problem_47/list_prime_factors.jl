import Primes

function get_prime_factors(number::Int)
    prime_factors = collect(keys(Primes.factor(number)))
    prime_factors_str = ""
    for pf in prime_factors
        prime_factors_str *= "$(pf),"
    end
    prime_factors_str = prime_factors_str[1:end-1]
    num_prime_factors = length(prime_factors)
    "$(number):$(prime_factors_str):$(num_prime_factors)"
end

function get_list_prime_factors(number_begin::Int, number_end::Int)
    [get_prime_factors(number) for number = number_begin:number_end]
end

function write_output(list_prime_factors::Vector{String}, file_name::String)
    open(file_name, "w") do file
        for s in list_prime_factors
            write(file, "$(s)\n")
        end
    end
end

function main()
    number_begin = parse(Int, ARGS[1])
    number_end = parse(Int, ARGS[2])
    file_name = "list_prime_factors_$(number_begin)_$(number_end).txt"
    list_prime_factors = get_list_prime_factors(number_begin, number_end)
    write_output(list_prime_factors, file_name)
end

main()
