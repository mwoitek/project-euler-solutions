from collections.abc import Iterator
from sys import argv

from more_itertools import pairwise
from more_itertools import windowed


def read_numbers(file_name: str) -> list[int]:
    numbers = []
    with open(file_name, "r") as file:
        for line in file:
            number = int(line.split(":")[0])
            numbers.append(number)
    return numbers


def get_ranges(num_elements: int, num_consecutive: int) -> list[tuple[int, ...]]:
    indexes = range(num_elements)
    ranges_gen = windowed(indexes, num_consecutive, fillvalue=-1)
    return list(ranges_gen)


def are_consecutive(numbers: list[int]) -> bool:
    return all(map(lambda p: p[1] - p[0] == 1, pairwise(numbers)))


def test_range(numbers: list[int], range_: tuple[int, ...]) -> list[int]:
    numbers_slice = [numbers[i] for i in range_]
    return numbers_slice if are_consecutive(numbers_slice) else []


def write_output(consecutive_numbers: Iterator[list[int]], file_name: str) -> None:
    with open(file_name, "w") as file:
        for lst in filter(lambda l: len(l) > 0, consecutive_numbers):
            line = ",".join([str(n) for n in lst]) + "\n"
            file.write(line)


if __name__ == "__main__":
    input_file = argv[1]
    num_consecutive = int(argv[2])

    numbers = read_numbers(input_file)
    ranges = get_ranges(len(numbers), num_consecutive)
    output_file = input_file.split(".")[0] + "_consecutive.txt"

    consecutive_numbers = map(lambda r: test_range(numbers, r), ranges)
    write_output(consecutive_numbers, output_file)
