# Distinct primes factors
# https://projecteuler.net/problem=47

from sys import argv

from more_itertools import pairwise

FactorsDict = dict[int, set[int]]


def create_factors_dict(file_name: str) -> FactorsDict:
    factors_dict = {}
    with open(file_name, mode="r") as file:
        for line in file:
            line_parts = line.strip().split(":")
            number = int(line_parts[0])
            prime_factors = set([int(f) for f in line_parts[1].split(",")])
            factors_dict[number] = prime_factors
    return factors_dict


def get_candidates(file_name: str) -> list[list[int]]:
    candidates = []
    with open(file_name, mode="r") as file:
        for line in file:
            candidate = [int(n) for n in line.strip().split(",")]
            candidates.append(candidate)
    return candidates


def test_candidate(candidate: list[int], factors_dict: FactorsDict) -> bool:
    factors_sets = [factors_dict[n] for n in candidate]
    return all(map(lambda p: len(p[0].intersection(p[1])) == 0, pairwise(factors_sets)))


def find_solutions(file_name_factors: str, file_name_candidates: str) -> list[list[int]]:
    solutions = []
    factors_dict = create_factors_dict(file_name_factors)
    candidates = get_candidates(file_name_candidates)
    for candidate in candidates:
        if test_candidate(candidate, factors_dict):
            solutions.append(candidate)
    return solutions


def write_solutions(solutions: list[list[int]], file_name: str) -> None:
    with open(file_name, mode="w") as file:
        for solution in solutions:
            line = ",".join([str(n) for n in solution]) + "\n"
            file.write(line)


if __name__ == "__main__":
    file_name_factors = argv[1]
    file_name_candidates = argv[2]
    file_name_output = argv[3]

    solutions = find_solutions(file_name_factors, file_name_candidates)
    if len(solutions) > 0:
        print(f"Solution: {solutions[0]}")
    else:
        print("Could not find solution!")
    write_solutions(solutions, file_name_output)
