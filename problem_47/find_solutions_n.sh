#!/bin/bash

in_file="$1"
n="$2"

./filter_by_num_prime_factors.sh "$in_file" "$n"
out_file_1="$(basename -s .txt "$in_file")_${n}.txt"

python3 filter_consecutive_numbers.py "$out_file_1" "$n"
out_file_2="$(basename -s .txt "$out_file_1")_consecutive.txt"

python3 main.py "$out_file_1" "$out_file_2" "solutions_${n}.txt"
trash-put "$out_file_1" "$out_file_2"
