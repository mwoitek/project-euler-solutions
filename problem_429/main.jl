# Sum of squares of unitary divisors
# https://projecteuler.net/problem=429

# julia main.jl 100000000 1000000009
# 2.92s user 0.37s system 113% cpu 2.901 total

const primes_file_name = "/home/woitek/repos/project_euler/primes/primes.txt"

function primes_generator(upper_limit)
    primes = Iterators.map(Base.Fix1(parse, Int), eachline(primes_file_name))
    Iterators.takewhile(≤(upper_limit), primes)
end

function compute_multiplicity(n, p)
    terms = Iterators.map(i -> fld(n, widen(p)^i), Iterators.countfrom(1, 1))
    non_zero_terms = Iterators.takewhile(>(0), terms)
    convert(Int, sum(non_zero_terms; init = 0))
end

get_prime_factors_and_multiplicities(n) =
    Iterators.map(p -> (p, compute_multiplicity(n, p)), primes_generator(n))

# https://mathworld.wolfram.com/UnitaryDivisorFunction.html
function unitary_divisor_function(n)
    s = 1
    for (prime, multiplicity) in get_prime_factors_and_multiplicities(n)
        s *= 1 + prime^(2 * multiplicity)
    end
    s
end

function unitary_divisor_function(n, m)
    s = 1
    for (prime, multiplicity) in get_prime_factors_and_multiplicities(n)
        s = mod(s * (1 + powermod(prime, 2 * multiplicity, m)), m)
    end
    s
end

function main()
    n = parse(Int, ARGS[1])
    answer = -1
    if length(ARGS) > 1
        m = parse(Int, ARGS[2])
        answer = unitary_divisor_function(n, m)
    else
        answer = unitary_divisor_function(n)
    end
    println("Answer: ", answer)
end

main()
