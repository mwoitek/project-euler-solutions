# Counting summations
# https://projecteuler.net/problem=76

# python3 main.py 100

from functools import cache
from sys import argv


@cache
def divisor_function(number: int) -> int:
    divisors = {1, number}
    d = 2
    while d**2 <= number:
        q, r = divmod(number, d)
        if r == 0:
            divisors.update({d, q})
        d += 1
    return sum(divisors)


@cache
def partition_function(number: int) -> int:
    # https://mathworld.wolfram.com/PartitionFunctionP.html
    if number == 0:
        return 1
    sum_ = sum(
        map(
            lambda k: divisor_function(number - k) * partition_function(k),
            range(number),
        )
    )
    return sum_ // number


if __name__ == "__main__":
    number = int(argv[1])
    # `number` is also counted amongst the partitions. So we need to subtract 1.
    num_summations = partition_function(number) - 1
    print(f"Number of summations: {num_summations}")
