# Pandigital products
# https://projecteuler.net/problem=32

from itertools import combinations
from operator import itemgetter


def read_pandigital_numbers() -> list[str]:
    with open("pandigital_numbers_1_to_9.txt", "r") as file:
        return [line.strip() for line in file]


def get_product_parts(number: str, idx_mul: int, idx_eq: int) -> list[int]:
    multiplicand = int(number[:idx_mul])
    multiplier = int(number[idx_mul:idx_eq])
    product = int(number[idx_eq:])
    return (
        [multiplicand, multiplier, product]
        if multiplicand * multiplier == product
        else []
    )


def get_pandigital_products(number: str) -> list[list[int]]:
    pandigital_products = []
    for idx_mul, idx_eq in combinations(range(1, 8), 2):
        product_parts = get_product_parts(number, idx_mul, idx_eq)
        if len(product_parts) > 0:
            pandigital_products.append(product_parts)
    return pandigital_products


def get_all_pandigital_products() -> list[list[int]]:
    all_pandigital_products = []
    pandigital_numbers = read_pandigital_numbers()
    for pandigital_products in map(get_pandigital_products, pandigital_numbers):
        all_pandigital_products += pandigital_products
    return all_pandigital_products


def compute_sum_products(all_pandigital_products: list[list[int]]) -> int:
    products_set = set(map(itemgetter(2), all_pandigital_products))
    return sum(products_set)


def write_output(all_pandigital_products: list[list[int]]) -> None:
    with open("pandigital_products.txt", "w") as file:
        for prod in map(lambda l: f"{l[0]} * {l[1]} = {l[2]}", all_pandigital_products):
            file.write(f"{prod}\n")


if __name__ == "__main__":
    all_pandigital_products = get_all_pandigital_products()
    answer = compute_sum_products(all_pandigital_products)
    print(f"Sum: {answer}")
    write_output(all_pandigital_products)
