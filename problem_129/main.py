# Repunit divisibility
# https://projecteuler.net/problem=129

from itertools import dropwhile
from itertools import takewhile
from sys import argv

from more_itertools import iterate


def read_ns(lower_limit: int, n_max: int) -> list[int]:
    with open("upper_bounds.csv", "r") as file:
        ns = map(lambda l: int(l.split(",")[0]), file)
        ns_l = dropwhile(lambda n: n <= lower_limit, ns)
        ns_u = takewhile(lambda n: n <= n_max, ns_l)
        return list(ns_u)


def compute_a(n: int) -> int:
    b = 10 % n
    rs = iterate(lambda r: (b * r + 1) % n, 1)
    ks_rs = enumerate(rs, start=1)
    itr = dropwhile(lambda p: p[1] != 0, ks_rs)
    k, _ = next(itr)
    return k


def find_n_min(lower_limit: int, ns: list[int]) -> int:
    n_min = ns[-1]
    for n in ns[:-1]:
        a = compute_a(n)
        if a > lower_limit and n < n_min:
            n_min = n
    return n_min


if __name__ == "__main__":
    lower_limit = int(argv[1])
    n_max = int(argv[2])
    ns = read_ns(lower_limit, n_max)
    answer = find_n_min(lower_limit, ns)
    print(f"Answer: {answer}")
