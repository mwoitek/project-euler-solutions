# This problem is related to the problem of finding the period k of the
# repeating decimal 1/n. And I already have a long list of such periods. I'll
# use these results to obtain upper bounds for the minimum values of n.

from csv import DictReader

PERIODS_FILE_NAME = "/home/woitek/repos/project_euler/problem_820/lengths.csv"


def read_periods() -> dict[int, int]:
    periods_dict = {}
    with open(PERIODS_FILE_NAME, mode="r", newline="") as csv_file:
        reader = DictReader(csv_file, fieldnames=["n", "len_non_repeat", "period"])
        for row in reader:
            if int(row["len_non_repeat"]) > 0:  # not coprime to 10
                continue
            periods_dict[int(row["n"])] = int(row["period"])
    return periods_dict


def write_periods(periods_dict: dict[int, int]) -> None:
    with open("upper_bounds.csv", "w") as file:
        for n, period in periods_dict.items():
            file.write(f"{n},{period}\n")


if __name__ == "__main__":
    periods_dict = read_periods()
    write_periods(periods_dict)
