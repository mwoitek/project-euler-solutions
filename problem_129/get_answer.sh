#!/bin/bash
# ./get_answer.sh 1000000
lower_limit="$1"
vals="$(awk -F',' -v lower_limit="$lower_limit" '$2>lower_limit { print $0; exit }' upper_bounds.csv)"
n="$(echo "$vals" | cut -d',' -f1)"
python3 main.py "$lower_limit" "$n"
