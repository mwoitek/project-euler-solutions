# Distinct powers
# https://projecteuler.net/problem=29

function compute_distinct_powers(max_a::BigInt, max_b::BigInt)
    distinct_powers = Set{BigInt}([])
    for a in BigInt(2):max_a
        for b in BigInt(2):max_b
            push!(distinct_powers, a^b)
        end
    end
    distinct_powers
end

function write_distinct_powers(distinct_powers::Set{BigInt}, file_name::String)
    open(file_name, "w") do file
        for power in (sort ∘ collect)(distinct_powers)
            write(file, "$(power)\n")
        end
    end
end

function main()
    max_a = parse(BigInt, ARGS[1])
    max_b = parse(BigInt, ARGS[2])
    distinct_powers = compute_distinct_powers(max_a, max_b)
    println("Number of distinct powers: $(length(distinct_powers))")
    file_name = "distinct_powers_$(max_a)_$(max_b).txt"
    write_distinct_powers(distinct_powers, file_name)
end

main()
