// Longest Collatz sequence
// https://projecteuler.net/problem=14

#include <iostream>

#include "CollatzLongest.h"

int main() {
  const unsigned int N_MAX = 1000000;

  CollatzLongest cl = CollatzLongest(N_MAX);
  cl.computeChainSizes();

  const unsigned int nLongestChain = cl.getNLongestChain();
  std::cout << "n that produces the longest chain: " << nLongestChain << '\n';

  cl.printChainSizesToFile();

  return 0;
}
