#ifndef COLLATZLONGEST_H
#define COLLATZLONGEST_H

#include <map>

class CollatzLongest {
 private:
  unsigned int nMax;
  std::map<unsigned int, unsigned int> chainSizes;
  void computeChainSizesFromN(const unsigned int& n);

 public:
  explicit CollatzLongest(const unsigned int& nMax);
  static unsigned int collatzFunction(const unsigned int& n);
  void computeChainSizes();
  unsigned int getNLongestChain();
  void printChainSizesToFile();
};

#endif  // PROBLEM_14_COLLATZLONGEST_H_
