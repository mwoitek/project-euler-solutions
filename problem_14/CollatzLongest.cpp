#include "CollatzLongest.h"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <list>

CollatzLongest::CollatzLongest(const unsigned int& nMax) {
  if (nMax < 3) {
    throw std::invalid_argument("nMax must satisfy nMax >= 3");
  }
  this->nMax = nMax;
  this->chainSizes = {{1, 4}, {2, 2}};
}

unsigned int CollatzLongest::collatzFunction(const unsigned int& n) {
  if (n % 2 == 0) {
    return n / 2;
  }
  return 3 * n + 1;
}

void CollatzLongest::computeChainSizesFromN(const unsigned int& n) {
  if (chainSizes.find(n) != chainSizes.end()) {
    return;
  }

  unsigned int term = n;
  unsigned int chainSize = 1;
  std::list<unsigned int> unseenNs;
  bool stop = false;

  while (!stop) {
    unseenNs.push_back(term);
    term = collatzFunction(term);
    if (chainSizes.find(term) != chainSizes.end()) {
      chainSize += chainSizes[term];
      stop = true;
    } else {
      chainSize++;
    }
  }

  for (auto it = unseenNs.begin(); it != unseenNs.end(); it++) {
    if (*it < nMax) {
      chainSizes[*it] = chainSize;
    }
    chainSize--;
  }
}

void CollatzLongest::computeChainSizes() {
  for (unsigned int n = 3; n < nMax; n++) {
    computeChainSizesFromN(n);
  }
}

unsigned int CollatzLongest::getNLongestChain() {
  using pt = decltype(chainSizes)::value_type;
  const auto comp = [](const pt& p1, const pt& p2) { return p1.second < p2.second; };
  const auto maxChainSize = std::ranges::max_element(chainSizes.begin(), chainSizes.end(), comp);
  return maxChainSize->first;
}

void CollatzLongest::printChainSizesToFile() {
  std::ofstream outFile;
  outFile.open("chain_sizes.txt");

  const int width1 = static_cast<int>(std::ceil(std::log10(nMax)));
  const int width2 = 14;

  outFile << std::setw(width1) << 'n';
  outFile << std::setw(width2) << "chain size" << '\n';
  for (const auto& p : chainSizes) {
    outFile << std::setw(width1) << p.first;
    outFile << std::setw(width2) << p.second << '\n';
  }

  outFile.close();
}
