# (prime-k) factorial
# https://projecteuler.net/problem=381

# julia -t 2 main.jl
# 51.57s user 6.68s system 149% cpu 39.009 total

const primes_file_name = "/home/woitek/repos/project_euler/primes/primes.txt"

function primes_generator(ch::Channel{Int})
    open(primes_file_name, "r") do file
        for line in eachline(file)
            p = parse(Int, line)
            5 ≤ p < 100000000 && put!(ch, p)
        end
    end
end

# https://en.wikipedia.org/wiki/Wilson's_theorem
s(p::Int) = mod(15 * invmod(24, p) - 1, p)

function compute_summation()
    summation = Threads.Atomic{Int}(0)
    Threads.foreach(Channel{Int}(primes_generator)) do p
        Threads.atomic_add!(summation, s(p))
    end
    summation[]
end

function main()
    answer = compute_summation()
    println("Answer: ", answer)
end

main()
