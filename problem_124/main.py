# Ordered radicals
# https://projecteuler.net/problem=124

# python3 main.py list_prime_factors_2_100000.txt 10000

from math import prod
from operator import itemgetter
from sys import argv


def create_factors_dict(file_name: str) -> dict[int, list[int]]:
    factors_dict = {}
    factors_dict[1] = [1]
    with open(file_name, "r") as file:
        for line in file:
            line_parts = line.strip().split(":")
            num = int(line_parts[0])
            factors = list(map(int, line_parts[1].split(",")))
            factors_dict[num] = factors
    return factors_dict


def compute_radicals(factors_dict: dict[int, list[int]]) -> dict[int, int]:
    return {num: prod(factors) for num, factors in factors_dict.items()}


if __name__ == "__main__":
    file_name = argv[1]
    k = int(argv[2])
    factors_dict = create_factors_dict(file_name)
    radicals_dict = compute_radicals(factors_dict)
    ordered_radicals = sorted(radicals_dict.items(), key=itemgetter(1, 0))
    print(f"Answer: {ordered_radicals[k - 1][0]}")
