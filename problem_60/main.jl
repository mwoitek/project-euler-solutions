# Prime pair sets
# https://projecteuler.net/problem=60

# julia main.jl 5 50000 10
# 28.24s user 0.35s system 101% cpu 28.222 total

using Combinatorics: combinations
using Primes: isprime

const init_min_sum = 107623 # Sum corresponding to solution [5381, 5507, 7877, 41621, 47237]
const primes_file_name = "/home/woitek/repos/project_euler/primes/primes.txt"

function read_primes(upper_limit::Int)
    primes_itr = Iterators.map(Base.Fix1(parse, Int), eachline(primes_file_name))
    Iterators.takewhile(≤(upper_limit), primes_itr) |> Base.Fix1(collect, Int)
end

function find_empty(candidate::Vector{Int})
    idx = findfirst(isequal(0), candidate)
    isnothing(idx) ? -1 : idx
end

concatenate(x::String, y::String) = parse(Int, x * y)

function test_pair(p1::Int, p2::Int)
    (p1 == 0 || p2 == 0) && return true
    s1, s2 = string(p1), string(p2)
    isprime(concatenate(s1, s2)) && isprime(concatenate(s2, s1))
end

test_all_pairs(candidate::Vector{Int}) =
    all(Iterators.map(p -> test_pair(p...), combinations(candidate, 2)))

mutable struct MinSum
    primes_set::Vector{Int}
    val::Int
    MinSum() = new(Int[], init_min_sum)
end

function find_solution!(candidate::Vector{Int}, my_min::MinSum, primes::Vector{Int})
    idx = find_empty(candidate)
    idx == -1 && return true
    for p in filter(>(candidate[idx - 1]), primes)
        sum(candidate) + p > my_min.val && break
        candidate[idx] = p
        if !test_all_pairs(candidate)
            candidate[idx] = 0
            continue
        end
        find_solution!(candidate, my_min, primes) && return true
        candidate[idx] = 0
    end
    false
end

function update_min!(my_min::MinSum, set_size::Int, first_prime::Int, primes::Vector{Int})
    candidate = zeros(Int, set_size)
    candidate[1] = first_prime
    if find_solution!(candidate, my_min, primes)
        my_min.primes_set = candidate
        my_min.val = sum(candidate)
    end
end

function compute_min_sum(set_size::Int, upper_limit::Int, num_primes::Int)
    my_min = MinSum()
    primes = read_primes(upper_limit)
    deleteat!(primes, [1 3])
    first_primes = @view primes[1:num_primes]
    foreach(f -> update_min!(my_min, set_size, f, primes), first_primes)
    my_min
end

function main()
    set_size = parse(Int, ARGS[1])
    upper_limit = parse(Int, ARGS[2])
    num_primes = parse(Int, ARGS[3])
    min_sum = compute_min_sum(set_size, upper_limit, num_primes)
    if min_sum.val == MinSum().val
        println("No solution was found!")
    else
        println("Solution: ", min_sum.primes_set)
        println("Sum: ", min_sum.val)
    end
end

main()
