# Pandigital Fibonacci ends
# https://projecteuler.net/problem=104

# 487.84s user 1.13s system 99% cpu 8:09.36 total

using StaticArrays

setprecision(BigFloat, 1024)
const ϕ = Base.MathConstants.golden |> BigFloat
const sqrt_5 = 5 |> BigFloat |> sqrt

function read_pandigital_numbers()
    pandigital_numbers = open("pandigital_numbers_1_to_9.txt", "r") do file
        readlines(file)
    end
    Set(pandigital_numbers)
end

# https://en.wikipedia.org//wiki/Fibonacci_number#Computation_by_rounding
min_fibonacci_index(lower_limit::BigInt) =
    lower_limit * sqrt_5 - 1 / 2 |> Base.Fix1(log, ϕ) |> Base.Fix1(ceil, Int)

# https://en.wikipedia.org//wiki/Fibonacci_number#Matrix_form
function fibonacci_generator(min_n::Int)
    M = SMatrix{2,2,BigInt}([1 1; 1 0])^(min_n + 1)
    M_3 = SMatrix{2,2,BigInt}([3 2; 2 1])
    Channel{Tuple{Int,BigInt}}() do ch
        for n in Iterators.countfrom(min_n, 3)
            put!(ch, (n, M[2, 2]))
            put!(ch, (n + 1, M[1, 2]))
            put!(ch, (n + 2, M[1, 1]))
            M *= M_3
        end
    end
end

function starts_with_pandigital(number::BigInt, pandigital_numbers::Set{String})
    number |> string |> (s -> s[1:9]) |> Base.Fix2(∈, pandigital_numbers)
end

function ends_with_pandigital(number::BigInt, pandigital_numbers::Set{String})
    number |> string |> (s -> s[(end - 8):end]) |> Base.Fix2(∈, pandigital_numbers)
end

function has_property(number::BigInt, pandigital_numbers::Set{String})
    starts_with_pandigital(number, pandigital_numbers) &&
        ends_with_pandigital(number, pandigital_numbers)
end

function find_solution()
    pandigital_numbers = read_pandigital_numbers()
    min_n = min_fibonacci_index(BigInt(100_000_000_000_000_000))
    itr = Iterators.filter(
        Base.Fix2(has_property, pandigital_numbers) ∘ last,
        fibonacci_generator(min_n),
    )
    Iterators.take(itr, 1) |> collect |> first |> first
end

function main()
    solution = find_solution()
    println("Solution: ", solution)
end

main()
