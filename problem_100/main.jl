# Arranged probability
# https://projecteuler.net/problem=100

# julia main.jl 1000000000000
# 1.13s user 0.36s system 136% cpu 1.092 total

# b = number of blue discs
# t = total number of discs

# b and t satisfy the negative Pell equation
# (2 * t - 1)^2 - 2 * (2 * b - 1)^2 = -1

function convergents_of_sqrt_2()
    num = BigInt(1)
    den = BigInt(1)
    tmp = den
    Channel{Rational{BigInt}}() do ch
        while true
            put!(ch, num // den)
            den += num
            num = den + tmp
            tmp = den
        end
    end
end

is_solution(a, b) = a^2 - 2 * b^2 == -1

is_solution(convergent::Rational{BigInt}) =
    is_solution(numerator(convergent), denominator(convergent))

function fundamental_solution()
    a_min, b_min = BigInt(0), BigInt(1)
    for convergent in convergents_of_sqrt_2()
        if is_solution(convergent)
            a_min = numerator(convergent)
            b_min = denominator(convergent)
            break
        end
    end
    (a_min, b_min)
end

# https://en.wikipedia.org/wiki/Pell's_equation#The_negative_Pell's_equation
function solutions_pell_equation()
    a, b = fundamental_solution()
    c_1 = a^2 + 2 * b^2
    c_2 = 2 * a * b
    Channel{NTuple{2,BigInt}}() do ch
        while true
            put!(ch, (a, b))
            a, b = a * c_1 + 2 * b * c_2, b * c_1 + a * c_2
        end
    end
end

convert_solution((a, b)::NTuple{2,BigInt}) = ((a + 1) ÷ 2, (b + 1) ÷ 2)

function main()
    lower_limit = parse(BigInt, ARGS[1])
    solutions = Iterators.map(convert_solution, solutions_pell_equation())
    answer = Iterators.dropwhile(≤(lower_limit) ∘ first, solutions) |> first |> last
    println("Answer: ", answer)
end

main()
