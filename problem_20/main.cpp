// Factorial digit sum
// https://projecteuler.net/problem=20

#include <boost/multiprecision/cpp_int.hpp>
#include <iostream>
#include <numeric>

template <typename T>
T computeFactorial(const unsigned num);
template <typename T>
unsigned computeSumDigits(const T& num);

int main() {
  const unsigned NUM = 100;
  static_assert(NUM <= 170);

  using uint_t = boost::multiprecision::uint1024_t;
  const auto factorial = computeFactorial<uint_t>(NUM);
  std::cout << NUM << "! = " << factorial << '\n';

  const unsigned sum = computeSumDigits(factorial);
  std::cout << "Sum of the digits: " << sum << '\n';

  return 0;
}

template <typename T>
T computeFactorial(const unsigned num) {
  T product = T(1);
  for (unsigned i = 2; i <= num; i++) {
    product *= i;
  }
  return product;
}

template <typename T>
unsigned computeSumDigits(const T& num) {
  std::stringstream tmp;
  tmp << num;
  const std::string strNum = tmp.str();

  const auto sumFunc = [](const unsigned sum, const char digit) {
    return sum + (unsigned)(digit - '0');
  };
  return std::accumulate(strNum.begin(), strNum.end(), (unsigned)0, sumFunc);
}
