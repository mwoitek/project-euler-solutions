# Spiral primes
# https://projecteuler.net/problem=58

import Primes

function get_corner_elements(dimension::Int)
    corner_elements = zeros(Int, 4)
    corner_elements[4] = dimension^2
    foreach(i -> corner_elements[i] = corner_elements[i + 1] - dimension + 1, 3:-1:1)
    corner_elements
end

count_primes(numbers::Vector{Int}) = count(Primes.isprime, numbers)

function update_count(cnt::Int, dimension::Int)
    corner_elements = get_corner_elements(dimension)
    cnt + count_primes(corner_elements)
end

compute_ratio(cnt::Int, dimension::Int) = cnt // (2 * dimension - 1)

function find_solution(ratio::Rational{Int})
    dimension = 3
    cnt = 3
    while compute_ratio(cnt, dimension) ≥ ratio
        dimension += 2
        cnt = update_count(cnt, dimension)
    end
    dimension
end

function main()
    ratio = 1 // 10
    dimension = find_solution(ratio)
    println("Dimension: ", dimension)
end

main()
