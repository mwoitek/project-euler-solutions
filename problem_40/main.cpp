// Champernowne's constant
// https://projecteuler.net/problem=40

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

using std::size_t;
using std::vector;
using digit_t = std::uint8_t;

vector<digit_t> getDigits(const unsigned totalDigits);
void printConstantToFile(const vector<digit_t>& digits);
template <typename Number, typename UInt>
Number myPow(Number b, UInt e);
unsigned computeProduct(const vector<digit_t>& digits);

int main() {
  const unsigned TOTAL_DIGITS = 1000000;

  const vector<digit_t> digits = getDigits(TOTAL_DIGITS);
  /* printConstantToFile(digits); */

  const unsigned product = computeProduct(digits);
  std::cout << "Product: " << product << '\n';

  return 0;
}

vector<digit_t> getDigits(const unsigned totalDigits) {
  vector<digit_t> digits(totalDigits);
  unsigned num = 1;
  unsigned numDigits = 0;

  while (numDigits < totalDigits) {
    for (const char d : std::to_string(num)) {
      try {
        digits.at((size_t)numDigits) = (digit_t)(d - '0');
        numDigits++;
      } catch (const std::out_of_range&) {
        break;
      }
    }
    num++;
  }

  return digits;
}

void printConstantToFile(const vector<digit_t>& digits) {
  std::stringstream fileName;
  fileName << "champernowne_" << digits.size() << ".txt";

  std::ofstream outFile;
  outFile.open(fileName.str());

  outFile << "0.";
  for (const digit_t digit : digits) {
    outFile << (unsigned)digit;
  }
  outFile << '\n';

  outFile.close();
}

template <typename Number, typename UInt>
Number myPow(Number b, UInt e) {
  Number result = Number(1);

  while (e > 0) {
    if (e % 2 != 0) {
      result *= b;
    }

    b *= b;
    e /= 2;
  }

  return result;
}

unsigned computeProduct(const vector<digit_t>& digits) {
  unsigned product = 1;

  for (unsigned e = 0; e <= 6; e++) {
    const size_t idx = myPow((size_t)10, e) - 1;
    product *= (unsigned)digits[idx];
  }

  return product;
}
