# Maximum path sum I
# https://projecteuler.net/problem=18

function read_triangle()
    lines = open("triangle.txt", "r") do f
        readlines(f)
    end
    num_lines = length(lines)

    triangle_matrix = zeros(Int, num_lines, num_lines)
    for (i, line) in enumerate(lines)
        triangle_matrix[i, 1:i] = parse.(Int, split(line))
    end

    triangle_matrix
end

function compute_sums_matrix(triangle_matrix::Matrix{Int})
    matrix_size = size(triangle_matrix)
    sums = zeros(Int, matrix_size)
    sums[1, 1] = triangle_matrix[1, 1]

    for row = 1:matrix_size[1]-1
        row_1 = row + 1
        for col = 1:row
            col_1 = col + 1

            s = sums[row, col] + triangle_matrix[row_1, col]
            if s > sums[row_1, col]
                sums[row_1, col] = s
            end

            s = sums[row, col] + triangle_matrix[row_1, col_1]
            if s > sums[row_1, col_1]
                sums[row_1, col_1] = s
            end
        end
    end

    sums
end

triangle_matrix = read_triangle()
sums = compute_sums_matrix(triangle_matrix)
max_sum = maximum(sums[end, :])
println("Maximum sum: $max_sum")
