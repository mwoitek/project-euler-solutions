#!/bin/bash

# Digit power sum
# https://projecteuler.net/problem=119

# ./main.sh 50 10000000

max_exp="$1"
num_powers="$2"
for ((exp = 2; exp <= "$max_exp"; exp++)); do
  julia solutions_fixed_exponent.jl "$exp" "$num_powers"
done

out_file="solutions.txt"
[[ -f "$out_file" ]] && trash-put "$out_file"
cat <<EOF >"$out_file"
81
512
2401
4913
5832
17576
19683
234256
390625
614656
1679616
EOF
for ((exp = 2; exp <= "$max_exp"; exp++)); do
  cat "solutions_${exp}.txt" >>"$out_file" && trash-put "solutions_${exp}.txt"
done
uniq <"$out_file" | sort -n >tmp.txt
mv -f tmp.txt "$out_file"

echo 'Answer:'
sed '30q;d' "$out_file"
