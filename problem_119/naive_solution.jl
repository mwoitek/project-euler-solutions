# Good for finding the first few terms. Otherwise, too slow.

# First 11 terms of the sequence:
# [81, 512, 2401, 4913, 5832, 17576, 19683, 234256, 390625, 614656, 1679616]

using Transducers

const x_max = 10^9

function has_property(x::T) where {T<:Integer}
    s = (sum ∘ digits)(x)
    s > 1 ? x == nextpow(s, x) : false
end

function generate_sequence(x_min::T, num_terms::Int) where {T<:Integer}
    tcollect(Filter(has_property) |> Take(num_terms), x_min:convert(T, x_max))
end

function main()
    num_terms = parse(Int, ARGS[1])
    seq = generate_sequence(10, num_terms)
    println("Answer: ", last(seq))
    println("First $(num_terms) terms of the sequence:")
    println(seq)
end

main()
