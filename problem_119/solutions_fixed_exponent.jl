const last_term = 1679616

function powers_generator(exp_::Int, num_powers::Int)
    min_base = ceil(Int, Float64(last_term)^(1 / exp_))
    max_base = min_base + num_powers - 1
    Channel{Tuple{Int,BigInt}}() do ch
        foreach(base -> put!(ch, (base, BigInt(base)^exp_)), min_base:max_base)
    end
end

test_tuple((base, power)::Tuple{Int,BigInt}) = base == (sum ∘ digits)(power)

function write_solutions(exp_::Int, num_powers::Int)
    open("solutions_$(exp_).txt", "w") do file
        for tpl in powers_generator(exp_, num_powers)
            test_tuple(tpl) && println(file, last(tpl))
        end
    end
end

function main()
    exp_ = parse(Int, ARGS[1])
    num_powers = parse(Int, ARGS[2])
    write_solutions(exp_, num_powers)
end

main()
